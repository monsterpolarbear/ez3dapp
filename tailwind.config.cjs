let contentPath = "static";
let pagesPath = "$lib/lib/Page";

if (process.env.VITE_CONTENT_PATH){
	contentPath = process.env.VITE_CONTENT_PATH + "static";
	pagesPath = process.env.VITE_CONTENT_PATH + "src/lib/Page"; 
}


const config = {
	content: ['./src/**/*.{html,js,svelte,ts}',`${pagesPath}/blocks/*.{html,js,svelte,ts}`],

	theme: {
		extend: {}
	},

	plugins: [
		require('@tailwindcss/typography')
	]
};

module.exports = config;
