import preprocess from 'svelte-preprocess';
// import adapter from '@sveltejs/adapter-auto';
import adapter from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/kit/vite';

let contentPath = "static";
let pagesPath = "src/lib/Page";
console.info("process.env.VITE_CONTENT_PATH: ", process.env.VITE_CONTENT_PATH)
if (process.env.VITE_CONTENT_PATH) {
	console.info("Content path set to: ", process.env.VITE_CONTENT_PATH)
	contentPath = process.env.VITE_CONTENT_PATH + "static";
	pagesPath = process.env.VITE_CONTENT_PATH + "src/lib/Page"; 
}

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://kit.svelte.dev/docs/integrations#preprocessors
	// for more information about preprocessors
	preprocess: [
		vitePreprocess(),
		preprocess({
			postcss: true,

			scss: {
				prependData: '@use "src/variables.scss" as *;'
			}
		})
	],

	kit: {
		alias:{
			'$pages':pagesPath,
			'$src':'src'
		},
		adapter: adapter(),
		prerender: { handleHttpError: 'ignore', handleMissingId: 'ignore' },
		files:{assets:contentPath}
	}
};

export default config;
