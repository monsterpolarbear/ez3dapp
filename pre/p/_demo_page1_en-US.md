<!--$demoLayout1 -->

<!--@topMenu-->
<!--::item1-->
Table Of Contents

<!--@>_demo_header-->


<!--@basic-->
<!--::content-->
## {{#welcome[#]Welcome to the ft3d Demo!}}

This is a demo page with a small tutorial to get you started.

In ft3d, the content of the 3D scene is linked with the content you can see in this page section.

Content can be on one page or on many, just like with any website.

For this tutorial, all content is on this page. When you start scrolling, you will notice that the 3d content stays in sync with the page view.



<!--@basic-->
<!--::content-->
## {{#navigate[#]Navigate in 3D}}

Ideally, you want your users to experience the 3D scene directly, without a detour over the page. 
In ft3d you can interact with the 3D scene throught hotspots, shown in the scene. 


<!--@basic-->
<!--::content-->
## {{#hotspots[#]Hotspots}}
Just like the page View, hotspots can have arbitrary content. 
This content is linked to the page. 
A hotspot can have many links to other 3D views and/or pages.

<!--@basic-->
<!--::content-->
## {{#3dviews[#]3D Views}}
In ft3d, a 3d View, or _shot_ as we call it, is a variation of the scene: Different camera angles, animations, lighting, visible objects and of course groups of hotspots.

<!--@basic-->
<!--::content-->

## {{#scrollTrigger[#]Scroll trigger}}
Without loading a new page, we trigger different shots.
Clicks on hotspots can guide the user to the information in the page View and highlight points of interest. 


<!--@basic-->
<!--::content-->
## {{#twoWay[#]Two way connection}}
This goes both ways: Hotspots can trigger and highlight information on a page and links on a page can trigger different shots with arbitrary groups of hotspots.



<!--@basic-->
<!--::content-->
## {{#yours[#]We will make it yours}}
Everything can be customized

