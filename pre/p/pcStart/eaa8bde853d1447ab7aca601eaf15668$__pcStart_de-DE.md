
<!--$demoLayout1-->

<!--@basicFullHeight-->
<!--::content-->
## {{#UCX_pcStart[,!pcStart]echtzeit3D Preview}}


Dies ist ein Preview Ihrer 3D-Daten.
Dieses Preview bleibt ab 16.3.2023 zwei Wochen online.


Mehr Infos zu den Features der interaktiven 3D-Visualisierung: 
<a class="__external" href="https://www.echtzeit-drei.de" target="_blank" rel="noopener noreferrer">www.echtzeit-drei.de</a>

{{#[,pcStart_chapter1_sub1]a shot}}

{{#[pcStart#kurzanleitung]Kurzanleitung}}

{{#[pcStart#kurzanleitung,pcStart_chapter1]__SVG__scrolldown}}


Symbol | Description | Min| Typ| Max | Unit
:--- | :--- | :--- | :--- | :--- |:--- |
VINMax| Maximum input voltage from VIN pad| 6| -| 20| V
VUSBMax| Maximum input voltage from USB connector| -| 5.5| V
PMax| Maximum Power Consumption| -| - | xx | mA


<!--@basicFullHeight-->
<!--::content-->
## {{#kurzanleitung[,!pcStart_chapter1] Die interaktive 3D-Visualisierung}}

Die Visualisierung besteht aus dem Page-View und der 3D-Ansicht.


### 3D-Ansicht
Die 3D-Ansicht stellt Variationen einer 3D-Szene dar, blendet zwischen diesen und ist gleichzeitig voll interaktiv.
Ob mit Maus oder per Touch, Nutzer:innen können sich in 3D frei bewegen.

Verortungen in der 3D-Ansicht sind virtuelle 'Post-its'. Sie können beliebige Information und Funktionalität enthalten. Mit ihnen werden Informationen verlinkt.

### Page-View
Der Page-View präsentiert Information und Funktionalität in längerer Form als in Verortungen sinnvoll ist.
Dabei sind Page-View und 3D-Ansicht miteinander verbunden und reagieren aufeinander.

Ob 200-seitige Betriebsanleitung oder Ihr Vertriebsprospekt in fünf Sprachen, der Page-View hält so viel Information wie Sie wollen. Der Page-View ist wie eine Webseite und kann neben den üblichen Medien wie Bilder und Videos auch funktionale User-Interface-Elemente enthalten (z.B. Menüs, Dropdowns, Buttons etc).

### Augmented Realtiy (AR)

Auf Smartphones und Tablets können Sie die 3D-Szene in ihrer Umgebung darstellen. Ist das Gerät technisch entsprechend ausgestattet, erscheint im 3D-View rechts oben der AR-Button. /
