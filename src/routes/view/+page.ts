import utilsConfig from '../../../utils/utils-config.js';
import { error } from '@sveltejs/kit';
import { language } from '$lib/store';

// Before build, these are overwritten with files from content:
import ftConfig from '../../../pre/ft-pc-config.json';
import backHierarchy from '../../../pre/menu/back_hierarchy.json';
import menu from '../../../pre/menu/menu.json';
import collections from '../../../pre/collections/collections.json';
import gltfLoadData from '../../../pre/data/gltf_load.json';
import ucxMeshMapping from '../../../pre/data/mv_ucx_mesh_mapping.json';


export async function load({ fetch, params, url }) {

    const dev = import.meta.env.DEV;

    let ftConfigDev, backHierarchyDev, collectionsDev, ucxMeshMappingDev, gltfLoadDataDev, menuDev;


    if (dev) {
        const contentPath = import.meta.env.VITE_CONTENT_PATH;

        console.info("Fetching... ", contentPath)

        if (!contentPath) {
            console.info("No contentPath: ", contentPath)
            throw error(404, {
                message: 'No content. Did you specify CONTENT env var?'
            });

        }
        console.info("Fetching Data ", `http://${utilsConfig.HOST}:${utilsConfig.PORT}/data`)
        const res = await fetch(`http://${utilsConfig.HOST}:${utilsConfig.PORT}/data`);
        const data = await res.json();

        if (res.ok) {
            ftConfigDev = data.ftConfig;
            backHierarchyDev = data.backHierarchy;
            collectionsDev = data.collections;
            ucxMeshMappingDev = data.ucxMeshMapping;
            gltfLoadDataDev = data.gltfLoadData;
            menuDev = data.menuData;
        } else {
            console.info("Could not fetch data")
            throw new Error(data);
        }
        // Set language in store
        language.set(ftConfigDev.locales[0]);
        return { ftConfigDev, backHierarchyDev, collectionsDev, gltfLoadDataDev, ucxMeshMappingDev, menuDev }
    }


    // this runs during build

    language.set(ftConfig.locales[0]);

    return { ftConfig, backHierarchy, collections, gltfLoadData, ucxMeshMapping, menu };

}