var Api = pc.createScript('api');

// initialize code called once per entity
Api.prototype.initialize = function () {
    console.info("Initialise! Yayayay!")
    var self = this;
    this.dev = false;
    this.visMeshInstances = [];
    this.visMin = 0.0;
    this.toShow = []; // Array<MeshInstace>
    this.toHide = [];// Array<MeshInstace>
    this.shotData = null;
    this.tweenStore = { "_env": {} };
    this.enableNodes = []; // Scene nodes that can be enabled/disabled from a shot
    this.ucxNodes = []; // Collider/Hotspot nodes
    this.hotspotCount = 0; // amount of visible hotspots
    this.pixelRatio = window.devicePixelRatio;
    this.cameraPivotPointTarget = null;
    this.cameraTransition = false;
    this.cameraPivotPointLerp = new pc.Vec3();
    this.cameraTransitionTimer = 0.0;
    this.cameraTransitionSpeed = 1.0;
    this.oCollections = {}; this.setCollectionsData(self);
    this.loadingIndicator = null; // the loading text

    // Store the inital shot, so we can access 
    // things like env, envSphere if they are missing
    // in a subsequent shot. 
    // This is especially important, if we trigger shots
    // during the loading phase. If a triggered shot has no
    // environment, the view will remain dark.
    this.initalShot = null;

    console.info("Device Pixel Ratio: ", this.pixelRatio)


    this.dWrapper = document.getElementById("d-wrapper");
    var rect = this.dWrapper.getBoundingClientRect();
    // console.log(rect.top, rect.right, rect.bottom, rect.left);

    // Collect all animations per GLTF and map them to the states in stateAnimGraph
    this.stateAnimNameMapping = {};

    // We only want to run model load once at start of app and ignore on subsequent shots
    this.loadWasStarted = false; // workaround for now, to block the api from loading again, if a load is in progress and shot is triggered again.
    this.modelLoaded = false;

    // Hotspots
    // Object with objectNames as keys {"Cube":{div:HTMLEL, screenPos:pcVec2, target:entity}}. Will be updated in postUpdate function.
    this.hotspotStore = {};

    this.initLoadingIndicator(self);

    if (window.location.href.indexOf("?debug=true") > -1) {
        console.info("In debug mode");
        self.dev = true;
    }


    //
    //Update wrapper bounding. 
    //const wrapperBounding = function () {
    //    rect = this.dWrapper.getBoundingClientRect();
    //    console.log(rect.top, rect.right, rect.bottom, rect.left);
    //}.bind(this);
    //window.addEventListener('resize', wrapperBounding, false);



    function mouseDownHandler(event) {
        if (event.target.nodeName != "CANVAS") {
            return;
        }
        const hotspotDivs = document.querySelectorAll('[data-ucx');
        hotspotDivs.forEach((hItem) => {
            hItem.classList.add("noHover");
        });
        console.info("noHover added")
    }


    function mouseUpHandler(event) {
        const hotspotDivs = document.querySelectorAll('[data-ucx');
        hotspotDivs.forEach((hItem) => {
            hItem.classList.remove("noHover");
        });
        console.info("noHover removed")
    }



    // this.dWrapper.addEventListener("click", mouseClickHandler);
    this.dWrapper.addEventListener("mousedown", mouseDownHandler)
    this.dWrapper.addEventListener("mouseup", mouseUpHandler)




    // listen for the meshtrigger event
    // this.app.on('pickerRaycast:meshtrigger', this.sendMeshtrigger, self);

    // remove event listeners when script destroyed
    //this.on('destroy', function () {
    //    this.app.off('pickerRaycast:meshtrigger', this.sendMeshtrigger);
    //});


    window.addEventListener('pagehide', (event) => { console.info("Destroy"); self.app.destroy(); });



    //var app = pc.Application.getApplication();
    this.cameraEntity = this.app.root.findByName('Camera');
    console.info("Camera: ", this.cameraEntity.script);

    // Disable lighting on background sphere
    this.envSphere = this.app.root.findByName('EnvSphere');
    if (this.envSphere) {
        this.envSphere.render.meshInstances.forEach(function (meshInstance) {
            meshInstance.material.useLighting = false;
            meshInstance.material.useGammaTonemap = false;
            // meshInstance.material.useSkybox = false;
        })
    }

    /*
        if (!self.dev) {
            window.addEventListener("message", function (event) {
                //if (event.origin === "http://example.com") { // always check message came from your website
                if (!event.data) {
                    return;
                }
                if (event.data.destroy) {
                    self.app.destroy();
                }
                // If shotData contains inlineOverride, do not replace
                // shotData. Just apply the shotOverride right away:
                if (event.data.inlineOverride) {
                    self.processInlineOverride(event.data, self);
    
                } else {
                    self.shotData = event.data; console.info("ShotData received: "); console.info(self.shotData);
                    self.applyShot(self.shotData, self);
                }
    
                //}
            }, false);
        } else {
            self.app.keyboard.on(pc.EVENT_KEYDOWN, this.onKeyDown, self);
            // On camera end move, log camera properties
            self.app.mouse.on(pc.EVENT_MOUSEUP, function (event) {
                const info = {
                    "pivotPoint": this.cameraEntity.script.orbitCamera.pivotPoint,
                    "float": {
                        "distance": this.cameraEntity.script.orbitCamera.distance,
                        "pitch": this.cameraEntity.script.orbitCamera.pitch,
                        "yaw": this.cameraEntity.script.orbitCamera.yaw,
                        "fov": this.cameraEntity.camera.fov
                    }
                };
                //console.info(info);
            }, self);
        }
    */
};

// update code called every frame
Api.prototype.update = function (dt) {
    // Handle Camera Transitions
    // YAW
    /*
    if (this.cameraTransitionYaw && this.cameraTargetFloats?.yaw) {
        const currentYaw = this.cameraEntity.script.orbitCamera.yaw;
        if (this.cameraTargetFloats.yaw != currentYaw) {
            // current camera yaw
            var diff = this.cameraTargetFloats.yaw - currentYaw;
            var remainder = diff % 360;
            if (remainder > 180) {
                this.cameraEntity.script.orbitCamera._targetYaw = currentYaw - (360 - remainder);
            } else if (remainder < -180) {
                this.cameraEntity.script.orbitCamera._targetYaw = currentYaw + (360 + remainder);
            } else {
                this.cameraEntity.script.orbitCamera._targetYaw = currentYaw + remainder;
            }
            this.cameraTransitionYaw = false;
        } else {
            this.cameraTransitionYaw = false;
        }

    } else {
        this.cameraTransitionYaw = false;
    }
    */
    /*
    if (this.cameraTransitionPitch && this.cameraTargetFloats?.pitch) {
        const currentPitch = this.cameraEntity.script.orbitCamera.pitch;
        if (this.cameraTargetFloats.pitch != currentPitch) {
            
            var diff = this.cameraTargetFloats.pitch - currentPitch;
            var reminder = diff % 360;
            if (reminder > 180) {
                this.cameraEntity.script.orbitCamera._targetYaw = currentYaw - (360 - reminder);
            } else if (reminder < -180) {
                this.cameraEntity.script.orbitCamera._targetYaw = currentYaw + (360 + reminder);
            } else {
                this.cameraEntity.script.orbitCamera._targetYaw = currentYaw + reminder;
            }
        } else {
            this.cameraTransitionYaw = false;
        }

    } else {
        this.cameraTransitionYaw = false;
    }

    */
    if (this.cameraTransition) {
        this.cameraTransitionTimer += this.cameraTransitionSpeed * dt;
        var vt = Math.sin((this.cameraTransitionTimer - 0.5) * 3.1416) * 0.5 + 0.5;
        this.cameraPivotPointLerp.lerp(this.cameraEntity.script.orbitCamera.pivotPoint, this.cameraPivotPointTarget, vt)
        this.cameraEntity.script.orbitCamera.pivotPoint = this.cameraPivotPointLerp;
        // console.info(this.cameraTransitionTimer)
        if (this.cameraTransitionTimer > 1.0) {
            this.cameraEntity.script.orbitCamera.pivotPoint.copy(this.cameraPivotPointTarget);
            this.cameraTransition = false;
            this.cameraTransitionTimer = 0;

        }
        // this.cameraEntity.script.orbitCamera._removeInertia();
        // this.cameraEntity.script.orbitCamera._updatePosition();
    }


    // Handle visibilities
    if (this.toShow.length) {
        this.toShow.forEach((n) => {
            const currentOpacity = n.parameters.material_opacity.data;
            if (currentOpacity < 0.999) {
                //console.info(`Increasing vis of ${nd.name} to ${nd.visibility} `)

                n.setParameter("material_opacity", 1.0);
            } else {
                this.removeFromShow(n.node.name, this);
            }
        });
    }
    if (this.toHide.length) {
        this.toHide.forEach((n) => {
            const currentOpacity = n.parameters.material_opacity.data;
            if (currentOpacity > this.visMin) {
                n.setParameter("material_opacity", this.visMin);
            } else {
                //nd.setEnabled(false);
                this.removeFromHide(n.node.name, this);
            }
        });
    }

};

Api.prototype.fitNumberInRange = function (sourceMin, sourceMax, destMin, destMax, inputNumber) {

    // (5 - 0) / (1000-0)  *  (100-10)+10
    const percent = (inputNumber - sourceMin) / (sourceMax - sourceMin);
    const result = percent * (destMax - destMin) + destMin;
    return result < 0 ? 0 : result;

}

Api.prototype.postUpdate = function (dt) {
    if (Object.keys(this.hotspotStore).length) {

        Object.keys(this.hotspotStore).forEach(objectName => {
            const hotspotData = this.hotspotStore[objectName];
            var worldPos = hotspotData.target.getPosition();
            this.cameraEntity.camera.worldToScreen(worldPos, hotspotData.screenPos);
            // check if the entity is in front of the camera
            if (hotspotData.screenPos.z > 0) {
                //this.entity.element.enabled = true;


                // hotspotData.screenPos.x *= this.pixelRatio;
                // hotspotData.screenPos.y *= this.pixelRatio;

                // var device = this.app.graphicsDevice;

                // Global position of elements is normalised between -1 and 1 on both axis

                //X:  ((screenPos.x / device.width) * 2) - 1, 
                //Y: ((1 - (screenPos.y / device.height)) * 2) - 1, 

                // ((hotspotData.screenPos.x / this.dWrapper.width)*2) - 1;
                // ((1 - (hotspotData.screenPos.y / this.dWrapper.height)) * 2) - 1;

                // Fit worldToScreen coordinates into dWrapper bounds

                // Figure out depth
                if (hotspotData.div.matches(':hover')) {
                    hotspotData.div.style.zIndex = 1000
                } else {
                    // To figure out depth, need to scale the distance first (depending on scene bounds?).
                    // This ensures, there is enough space between the numbers
                    hotspotData.div.style.zIndex = Math.round(this.fitNumberInRange(0, 1000, 100, 10, hotspotData.screenPos.z * 100));
                }
                // hotspotData.div.style.opacity = this.fitNumberInRange(0, 1000, 1, 0, hotspotData.screenPos.z * 100);
                hotspotData.div.style.left = hotspotData.screenPos.x + 'px';
                hotspotData.div.style.top = hotspotData.screenPos.y + 'px';



            } else {
                // Hide DOM element?
                //hotspotData.div.style.display = 'none';
                null;
            }
        })
    }
}



// For dev testing 
//Api.prototype.onKeyDown = function (event) {

//};


Api.prototype.runExternalShot = function () {
    console.info("Running external shot")
    console.info(this.shotData)
    this.applyShot(this.shotData, this);
}

/** LOADING **/

Api.prototype.loadGlbContainerFromUrl = function (url, options, assetName, callback, ctx) {
    var filename = assetName + '.glb';
    var file = {
        url: url,
        filename: filename
    };

    var asset = new pc.Asset(filename, 'container', file, null, options);
    asset.once('load', function (containerAsset) {
        if (callback) {
            callback(null, containerAsset);
        }
    });

    ctx.app.assets.add(asset);
    ctx.app.assets.load(asset);

    return asset;
};


Api.prototype.loadModel = function (d, ctx) {

    // Little workaround to resize the canvas at the beginning
    this.app.resizeCanvas(this.dWrapper.clientWidth, this.dWrapper.clientHeight);


    // If we end up in this function, we can only continue with a model path
    if (!d.model) {
        return;
    }
    var filename = d.model.substring(d.model.lastIndexOf('/') + 1);

    ctx.loadGlbContainerFromUrl(d.model, null, filename,
        function (err, asset) { // Callback for when model is loaded
            if (err) {
                console.error(err);
                return;
            }
            console.info(asset)
            //const gltfAnimStateMapping = {};
            var renderRootEntity = asset.resource.instantiateRenderEntity();

            // Add to scene
            // Create a new entity with name of file, add stateGraph to this node
            var gltfRoot = new pc.Entity();
            gltfRoot.enabled = false;
            const rootName = filename.replace('.glb', '');
            gltfRoot.name = rootName;
            ctx.app.root.addChild(gltfRoot);

            gltfRoot.addChild(renderRootEntity);

            // Set scale
            const scale = new pc.Vec3(d.scale, d.scale, d.scale);
            renderRootEntity.setLocalScale(scale);

            // Set Lookat
            if (d.lookAt?.length) {
                d.lookAt.forEach(o => {
                    // find entity
                    const found = ctx.app.root.findByName(o);
                    if (found) {
                        // add script component
                        found.addComponent('script');
                        console.info(found)
                        found.script.create('lookAt', {
                            attributes: {
                                cameraEntity: ctx.cameraEntity
                            }
                        });
                    }
                })
            }

            //Add Uv offset script
            // Add the script to the entity
            if (d.uvOffset?.length) {
                d.uvOffset.forEach(item => {
                    const found = ctx.app.root.findByName(item.objectName);
                    if (found) {
                        found.addComponent('script');
                        found.script.create('uvOffset', {
                            attributes: {
                                boneName: item.boneName,
                                speed: item.speed,
                                sourceBoneAxis: item.sourceAxis,
                                targetOffsetAxis: item.targetAxis
                            }
                        });
                        console.info("UVOffset: Setup material");
                        found.render.meshInstances.forEach(function (meshInstance) {
                            meshInstance.material.emissiveIntensity = 1;
                            if (item.cull) {
                                meshInstance.material.cull = item.cull;
                            } else {
                                meshInstance.material.cull = 0;
                            }
                            meshInstance.material.blendType = pc.BLEND_ADDITIVEALPHA;
                            meshInstance.material.update();
                            console.info(meshInstance.material)
                        });
                        // Setup material
                        // opacity blend type=alpha
                        // is map in emissive?
                        // depth test?
                        // cull mode?

                        console.info("UVOffset: Render component setup")
                        console.info(found.render)
                        found.render.castShadows = false;
                        found.render.receiveShadows = false;
                        found.render.castShadowsLightmap = false;
                    }
                });
            }
            console.info("Loading model done!");
            // window.parent.postMessage("modelLoadDone", "*");



            // Process Collider
            gltfRoot.forEach(c => {
                if (c.name.startsWith('UCX_')) {
                    const assetID = c.render.asset;
                    try {
                        // Remove render component if exists
                        c.removeComponent('render');

                    } catch (error) {
                        console.info("No render component to remove");
                    }
                    try {
                        // Remove collision if exists
                        c.removeComponent('collision');
                    } catch (error) {
                        console.info("No collision component to remove");
                    }
                    c.addComponent('collision', {
                        type: 'mesh', renderAsset: assetID
                    });
                }
            });

            // Process Animations
            if (d.animation?.length) {
                // Build anim stateGraph
                const stateGraph = ctx.buildAnimStateGraph(d, ctx);
                // Prepare Anim Component
                gltfRoot.addComponent('anim');
                gltfRoot.anim.loadStateGraph(stateGraph);
                //gltfRoot.anim.activate = false;
                const animLayer = gltfRoot.anim.baseLayer;
                console.info("StateGraph loaded")

                // Collect all animations to load into object
                const animsToLoad = {};
                d.animation.forEach(a => {
                    animsToLoad[a.name] = false;
                });

                // Add each animations
                for (let i = 0; i < d.animation.length; i++) {
                    const a = d.animation[i];

                    var animFilename = a.path.substring(a.path.lastIndexOf('/') + 1);
                    ctx.loadGlbContainerFromUrl(a.path, null, animFilename,
                        function (err, animAsset) {  // Callback for when animation is loaded

                            if (err) {
                                console.error(err);
                                return;
                            }
                            // add animation to anim state graph
                            console.info("Loaded animation", a.name);
                            if (i === 0) {
                                // Add first Animation to Initial State
                                animLayer.assignAnimation("Initial State",
                                    animAsset.resource.animations[0].resource);
                            }
                            animLayer.assignAnimation(a.name,
                                animAsset.resource.animations[0].resource, 1, a.loop);
                            console.info("Assigned anim ", a.name)
                            animsToLoad[a.name] = true;

                            // Finish up if all anims are true
                            if (Object.values(animsToLoad).every(item => item === true)) {

                                // ctx.stateAnimNameMapping[rootName] = gltfAnimStateMapping;
                                // console.info(ctx.stateAnimNameMapping);

                                ctx.collectEnableNodes(gltfRoot, ctx);
                                // ctx.collectVisMeshInstances(ctx); // Vis is currently turned off
                                gltfRoot.enabled = true;
                                ctx.modelLoaded = true;
                                console.info("Loading Animation Done!");
                                // window.parent.postMessage("animationLoadDone", "*");
                                // Trigger the shot again, to process the rest of the shot
                                const loading_circle = ctx.app.root.findByName("loading_circle")
                                if (loading_circle) {
                                    loading_circle.enabled = false;
                                }
                                if (ctx.loadingIndicator) {
                                    ctx.loadingIndicator.remove()
                                };
                                //gltfRoot.anim.activate = true;
                                //animLayer.reset()
                                //animLayer.play()
                                ctx.applyShot(ctx.shotData, ctx);
                            }

                        },
                        ctx);
                }
            } else {
                // No animation, finish up
                // window.parent.postMessage("animationLoadDone", "*");
                gltfRoot.enabled = true;
                ctx.modelLoaded = true;
                // Trigger the shot again, to process the rest of the shot
                const loading_circle = ctx.app.root.findByName("loading_circle")
                if (loading_circle) {
                    loading_circle.enabled = false;
                }
                if (ctx.loadingIndicator) {
                    ctx.loadingIndicator.remove()
                };
                ctx.applyShot(ctx.shotData, ctx);
            }
        },
        ctx);

};



Api.prototype.animGraphTrigger = function (d, ctx) {
    Object.keys(d.animGraph).forEach(entityName => {
        const anm = d.animGraph[entityName];

        const en = ctx.app.root.findByName(entityName);
        if (!en) { return; }
        if (!anm.name || !anm.value) {
            return;
        }
        const stateName = anm.name;
        console.info("Set trigger ", stateName, anm.value)
        console.info(en.anim)
        en.anim.setTrigger(stateName, anm.value);
        if (anm.speed != null) {
            en.anim.speed = anm.speed;
        }
    });
};

/**
 * Look up page/shot in hotspots data
 */
Api.prototype.sendMeshtrigger = function (event) {
    if (!this.shotData?.hotspots) {
        return;
    }

    //const found = this.shotData.hotspots.findIndex(h =>
    //    h.object === event
    //);
    if (this.shotData.hotspots[event] != null) {
        // If shot in meshtrigger data contains inline Override, it will
        // round trip and come back through the message, where the API will 
        // catch that it is an inline-override
        window.parent.postMessage({ "meshtrigger": this.shotData.hotspots[event] }, "*");

    }
};


Api.prototype.collectVisMeshInstances = function (ctx) {
    ctx.visMeshInstances.length = 0;

    ctx.app.root.find((n) => {
        var renders = n.findComponents('render');
        for (var i = 0; i < renders.length; ++i) {
            var meshInstances = renders[i].meshInstances;
            for (var j = 0; j < meshInstances.length; j++) {
                ctx.visMeshInstances.push(meshInstances[j]);
            }
        }
    }); console.info(ctx.visMeshInstances)
};

Api.prototype.removeFromShow = function (item, ctx) {
    //const index = ctx.toShow.indexOf(item);
    const index = ctx.toShow.findIndex((i => {
        i.node.name === item
    }))
    if (index > -1) {
        ctx.toShow.splice(index, 1);
    }
};

Api.prototype.removeFromHide = function (item, ctx) {
    const index = ctx.toHide.findIndex((i => i.node.name === item));
    if (index > -1) {
        ctx.toHide.splice(index, 1);
    }
}

Api.prototype.updateVisibility = function (d, ctx) {
    const objects = d.vis;
    ctx.visMin = d.visMin != null ? d.visMin : 0.0;

    ctx.visMeshInstances.forEach((m) => {
        if (!m.parameters?.material_opacity) {
            m.setParameter("material_opacity", 0.999);
        }
        const opacity = m.parameters.material_opacity.data;

        // Depending on shotData and opacity, move visibilityMesh
        // into toShow or toHide
        switch (d.visType) {
            case "show":
                if (
                    (opacity >= 0.999 && objects.includes(m.node.name)) ||
                    (opacity <= ctx.visMin && !objects.includes(m.node.name))
                ) {
                    null;
                }
                if (opacity < 0.999 && objects.includes(m.node.name)) {
                    ctx.removeFromHide(m.node.name, ctx);
                    ctx.toShow.push(m);
                }

                if (opacity > ctx.visMin && !objects.includes(m.node.name)) {
                    ctx.removeFromShow(m.node.name, ctx);
                    ctx.toHide.push(m);
                }
                break;
            case "hide":
                if (
                    (opacity <= ctx.visMin && objects.includes(m.node.name)) ||
                    (opacity >= 0.999 && !objects.includes(m.node.name))
                ) {
                    null;
                }
                if (opacity > ctx.visMin && objects.includes(m.node.name)) {
                    ctx.removeFromShow(m.node.name, ctx);
                    ctx.toHide.push(m);
                }

                if (opacity < 0.999 && !objects.includes(m.node.name)) {
                    ctx.removeFromHide(m.node.name, ctx);
                    ctx.toShow.push(m);
                }
                break;
        }


    })

}


Api.prototype.applyShot = function (d, ctx) {
    if (!ctx.modelLoaded) {
        // Only proceed if no loading is in progress
        if (ctx.loadWasStarted) {
            console.info("Load in progress...")
            return;
        }
        if (d.load?.length) {
            ctx.loadWasStarted = true;
            ctx.initalShot = d; // Store as inital shot data.
            d.load.forEach((modelData) => {
                ctx.loadModel(modelData, ctx);
            });
        }
        // We dont want to process the rest of the shot
        // until the model is loaded. Therefore early return here. 
        // The load function will call the shot again when it is done.
        console.info("No model loaded.")
        return;
    }

    // If the shot has no data related to env and EnvSphere,
    // copy the data from inital shot:
    if (!d.animation?.env || !d.animation?.EnvSphere) {
        if (!d.animation) {
            d["animation"] = {}
        }

        if (!d.animation?.env && ctx.initalShot.animation?.env) {
            d.animation["env"] = ctx.initalShot.animation.env;
        }
        if (!d.animation?.EnvSphere && ctx.initalShot.animation?.EnvSphere) {
            d.animation["EnvSphere"] = ctx.initalShot.animation.EnvSphere;
        }
    }



    if (d.setEnabled != null && d.enabledState != null) {
        ctx.handleEnabled(d, ctx);
    }

    if (d.animation) {
        // Trigger animations
        Object.keys(d.animation).forEach(entityName => {
            const anim = d.animation[entityName];
            //})
            //d.animation.forEach(anim => {
            const en = ctx.app.root.findByName(entityName);

            // If there have not been any tweens stored, 
            // initialise in tweenStore
            if (!(entityName in ctx.tweenStore)) {
                ctx.tweenStore[entityName] = {};
            }

            switch (anim.type) {

                case 'geo':
                    // Start animation at first in Array of keys
                    if (anim.keys[0]?.geo.position) {
                        ctx.tweenPosition(en, 'geo', anim.keys, 0, ctx);
                    }
                    if (anim.keys[0]?.geo.rotation) {
                        ctx.tweenRotation(en, 'geo', anim.keys, 0, ctx);
                    }
                    // enabled state. This is for things that do not come from the gltf. Mainly for use with
                    // the envSphere
                    if (anim.keys[0]?.geo.enabled != null) {
                        en.enabled = anim.keys[0].geo.enabled;
                    }
                    // animate material
                    if (anim.keys[0]?.geo.material != null) {
                        ctx.tweenMaterial(en, 'geo', anim.keys, 0, ctx);
                    }
                    break;
                case 'light':
                    // Start animation at first in Array of keys
                    if (anim.keys[0]?.light.position) {
                        ctx.tweenPosition(en, 'light', anim.keys, 0, ctx);
                    }
                    if (anim.keys[0]?.light.rotation) {
                        ctx.tweenRotation(en, 'light', anim.keys, 0, ctx);
                    }
                    if (anim.keys[0]?.light.float) {
                        ctx.tweenFloat(en, 'light', anim.keys, 0, ctx);
                    }
                    if (anim.keys[0]?.light.color) {
                        ctx.tweenColor(en, 'light', anim.keys, 0, ctx);
                    }
                    break;

                case 'camera':
                    if (anim.keys[0]?.camera?.float?.yaw != null) {
                        //ctx.tweenCameraFloats(en, 'camera', anim.keys, 0, ctx);
                        ctx.cameraEntity.script.orbitCamera._targetYaw = anim.keys[0].camera.float.yaw;

                    }
                    if (anim.keys[0]?.camera?.float?.pitch != null) {
                        ctx.cameraEntity.script.orbitCamera._targetPitch = anim.keys[0].camera.float.pitch;
                    }

                    if (anim.keys[0]?.camera?.float?.distance != null) {
                        ctx.cameraEntity.script.orbitCamera._targetDistance = anim.keys[0].camera.float.distance;
                    }


                    if (anim.keys[0]?.camera.pivotPoint) {
                        // PivotPoint can also be string. In this case, we want
                        // to look for an object with string as name, and use
                        // its localPosition as pivotPoint
                        if (typeof anim.keys[0]?.camera.pivotPoint === "string") {
                            const found = ctx.app.root.findByName(anim.keys[0]?.camera.pivotPoint);
                            if (found) {
                                const pos = found.getPosition();
                                if (pos) {
                                    // Replace string pivotPoint in shotdata
                                    anim.keys[0].camera.pivotPoint = pos;

                                    // ctx.cameraEntity.script.orbitCamera.pivotPoint = anim.keys[0]?.camera.pivotPoint;
                                    ctx.cameraPivotPointTarget = anim.keys[0]?.camera.pivotPoint;
                                    ctx.cameraTransition = true;
                                    ctx.cameraTransitionTimer = 0.0;

                                }
                            }
                        } else {

                            // ctx.cameraEntity.script.orbitCamera.pivotPoint = anim.keys[0]?.camera.pivotPoint;
                            ctx.cameraPivotPointTarget = anim.keys[0]?.camera.pivotPoint;
                            ctx.cameraTransition = true;
                            ctx.cameraTransitionTimer = 0.0;

                        }
                    }
                    if (anim.keys[0]?.camera.fov) {
                        ctx.tweenCameraFov(en, 'camera', anim.keys, 0, ctx);
                    }
                    break;
                case 'env':
                    if (anim.keys[0]?.env.skyboxIntensity) {
                        ctx.tweenSkyboxIntensity(en, 'env', anim.keys, 0, ctx);
                    }

                    if (anim.keys[0]?.env.skyboxRotation) {
                        ctx.tweenSkyboxRotation(en, 'env', anim.keys, 0, ctx);
                    }

                    break;
                default:
                    console.info("No Animation Handler for this type")
            }
        })
    }

    if (d.seeThrough) {
        // Find node
        const found = ctx.app.root.findByName(d.seeThrough[0]);
        // Get all the mesh instances to modify later in the update loop
        var renders = found.findComponents('render');

        for (var i = 0; i < renders.length; ++i) {
            var meshInstances = renders[i].meshInstances;
            for (var j = 0; j < meshInstances.length; j++) {
                meshInstances[j].setParameter("material_opacity", 0.8);
            }
        }

    }
    if (d.vis) {
        ctx.updateVisibility(d, ctx);
    }
    if (d.animGraph) {
        ctx.animGraphTrigger(d, ctx);
    }

    if (!d.keepHotspots) {
        ctx.handleHotspots(d, ctx);
    }


};



Api.prototype.tweenPosition = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].position.stop(); console.info("Position tween stopped");
    } catch { console.info("No position tween to stop"); }

    ctx.tweenStore[en.name].position =
        en.tween(en.getLocalPosition())
            .to(keys[index][type].position, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].position) {
                    ctx.tweenPosition(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Position tween complete")
                }
            }).start();
};

Api.prototype.tweenRotation = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].rotation.stop(); console.info("Rotation tween stopped");
    } catch { console.info("No rotation tween to stop"); }
    ctx.tweenStore[en.name].rotation =
        en.tween(en.getLocalEulerAngles())
            .rotate(new pc.Vec3(keys[index][type].rotation.x, keys[index][type].rotation.y, keys[index][type].rotation.z), keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].rotation) {
                    ctx.tweenRotation(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Rotation tween complete");
                }
            })
            .start();
};

Api.prototype.tweenMaterial = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].material.forEach(t => {
            t.stop(); console.info("Material tween stopped");
        })
    } catch { console.info("No Material tween to stop"); }

    // Store tween for each meshInstance
    ctx.tweenStore[en.name]["material"] = []

    en.render.meshInstances.forEach(function (meshInstance) {

        // Object to fill with start data
        const startMaterial = { diffuse: {}, glossiness: { "value": null }, emissive: {} }

        // Alter!
        const startDataDiffuse = meshInstance.material.getParameter('material_diffuse').data
        const startDataEmissive = meshInstance.material.getParameter('material_emissive').data

        startMaterial.diffuse["r"] = startDataDiffuse[0];
        startMaterial.diffuse["g"] = startDataDiffuse[1];
        startMaterial.diffuse["b"] = startDataDiffuse[2];
        startMaterial.glossiness.value = meshInstance.material.getParameter('material_shininess').data * 100;
        startMaterial.emissive["r"] = startDataEmissive[0];
        startMaterial.emissive["g"] = startDataEmissive[1];
        startMaterial.emissive["b"] = startDataEmissive[2];

        // startMaterial.emissiveIntensity.value = meshInstance.material.emissiveIntensity;

        console.info("Material:")
        console.info(meshInstance.material)
        console.info(startMaterial)


        // Object that holds the target data for the tween.
        // If no data in shot, fill Object with current material data.
        const material = {
            diffuse: {
                "r": keys[index][type].material.diffuse ? keys[index][type].material.diffuse.r : meshInstance.material.diffuse.r,
                "g": keys[index][type].material.diffuse ? keys[index][type].material.diffuse.g : meshInstance.material.diffuse.g,
                "b": keys[index][type].material.diffuse ? keys[index][type].material.diffuse.b : meshInstance.material.diffuse.b
            },
            glossiness: {
                "value": keys[index][type].material.glossiness != null ? keys[index][type].material.glossiness : startMaterial.glossiness.value
            },
            emissive: {
                "r": keys[index][type].material.emissive ? keys[index][type].material.emissive.r : meshInstance.material.emissive.r,
                "g": keys[index][type].material.emissive ? keys[index][type].material.emissive.g : meshInstance.material.emissive.g,
                "b": keys[index][type].material.emissive ? keys[index][type].material.emissive.b : meshInstance.material.emissive.b
            },
        }

        // Only tween diffuse if there is a change in data
        // This is really wierd. Need to compare to material.diffuse, but animate from material.getParameter...
        if (meshInstance.material.diffuse.r != material.diffuse.r || meshInstance.material.diffuse.g != material.diffuse.g || meshInstance.material.diffuse.b != material.diffuse.b) {
            console.info("Material Diffuse Tween: Change in data")
            const t = ctx.app.tween(startMaterial.diffuse)
                .to(material.diffuse, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
                .on('update', function () {
                    // meshInstance.material.diffuse = new pc.Color(startMaterial.diffuse.r, startMaterial.diffuse.g, startMaterial.diffuse.b,1)
                    meshInstance.material.diffuse.set(startMaterial.diffuse.r, startMaterial.diffuse.g, startMaterial.diffuse.b, 1)
                    //meshInstance.setParameter('material_diffuse', [startMaterial.diffuse.r, startMaterial.diffuse.g, startMaterial.diffuse.b]);
                    meshInstance.material.update();
                })
                .on('complete', function () {
                    if (keys[index + 1] && keys[index + 1][type].color) {
                        ctx.tweenMaterial(en, type, keys, index + 1, ctx);
                    } else {
                        console.info("Material Diffuse tween complete");
                    }
                }).start();
            ctx.tweenStore[en.name]["material"].push(t);
        } else {
            console.info("Material Diffuse Tween: NO Change in data")
        }

        if (material.glossiness?.value != null && startMaterial.glossiness.value != material.glossiness.value) {
            // console.info("Material Glossiness Tween: Change in data");
            console.info("Tween shininess FROM", startMaterial.glossiness.value, "TO", material.glossiness.value)

            const t = ctx.app.tween(startMaterial.glossiness)
                .to(material.glossiness, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
                .on('update', function () {
                    meshInstance.material.shininess = startMaterial.glossiness.value;
                    meshInstance.material.update();
                })
                .on('complete', function () {
                    if (keys[index + 1] && keys[index + 1][type].color) {
                        ctx.tweenMaterial(en, type, keys, index + 1, ctx);
                    } else {
                        console.info("Material Glossiness tween complete");
                    }
                }).start();
            ctx.tweenStore[en.name]["material"].push(t);
        } else {
            console.info("Material Glossiness Tween: NO Change in data")
        }

        if (meshInstance.material.emissive.r != material.emissive.r || meshInstance.material.emissive.g != material.emissive.g || meshInstance.material.emissive.b != material.emissive.b) {
            console.info("Material Emissive Tween: Change in data")
            const t = ctx.app.tween(startMaterial.emissive)
                .to(material.emissive, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
                .on('update', function () {
                    meshInstance.material.emissive.set(startMaterial.emissive.r, startMaterial.emissive.g, startMaterial.emissive.b, 1);
                    meshInstance.material.update();
                })
                .on('complete', function () {
                    if (keys[index + 1] && keys[index + 1][type].color) {
                        ctx.tweenMaterial(en, type, keys, index + 1, ctx);
                    } else {
                        console.info("Material Emissive tween complete");
                    }
                }).start();
            ctx.tweenStore[en.name]["material"].push(t);
        } else {
            console.info("Material Emissive Tween: NO Change in data")
        }



    })

    console.info(ctx.tweenStore)

}

// tweenColor used for lights
Api.prototype.tweenColor = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].color.stop(); console.info("Color tween stopped");
    } catch { console.info("No color tween to stop"); }
    var color = en[type].color;
    ctx.tweenStore[en.name].color =
        ctx.app.tween(color)
            .to(new pc.Color(keys[index][type].color.r, keys[index][type].color.g, keys[index][type].color.b), keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('update', function () {
                en[type].color = color;
            })
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].color) {
                    ctx.tweenColor(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Color tween complete");
                }
            }).start();
};

Api.prototype.tweenFloat = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].float.stop(); console.info("Float tween stopped");
    } catch { console.info("No float tween to stop"); }
    ctx.tweenStore[en.name].float =
        en.tween(en[type])
            .to(keys[index][type].float, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].float) {
                    ctx.tweenFloat(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Float tween complete");
                }
            }).start();
};
/*
Api.prototype.tweenCameraFloats = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].float.stop(); console.info("Camera floats tween stopped");
    } catch { console.info("No Camera floats tween to stop"); }
    var d = en.script.orbitCamera;
    ctx.tweenStore[en.name].float =
        ctx.app.tween(d)
            .to(keys[index][type].float, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('update', function () {
                en.script.orbitCamera._targetYaw = d.yaw;
                en.script.orbitCamera._targetPitch = d.pitch;
                en.script.orbitCamera._targetDistance = d.distance;
                en.script.orbitCamera._removeInertia();
                en.script.orbitCamera._updatePosition();
                // en.screen.orbitCamera.reset(d.yaw, d.pitch,d.distance);
            })
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].float.yaw) {
                    ctx.tweenCameraFloats(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Camera floats tween complete");
                }
            }).start();

};
*/
Api.prototype.tweenCameraFov = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].fov.stop(); console.info("Camera FOV tween stopped");
    } catch { console.info("No Camera FOV tween to stop"); }
    ctx.tweenStore[en.name].fov =
        en.tween(en[type])
            .to({ fov: keys[index][type].fov }, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].fov) {
                    ctx.tweenCameraFov(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Camera FOV tween complete");
                }
            }).start();
};

/*
Api.prototype.tweenCameraPivot = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore[en.name].pivotPoint.stop(); console.info("Camera pivotPoint tween stopped");
    } catch { console.info("No Camera pivotPoint tween to stop"); }
    var pivotPoint = en.script.orbitCamera.pivotPoint;
    ctx.tweenStore[en.name].pivotPoint =
        ctx.app.tween(pivotPoint)
            .to(keys[index][type].pivotPoint, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on('update', function () {
                en.script.orbitCamera.pivotPoint = pivotPoint;
                en.script.orbitCamera._removeInertia();
                en.script.orbitCamera._updatePosition();
            })
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].pivotPoint) {
                    ctx.tweenCameraPivot(en, type, keys, index + 1, ctx);
                } else {
                    console.info("Camera pivotPoint tween complete");
                }
            }).start();
};
*/


Api.prototype.tweenSkyboxIntensity = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore._env.skyboxIntensity.stop(); console.info("skyboxIntensity tween stopped");
    } catch { console.info("No skyboxIntensity tween to stop"); }
    var env = { skyboxIntensity: ctx.app.scene.skyboxIntensity };
    ctx.tweenStore._env.skyboxIntensity =
        ctx.app.tween(env)
            .to({ skyboxIntensity: keys[index][type].skyboxIntensity }, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on("update", function () {
                ctx.app.scene.skyboxIntensity = env.skyboxIntensity;
            })
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].skyboxIntensity) {
                    ctx.tweenSkyboxIntensity(en, type, keys, index + 1, ctx);
                } else {
                    console.info("skyboxIntensity tween complete");
                }
            }).start();
};

Api.prototype.tweenSkyboxRotation = function (en, type, keys, index, ctx) {
    try {
        ctx.tweenStore._env.skyboxRotation.stop(); console.info("skyboxRotation tween stopped");
    } catch { console.info("No skyboxRotation tween to stop"); }
    var env = { skyboxRotation: ctx.app.scene.skyboxRotation.clone() };

    var quat = new pc.Quat();
    quat.setFromEulerAngles(keys[index][type].skyboxRotation.x, keys[index][type].skyboxRotation.y, keys[index][type].skyboxRotation.z);

    ctx.tweenStore._env.skyboxRotation =
        ctx.app.tween(env.skyboxRotation)
            .rotate(quat, keys[index].duration ? keys[index].duration : 1.0, keys[index].ease ? pc[keys[index].ease] : pc.SineInOut)
            .on("update", function () {
                ctx.app.scene.skyboxRotation = env.skyboxRotation;
            })
            .on('complete', function () {
                if (keys[index + 1] && keys[index + 1][type].skyboxRotation) {
                    ctx.tweenSkyboxRotation(en, type, keys, index + 1, ctx);
                } else {
                    console.info("skyboxRotation tween complete");
                }
            }).start();
};

Api.prototype.processInlineOverride = function (d, ctx) {
    const extractedData = {};

    // Remove $ from start of string
    const or = d.inlineOverride.replaceAll('$', '');

    // split by ;
    const keyValuePairs = or.split(';');

    //Split pairs by : into objects
    keyValuePairs.map(p => {
        const s = p.split(':');
        extractedData[s[0]] = s[1];
    });

    // Construct a new shot object with animation and apply the shot. 
    // THIS DOES NOT SET THE API SHOTDATA! Which is good:
    // all meshtriggers are still there

    let tmpShot = {};

    const animData = {};

    // Construct a Camera Keyframe
    const cameraKey = {};
    const cameraFloats = {};

    if (extractedData.yaw != null) {
        cameraFloats.yaw = extractedData.yaw;
    }
    if (extractedData.distance != null) {
        cameraFloats.distance = extractedData.distance;
    }
    if (extractedData.pitch != null) {
        cameraFloats.pitch = extractedData.pitch;
    }
    if (Object.keys(cameraFloats).length) {
        cameraKey.float = cameraFloats;
    }


    if (extractedData.pivotPoint != null) {
        cameraKey.pivotPoint = extractedData.pivotPoint;
    }

    if (extractedData.fov != null) {
        cameraKey.fov = extractedData.fov;
    }

    if (Object.keys(cameraKey).length) {
        const cameraData = {
            "type": "camera",
            "keys": [
                {
                    "camera": cameraKey
                }]
        };
        animData[ctx.cameraEntity.name] = cameraData;
    }
    tmpShot.animation = animData;

    // set keepHotspots if it is true
    if (extractedData.keepHotspots) {
        tmpShot.keepHotspots = extractedData.keepHotspots;
    }

    ctx.applyShot(tmpShot, ctx);
};

Api.prototype.handleHotspots = function (d, ctx) {
    // UCX and hotspots are linked. 
    // Clean up
    ctx.ucxNodes.forEach(ucx => {
        ucx.enabled = false;
    });
    Object.keys(ctx.hotspotStore).forEach(objectName => {
        ctx.hotspotStore[objectName].div.remove();
        delete ctx.hotspotStore[objectName];
    });
    if (!d.hotspots) {
        return;
    }
    // Add new ones

    if (Object.keys(d.hotspots).length) {
        ctx.hotspotCount = Object.keys(d.hotspots).length;
        Object.keys(d.hotspots).forEach(hName => {
            const h = d.hotspots[hName];
            // Enable the hotspots UCX node
            ctx.ucxNodes.forEach(ucx => {
                if (ucx.name === hName) {
                    ucx.enabled = true;
                }
                if (ucx.name === hName.replace("UCX_", "UCXLINE_")) {
                    ucx.enabled = true;
                }
            });

            // Delete hotspot if one exists for this object
            if (ctx.hotspotStore[hName] != null) {
                ctx.hotspotStore[hName].div.remove();
            }
            const nh = {
                div: null,
                screenPos: null,
                target: null
            };

            nh.target = ctx.app.root.findByName(hName);

            if (!nh.target) {
                return;
            }

            nh.screenPos = new pc.Vec3();
            // for each object create the hotspot
            nh.div = document.createElement('div');
            nh.div.innerHTML = h.content;

            // Add the HTML 
            nh.div.classList.add('h_container');
            if (h.expanded) {
                nh.div.classList.add('expanded');
            }
            // Set class from shot to div with class .hotspot
            if (h.classes) {
                // find div with class .hotspot
                const hotspotDiv = nh.div.querySelector('.hotspot')
                if (hotspotDiv) {
                    hotspotDiv.classList.add(...h.classes)
                }
            }
            nh.div.dataset.ucx = hName;
            nh.div.style.position = "absolute";
            ctx.hotspotStore[hName] = nh;

            // append to body
            // can be appended somewhere else
            // it is recommended to have some container element
            // to prevent iOS problems of overfloating elements off the screen
            ctx.dWrapper.appendChild(ctx.hotspotStore[hName].div);
            // console.info(ctx.hotspotStore)

        });

    }
};

/**
 * Collect scene nodes that can be enabled/disabled (nodes with 'render' component)
 * Collect hotspot nodes ('UCX_' and 'UCXLINE_' nodes)
 */
Api.prototype.collectEnableNodes = function (rootNode, ctx) {
    rootNode.find((n) => {
        if (n.name.startsWith('UCX_') || n.name.startsWith('UCXLINE_')) {
            ctx.ucxNodes.push(n);
        } else {
            //var renders = n.findComponents('render');
            if (n.render) {
                ctx.enableNodes.push(n);
            }
        }
    });
};

Api.prototype.handleEnabled = function (d, ctx) {
    // replace d.setEnabled with content from oCollections
    // d.setEnabled is list of collection names
    const objectSet = new Set()
    d.setEnabled.forEach(cName => {
        if (cName in ctx.oCollections) {
            ctx.oCollections[cName].forEach(o => {
                objectSet.add(o)
            })
        }
    })
    const listOfObjects = [...objectSet]

    ctx.enableNodes.forEach(obj => {
        switch (d.enabledState) {
            case true:
                // Show only objects specified
                if (listOfObjects.includes(obj.name)) {
                    obj.enabled = true;
                } else {
                    obj.enabled = false;
                }
                break;
            case false:
                // hide objects specified
                if (listOfObjects.includes(obj.name)) {
                    obj.enabled = false;
                } else {
                    obj.enabled = true;
                }
                break;
            default: null;
            //console.info("No enabled state handler")
        }
    })

};


/**
 * 
 * @param {*} d Model data (IModel) 
 * @param {*} ctx 
 */
Api.prototype.buildAnimStateGraph = function (d, ctx) {

    const graph = {
        layers: [{
            name: "Base",
            blendType: "OVERWRITE",
            weight: 1,
            defaultState: "Initial State",
            states: [
                {
                    name: "START",
                    speed: 1,
                    //nodeType: 3
                },
                {
                    name: "END",

                    //nodeType: 5
                },
                {
                    name: "ANY",
                    //speed: 1,
                    //nodeType: 4
                },
                {
                    name: "Initial State",
                    speed: 1,
                    loop: false,
                    defaultState: true,
                    //nodeType: 1
                },
            ],
            transitions: [
                {
                    from: "START",
                    to: "Initial State",
                    defaultTransition: true,
                    conditions: []
                }
            ]
        }], parameters: {}
    };


    for (let i = 0; i < d.animation.length; i++) {
        let dur;
        if (d.animation[i].transitionDuration != null) {
            dur = d.animation[i].transitionDuration
        } else {
            dur = 1;
        }

        const state = {
            name: d.animation[i].name,
            speed: 1,
            loop: d.animation[i].loop,
            //nodeType: 0
        }
        const tr = {
            exitTime: null,
            from: "ANY",
            to: d.animation[i].name,
            conditions: [
                {
                    parameterName: d.animation[i].name,
                    predicate: "EQUAL_TO",
                    value: true
                }
            ],
            time: dur,
            interruptionSource: "NEXT_STATE"
        }

        const param = {
            name: d.animation[i].name,
            type: "TRIGGER",
            value: false
        }

        graph.layers[0].states.push(state);
        graph.layers[0].transitions.push(tr);
        graph.parameters[d.animation[i].name] = param;

    }

    return graph;

};

Api.prototype.initLoadingIndicator = function (ctx) {
    if (ctx.loadingIndicator) {
        ctx.loadingIndicator.remove()
    }

    ctx.loadingIndicator = document.createElement('div');
    ctx.loadingIndicator.innerHTML = '<span>Loading 3D</span>';
    ctx.loadingIndicator.classList.add('loadingIndicator');
    ctx.dWrapper.appendChild(ctx.loadingIndicator);

};
