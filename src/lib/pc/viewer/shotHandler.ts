import cloneDeep from 'lodash.clonedeep';
import type { IShot, IModel } from '../../types/interface';

import Loader from './loader';

import { MeshInstance, Entity, Vec3, Quat } from 'playcanvas';
import type { App } from './app';


class ShotHandler {
    app: App;
    shotData: IShot | null = null;
    dWrapper: HTMLDivElement;
    loader: Loader;

    // We only want to run model load once at start of app and ignore on subsequent shots
    loadWasStarted = false;
    modelLoaded = false;

    // Hotspots
    // Object with objectNames as keys {"Cube":{div:HTMLEL, screenPos:pcVec2, target:entity}}. Will be updated in postUpdate function.
    hotspotStore = {};

    // Store the inital shot, so we can access 
    // things like env, envSphere if they are missing
    // in a subsequent shot. 
    // This is especially important, if we trigger shots
    // during the loading phase. If a triggered shot has no
    // environment, the view will remain dark.
    initialShot: IShot | null = null;


    dev = false;
    visMeshInstances = [];
    visMin = 0.0;
    toShow: Array<MeshInstance> = []; // Array<MeshInstace>
    toHide: Array<MeshInstance> = [];// Array<MeshInstace>
    cameraEntity: Entity;
    tweenStore = { "_env": {} };
    enableNodes: Array<Entity> = []; // Scene nodes that can be enabled/disabled from a shot
    ucxNodes: Array<Entity> = []; // Collider/Hotspot nodes
    hotspotCount = 0; // amount of visible hotspots
    pixelRatio = window.devicePixelRatio;
    cameraPivotPointTarget = null;
    cameraTransition = false;
    cameraPivotPointLerp = new Vec3();
    cameraTransitionTimer = 0.0;
    cameraTransitionSpeed = 1.0;
    oCollections = {}; // object collections
    loadingIndicator = null; // the loading text
    gltfRoot: Entity


    constructor(app: App, camera: Entity, dWrapper: HTMLDivElement) {
        this.app = app;
        this.cameraEntity = camera;
        this.dWrapper = dWrapper;

        // Loader
        this.loader = new Loader(this.app, camera);
    }


    async applyShot() {
        console.info("Apply Shot")
        if (!this.shotData) {
            console.info("No shot data")
            return;
        }
        if (!this.modelLoaded) {
            // Only proceed if no loading is in progress
            if (this.loadWasStarted) {
                console.info("Load in progress...")
                return;
            }
            if (this.shotData?.load?.length) {
                this.loadWasStarted = true;

                // Store as inital shot data so we can
                // use this data for missing properties
                // in subsequen shots:
                this.initialShot = cloneDeep(this.shotData);


                // Collect all promises
                const promises = [];

                this.initialShot.load.forEach((modelData: IModel) => {
                    promises.push(this.loader.loadModel(modelData));
                });
                Promise.all(promises).then((res) => {
                    this.modelLoaded = true
                    console.info("Resolved, model loaded!");

                    // Bit hacky: last in array of res has all nodes
                    this.enableNodes = res.slice(-1)[0].enableNodes;
                    this.ucxNodes = res.slice(-1)[0].ucxNodes;
                    this.gltfRoot = res.slice(-1)[0].gltfRoot;
                    console.info(this.gltfRoot)
                    this.applyShot();
                })
            }
            // We dont want to process the rest of the shot
            // until the model is loaded. Therefore early return here. 
            // The load function will call the shot again when it is done.
            console.info("No model loaded.")
            return;
        }

        // Model and Animation is loaded, we can apply the shot
        console.info("Applying the shot");

        // If the shot has no data related to env and EnvSphere,
        // copy the data from inital shot:
        if (!this.shotData.animation?.env || !this.shotData.animation?.EnvSphere) {
            if (!this.shotData.animation) {
                this.shotData["animation"] = {}
            }

            if (!this.shotData.animation?.env && this.initialShot?.animation?.env) {
                this.shotData.animation["env"] = this.initialShot.animation.env;
            }
            if (!this.shotData.animation?.EnvSphere && this.initialShot?.animation?.EnvSphere) {
                this.shotData.animation["EnvSphere"] = this.initialShot.animation.EnvSphere;
            }
        }

        if (this.shotData.animation) {
            Object.keys(this.shotData.animation).forEach(entityName => {

                const anim = this.shotData.animation[entityName];
                const en = this.app.root.findByName(entityName);

                // If there have not been any tweens stored, 
                // initialise in tweenStore
                if (!(entityName in this.tweenStore)) {
                    this.tweenStore[entityName] = {};
                }

                switch (anim.type) {
                    case 'camera':
                        this.tweenStore['camera'] = {}
                        if (anim.keys[0]?.camera?.float?.yaw != null) {
                            this.tweenStore.camera['yaw'] = anim.keys[0].camera.float.yaw;

                        }
                        if (anim.keys[0]?.camera?.float?.pitch != null) {
                            this.tweenStore.camera['pitch'] = anim.keys[0].camera.float.pitch;
                        }

                        if (anim.keys[0]?.camera?.float?.distance != null) {
                            this.tweenStore.camera['distance'] = anim.keys[0].camera.float.distance;
                        }

                        if (anim.keys[0]?.camera?.fov != null) {
                            // SPECIAL CASE!
                            // In general, the camera is handled through the orbitCamera script. 
                            // Animations are not tweened. 
                            // This is not the case for FOV; it needs to be tweened. Therefore,
                            // we store it at the top level, not inside 'camera'
                            const currentFOV = this.cameraEntity.camera.fov;
                            console.info("FOV start ", currentFOV)
                            this.tweenStore['fov'] = {
                                valueStart: currentFOV,
                                valueEnd: anim.keys[0]?.camera?.fov,
                                valueDelta: anim.keys[0]?.camera?.fov - currentFOV,
                                duration: 50,
                                progress: 0,
                                currentValue: currentFOV,
                                ease: 'easeInOutSine'
                            };
                        }

                        if (anim.keys[0]?.camera?.pivotPoint != null) {
                            this.tweenStore.camera['pivotPoint'] = anim.keys[0].camera.pivotPoint;
                        }

                        break;
                    case 'env':
                        if (anim.keys[0]?.env?.skyboxIntensity != null ) {
                            this.tweenStore['skyboxIntensity'] = {
                                valueStart: this.app.scene.skyboxIntensity,
                                valueEnd: anim.keys[0].env.skyboxIntensity,
                                valueDelta: anim.keys[0].env.skyboxIntensity - this.app.scene.skyboxIntensity,
                                duration: 50,
                                progress: 0,
                                currentValue: this.app.scene.skyboxIntensity,
                                ease: 'easeInOutSine'
                            };
                        }
                        if (anim.keys[0]?.env?.skyboxRotation != null) {
                            const initQuat = this.app.scene.skyboxRotation.clone();
                            const targetQuat = new Quat();
                            console.info("anim.keys[0].env.skyboxRotation.x", anim.keys[0].env.skyboxRotation.x)
                            targetQuat.setFromEulerAngles(anim.keys[0].env.skyboxRotation.x, anim.keys[0].env.skyboxRotation.y, anim.keys[0].env.skyboxRotation.z);

                            this.tweenStore['skyboxRotation'] = {
                                valueStart: initQuat,
                                valueEnd: targetQuat,
                                valueDelta: 1, // interpolation point for slerp function
                                duration: 50,
                                progress: 0,
                                currentValue: 0, //interpolation point start for slerp function
                                ease: 'easeInOutSine'
                            };
                        }
                        break;
                    case 'geo':
                        if (anim.keys[0]?.geo?.material) {
                            // Tween Material
                        }
                        break;
                    default:
                        console.info("No Animation Handler for type ", anim.type)
                }
            })
        }

        // Set camera limits
        // max dist, focalPoint max X,Y,Z

        // Set object visibilites
        if (this.shotData.setEnabled != null && this.shotData.enabledState != null) {
            this.handleEnabled();
        }

        if (this.shotData.vis) {
            //this.updateVisibility();
        }
        if (this.shotData.animGraph) {
            this.animGraphTrigger();
        }

        if (!this.shotData.keepHotspots) {
            this.handleHotspots();
        }

        if(this.shotData.background){
            // find el
            // set style
            const el = document.getElementById("ez_view")
            if(el){
                console.info(el.style.backgroundImage)
                console.info("Animate BG")
                const x = this.shotData.background.X
                const y = this.shotData.background.Y
                const cols = this.shotData.background.colors.join(",")
                
                el.style.backgroundImage = `radial-gradient(circle at ${x}% ${y}%, ${cols}),url("grain.jpg")`
            }
        }


    }

    mouseDownHandler(event) {
        if (event.target.nodeName != "CANVAS") {
            return;
        }
        const hotspotDivs = document.querySelectorAll('[data-ucx');
        hotspotDivs.forEach((hItem) => {
            hItem.classList.add("noHover");
        });
        console.info("noHover added")
    }


    mouseUpHandler(event) {
        const hotspotDivs = document.querySelectorAll('[data-ucx');
        hotspotDivs.forEach((hItem) => {
            hItem.classList.remove("noHover");
        });
        console.info("noHover removed")
    }


    handleEnabled() {
        console.info("Handle enabled")
        // shotData.setEnabled is list of collection names.
        // Replace it with content from oCollections
        const objectSet = new Set()
        this.shotData.setEnabled.forEach(cName => {
            if (cName in this.oCollections) {
                this.oCollections[cName].forEach(o => {
                    objectSet.add(o)
                })
            }
        })
        const listOfObjects = [...objectSet]

        this.enableNodes.forEach(obj => {
            switch (this.shotData.enabledState) {
                case true:
                    // Show only objects specified
                    if (listOfObjects.includes(obj.name)) {
                        obj.enabled = true;
                    } else {
                        obj.enabled = false;
                    }
                    break;
                case false:
                    // hide objects specified
                    if (listOfObjects.includes(obj.name)) {
                        obj.enabled = false;
                    } else {
                        obj.enabled = true;
                    }
                    break;
                default: null;
                //console.info("No enabled state handler")
            }
        })

    };

    animGraphTrigger() {
        console.info("animGraphTrigger")
        Object.keys(this.shotData.animGraph).forEach(entityName => {
            const anm = this.shotData.animGraph[entityName];

            const en = this.app.root.findByName(entityName);
            if (!en) { return; }
            if (!anm.name || !anm.value) {
                return;
            }
            const stateName = anm.name;
            console.info("Set trigger ", stateName, anm.value)
            en.anim.setTrigger(stateName, anm.value);

            if (anm.speed != null) {
                en.anim.speed = anm.speed;
            }
            // console.info(this.app.renderNextFrame)
            // setTimeout(() => {
            //     console.info("Set cube jump")
            //     en.anim.setTrigger("cube_jump", 1);
            // }, 3000)

        });
    };

    handleHotspots() {
        console.info("Handle Hotspots")
        // UCX and hotspots are linked. 
        // Clean up
        this.ucxNodes.forEach(ucx => {
            ucx.enabled = false;
        });

        Object.keys(this.hotspotStore).forEach(objectName => {
            this.hotspotStore[objectName].div.remove();
            delete this.hotspotStore[objectName];
        });
        if (!this.shotData?.hotspots) {
            return;
        }

        // Add new ones
        if (Object.keys(this.shotData.hotspots).length) {
            this.hotspotCount = Object.keys(this.shotData.hotspots).length;

            Object.keys(this.shotData.hotspots).forEach(hName => {
                const h = this.shotData.hotspots[hName];
                // Enable the hotspots UCX node
                this.ucxNodes.forEach(ucx => {
                    if (ucx.name === hName) {
                        ucx.enabled = true;
                    }
                    if (ucx.name === hName.replace("UCX_", "UCXLINE_")) {
                        ucx.enabled = true;
                    }
                });

                // Delete hotspot if one exists for this object
                if (this.hotspotStore[hName] != null) {
                    this.hotspotStore[hName].div.remove();
                }
                const nh = {
                    div: null,
                    screenPos: null,
                    target: null
                };

                nh.target = this.app.root.findByName(hName);

                if (!nh.target) {
                    return;
                }

                nh.screenPos = new Vec3();
                // for each object create the hotspot
                nh.div = document.createElement('div');
                nh.div.innerHTML = h.content;

                // Add the HTML 
                nh.div.classList.add('h_container');
                if (h.expanded) {
                    nh.div.classList.add('expanded');
                }

                // Set class from shot to div with class .hotspot
                if (h.classes) {
                    // find div with class .hotspot
                    const hotspotDiv = nh.div.querySelector('.hotspot')
                    if (hotspotDiv) {
                        hotspotDiv.classList.add(...h.classes)
                    }
                }

                nh.div.dataset.ucx = hName;
                nh.div.style.position = "absolute";
                this.hotspotStore[hName] = nh;

                // append to body
                // can be appended somewhere else
                // it is recommended to have some container element
                // to prevent iOS problems of overfloating elements off the screen
                this.dWrapper.appendChild(this.hotspotStore[hName].div);
                // console.info(ctx.hotspotStore)

            });

        }
    };


}

export default ShotHandler