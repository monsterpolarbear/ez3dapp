
import { ScriptType, Vec2 } from 'playcanvas';


export default class UvOffset extends ScriptType {

    public initialize(): void {
        console.info("Initialize UvOffset")
        // Find the bone entity
        this.bone = this.app.root.findByName(this.boneName);
        this.material = this.entity.render.meshInstances[0].material;
        this.offset = new Vec2();
    }

    public update(dt: number): void {
        // Offset is bone.axis
        if (this.bone && this.material) {
            this.offset[this.targetOffsetAxis] = this.bone.getLocalPosition()[this.sourceBoneAxis] * this.speed;
            //this.material.diffuseMapOffset = this.offset;
            this.material.opacityMapOffset = this.offset;
            this.material.emissiveMapOffset = this.offset;
            this.material.update();
        }
    }

}