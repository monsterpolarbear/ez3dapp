import { MeshoptDecoder } from './meshopt_decoder.module.js';
import type { IModel } from '../../types/interface';
import { loadingSpinner } from '../../store';
import {
    path,
    Asset,
    Entity,
    Vec3,
    registerScript,
    BLEND_ADDITIVEALPHA, AnimEvents
} from 'playcanvas';
import type { App } from './app';
import LookAt from './lookAt.js';
import UvOffset from './uvOffset.js';

import ftConfig from '../../../../ft-pc-config.js';

class Loader {
    app: App;
    cameraEntity: Entity;
    ucxNodes: Array<Entity> = [];
    enableNodes: Array<Entity> = [];
    gltfRoot: Entity;
    loading_rig: Entity;
    envSphere: Entity;
    modelScale = 1;
    modelXRScale = 1;

    constructor(app: App, camera: Entity) {
        this.app = app;
        this.cameraEntity = camera;

        registerScript(LookAt, "lookAt");
        LookAt.attributes.add('cameraEntity', { type: 'entity' });

        registerScript(UvOffset, 'uvOffset');
        UvOffset.attributes.add('boneName', { type: 'string' });
        UvOffset.attributes.add('speed', { type: 'number' });
        UvOffset.attributes.add('sourceBoneAxis', { type: 'string' });
        UvOffset.attributes.add('targetOffsetAxis', { type: 'string' });
        UvOffset.attributes.add('bone', { type: 'entity' });
        //UvOffset.attributes.add('material', { type: 'entity' });
        UvOffset.attributes.add('offset', { type: 'vec2' });



    }

    // load gltf model given its url and list of external urls
    private loadGltf(gltfUrl: File, externalUrls: Array<File>, finishedCallback: (err: string | null, asset: Asset) => void) {

        // provide buffer view callback so we can handle models compressed with MeshOptimizer
        // https://github.com/zeux/meshoptimizer
        const processBufferView = function (gltfBuffer: any, buffers: Array<any>, continuation: (err: string, result: any) => void) {
            if (gltfBuffer.extensions && gltfBuffer.extensions.EXT_meshopt_compression) {
                const extensionDef = gltfBuffer.extensions.EXT_meshopt_compression;

                MeshoptDecoder.ready.then(() => {
                    const byteOffset = extensionDef.byteOffset || 0;
                    const byteLength = extensionDef.byteLength || 0;

                    const count = extensionDef.count;
                    const stride = extensionDef.byteStride;

                    const result = new Uint8Array(count * stride);
                    const source = new Uint8Array(buffers[extensionDef.buffer].buffer,
                        buffers[extensionDef.buffer].byteOffset + byteOffset,
                        byteLength);

                    MeshoptDecoder.decodeGltfBuffer(result, count, stride, source, extensionDef.mode, extensionDef.filter);

                    continuation(null, result);
                });
            } else {
                continuation(null, null);
            }
        };

        const processImage = function (gltfImage: any, continuation: (err: string, result: any) => void) {
            const u: File = externalUrls.find((url) => {
                return url.filename === path.normalize(gltfImage.uri || "");
            });
            if (u) {
                const textureAsset = new Asset(u.filename, 'texture', {
                    url: u.url,
                    filename: u.filename
                });
                textureAsset.on('load', () => {
                    continuation(null, textureAsset);
                });
                this.app.assets.add(textureAsset);
                this.app.assets.load(textureAsset);
            } else {
                continuation(null, null);
            }
        };

        const postProcessImage = (gltfImage: any, textureAsset: Asset) => {
            // max anisotropy on all textures
            textureAsset.resource.anisotropy = this.app.graphicsDevice.maxAnisotropy;
        };

        const processBuffer = function (gltfBuffer: any, continuation: (err: string, result: any) => void) {
            const u = externalUrls.find((url) => {
                return url.filename === path.normalize(gltfBuffer.uri || "");
            });
            if (u) {
                const bufferAsset = new Asset(u.filename, 'binary', {
                    url: u.url,
                    filename: u.filename
                });
                bufferAsset.on('load', () => {
                    continuation(null, new Uint8Array(bufferAsset.resource));
                });
                this.app.assets.add(bufferAsset);
                this.app.assets.load(bufferAsset);
            } else {
                continuation(null, null);
            }
        };

        const containerAsset = new Asset(gltfUrl.filename, 'container', gltfUrl, null, {

            bufferView: {
                processAsync: processBufferView.bind(this)
            },
            image: {
                processAsync: processImage.bind(this),
                postprocess: postProcessImage
            },
            buffer: {
                processAsync: processBuffer.bind(this)
            }
        });
        containerAsset.on('load', () => {
            finishedCallback(null, containerAsset);
        });
        containerAsset.on('error', (err: string) => {
            finishedCallback(err, containerAsset);
        });

        loadingSpinner.set(true)

        this.app.assets.add(containerAsset);
        this.app.assets.load(containerAsset);
    }

    loadModel(modelData: IModel) {

        return new Promise((resolve, reject) => {

            // If we end up in this function, we can only 
            //continue with a model path
            if (!modelData.model) {
                reject();
            }

            const file: File = {
                url: modelData.model,
                filename: modelData.model.substring(modelData.model.lastIndexOf('/') + 1)
            }

            const files = [] as Array<File>;



            // .loadGltf comes from playcanvas gltf viewer.
            // External urls are empty for our use case
            this.loadGltf(file, [], (err, asset) => {
                if (asset) {
                    const renderRootEntity = asset.resource.instantiateRenderEntity();

                    // Add to scene
                    // Create a new entity with name of file, 
                    // add stateGraph to this node:
                    this.gltfRoot = new Entity();
                    this.gltfRoot.enabled = false;
                    const rootName = file.filename.replace('.glb', '');
                    this.gltfRoot.name = rootName;
                    this.app.root.addChild(this.gltfRoot);

                    this.gltfRoot.addChild(renderRootEntity);

                    // Set scale
                    if (modelData.scale) {
                        this.modelScale = modelData.scale;
                        console.info("modelScale set to ", this.modelScale)
                    }
                    if (modelData.xrScale) {
                        this.modelXRScale = modelData.xrScale;
                        console.info("xr modelScale set to ", this.modelXRScale)
                    }

                    const scale = new Vec3(this.modelScale, this.modelScale, this.modelScale);
                    renderRootEntity.setLocalScale(scale);


                    // Set Lookat
                    if (modelData.lookAt?.length) {
                        modelData.lookAt.forEach(o => {
                            // find entity
                            const found = this.app.root.findByName(o);

                            if (found) {
                                // add script component
                                found.addComponent("script");

                                found.script.create('lookAt', {
                                    attributes: {
                                        cameraEntity: this.cameraEntity
                                    }
                                });
                            }
                        })
                    };

                    //Add Uv offset script
                    // Add the script to the entity
                    if (modelData.uvOffset?.length) {
                        modelData.uvOffset.forEach(item => {
                            const found = this.app.root.findByName(item.objectName);
                            if (found?.render?.meshInstances[0]?.material) {
                                console.info("UvOffset Material:")
                                console.info(found.render.meshInstances[0].material)
                                found.addComponent('script');
                                found.script.create('uvOffset', {
                                    attributes: {
                                        boneName: item.boneName,
                                        speed: item.speed,
                                        sourceBoneAxis: item.sourceAxis,
                                        targetOffsetAxis: item.targetAxis
                                    }
                                });

                                console.info("UVOffset: Setup material");
                                found.render.meshInstances.forEach(function (meshInstance) {
                                    meshInstance.material.emissiveIntensity = 1;
                                    if (item.cull) {
                                        meshInstance.material.cull = item.cull;
                                    } else {
                                        meshInstance.material.cull = 0;
                                    }
                                    meshInstance.material.blendType = BLEND_ADDITIVEALPHA;
                                    meshInstance.material.update();
                                });
                                // Setup material
                                // opacity blend type=alpha
                                // is map in emissive?
                                // depth test?
                                // cull mode?

                                console.info("UVOffset: Render component setup")
                                console.info(found.render)
                                found.render.castShadows = false;
                                found.render.receiveShadows = false;
                                found.render.castShadowsLightmap = false;
                            } else {
                                console.info("UvOffset: object has no material")
                            }
                        });
                    }

                    // Set Material Params
                    if (modelData.material?.length) {
                        modelData.material.forEach(m => {
                            const found = this.app.root.findByName(m.objectName);
                            if (found.render?.meshInstances[0]?.material) {
                                found.render.meshInstances?.forEach(function (meshInstance) {
                                    if (m.cull) {
                                        meshInstance.material.cull = m.cull;
                                    }
                                })
                            }
                        })

                    }

                    // Process Collider
                    this.gltfRoot.forEach(c => {
                        if (c.name.startsWith('UCX_')) {
                            const assetID = c.render.asset;
                            try {
                                // Remove render component if exists
                                c.removeComponent('render');

                            } catch (error) {
                                console.info("No render component to remove");
                            }
                            try {
                                // Remove collision if exists
                                c.removeComponent('collision');
                            } catch (error) {
                                console.info("No collision component to remove");
                            }
                            c.addComponent('collision', {
                                type: 'mesh', renderAsset: assetID
                            });
                        }
                    });

                    // Process Animations
                    if (modelData.animation?.length) {
                        // Build anim stateGraph
                        const stateGraph = this.buildAnimStateGraph(modelData);

                        // Prepare Anim Component
                        this.gltfRoot.addComponent('anim');
                        this.gltfRoot.anim.loadStateGraph(stateGraph);

                        const animLayer = this.gltfRoot.anim.baseLayer;
                        console.info("StateGraph loaded")

                        // Collect all animations to load into object
                        const animsToLoad = {};
                        modelData.animation.forEach(a => {
                            animsToLoad[a.name] = false;
                        });

                        // Add each animations
                        for (let i = 0; i < modelData.animation.length; i++) {
                            const a = modelData.animation[i];

                            const file: File = {
                                url: a.path,
                                filename: a.path.substring(a.path.lastIndexOf('/') + 1)
                            }
                            this.loadGltf(file, [], (err, asset) => {
                                // Callback for when animation is loaded

                                if (err) {
                                    console.error(err);
                                    return;
                                }
                                // add animation to anim state graph
                                console.info("Loaded animation", a.name);
                                if (i === 0) {
                                    // Add first Animation to Initial State
                                    animLayer.assignAnimation("Initial State",
                                        asset.resource.animations[0].resource);
                                }
                                animLayer.assignAnimation(a.name,
                                    asset.resource.animations[0].resource, 1, a.loop);
                                console.info("Assigned anim ", a.name)
                                animsToLoad[a.name] = true;

                                // Finish up if all anims are true
                                if (Object.values(animsToLoad).every(item => item === true)) {

                                    this.collectEnableNodes(this.gltfRoot);
                                    // ctx.collectVisMeshInstances(ctx); // Vis is currently turned off
                                    this.loading_rig.enabled = false;
                                    this.gltfRoot.enabled = true;
                                    resolve({ enableNodes: this.enableNodes, ucxNodes: this.ucxNodes, gltfRoot: this.gltfRoot });
                                }

                            });
                        }
                    } else {
                        console.info("No Animation to Load. Finishing up...")
                        this.loading_rig.enabled = false;
                        this.gltfRoot.enabled = true;

                        resolve({ enableNodes: this.enableNodes, ucxNodes: this.ucxNodes, gltfRoot: this.gltfRoot });
                    }
                }

            });

        })
    }

    buildAnimStateGraph(modelData: IModel) {
        const graph = {
            layers: [{
                name: "Base",
                blendType: "OVERWRITE",
                weight: 1,
                defaultState: "Initial State",
                states: [
                    {
                        name: "START",
                        speed: 1,
                        //nodeType: 3
                    },
                    {
                        name: "END",

                        //nodeType: 5
                    },
                    {
                        name: "ANY",
                        //speed: 1,
                        //nodeType: 4
                    },
                    {
                        name: "Initial State",
                        speed: 1,
                        loop: false,
                        defaultState: true,
                        //nodeType: 1
                    },
                ],
                transitions: [
                    {
                        from: "START",
                        to: "Initial State",
                        defaultTransition: true,
                        conditions: []
                    }
                ]
            }], parameters: {}
        };


        for (let i = 0; i < modelData.animation.length; i++) {
            let dur;
            if (modelData.animation[i].transitionDuration != null) {
                dur = modelData.animation[i].transitionDuration
            } else {
                dur = 1;
            }

            const state = {
                name: modelData.animation[i].name,
                speed: 1,
                loop: modelData.animation[i].loop,
                //nodeType: 0
            }
            const tr = {
                exitTime: null,
                from: "ANY",
                to: modelData.animation[i].name,
                conditions: [
                    {
                        parameterName: modelData.animation[i].name,
                        predicate: "EQUAL_TO",
                        value: true
                    }
                ],
                time: dur,
                interruptionSource: "NEXT_STATE"
            }

            const param = {
                name: modelData.animation[i].name,
                type: "TRIGGER",
                value: false
            }

            graph.layers[0].states.push(state);
            graph.layers[0].transitions.push(tr);
            graph.parameters[modelData.animation[i].name] = param;

        }

        return graph;

    }

    /**
     * Collect scene nodes that can be enabled/disabled (nodes with 'render' component)
     * Collect hotspot nodes ('UCX_' and 'UCXLINE_' nodes)
     */
    collectEnableNodes(rootNode) {
        rootNode.find((n) => {
            if (n.name.startsWith('UCX_') || n.name.startsWith('UCXLINE_')) {
                this.ucxNodes.push(n);
            } else {
                //var renders = n.findComponents('render');
                if (n.render) {
                    this.enableNodes.push(n);
                }
            }
        });
    };

    /**
     * Loads loading.glb 
     */
    loaderAndEnv() {
        return new Promise((resolve, reject) => {

            console.info("Loading loading and Env");
            const file: File = {
                url: "d/loading.glb",
                filename: "loading.glb"
            }

            this.loadGltf(file, [], (err, asset) => {
                if (asset) {
                    const renderRootEntity = asset.resource.instantiateRenderEntity();
                    this.loading_rig = renderRootEntity;
                    const animTracks = [];
                    asset.resource.animations.forEach((a: any) => {
                        animTracks.push(a.resource);
                    });


                    // Play animation
                    renderRootEntity.addComponent('anim', {
                        activate: true,
                        speed: 1
                    });
                    renderRootEntity.anim.rootBone = renderRootEntity;

                    animTracks.forEach((t: any, i: number) => {
                        // add an event to each track which transitions to the next track when it ends
                        t.events = new AnimEvents([
                            {
                                name: "transition",
                                time: t.duration,
                                nextTrack: "track_" + (i === animTracks.length - 1 ? 0 : i + 1)
                            }
                        ]);
                        renderRootEntity.anim.assignAnimation('track_' + i, t);
                        //this.animationMap[t.name] = 'track_' + i;
                    });


                    // Setup UV Offset
                    const found = renderRootEntity.findByName("loading_geo");
                    console.info("found loading_geo: ", found)
                    if (found?.render?.meshInstances[0]?.material) {
                        console.info("loading_geo UvOffset Material:")
                        console.info(found.render.meshInstances[0].material)
                        found.addComponent('script');
                        found.script.create('uvOffset', {
                            attributes: {
                                boneName: "markerCTRL",
                                speed: 1,
                                sourceBoneAxis: "x",
                                targetOffsetAxis: "x"
                            }
                        });
                        console.info("UVOffset: Setup material");
                        found.render.meshInstances.forEach(function (meshInstance) {
                            meshInstance.material.emissiveIntensity = 1;
                            meshInstance.material.cull = 0;
                            meshInstance.material.blendType = BLEND_ADDITIVEALPHA;
                            meshInstance.material.update();
                        });

                        console.info("UVOffset: Render component setup")
                        console.info(found.render)
                        found.render.castShadows = false;
                        found.render.receiveShadows = false;
                        found.render.castShadowsLightmap = false;
                    } else {
                        console.info("UvOffset: object has no material");
                        null;
                    }

                    // TODO add script to Sphere to follow camera position if set in ft-config
                    this.envSphere = renderRootEntity.findByName("EnvSphere");
                    if (this.envSphere) {
                        if (ftConfig.envFollowCamPos && this.envSphere) {
                            this.envSphere.setPosition(this.cameraEntity.getPosition());
                            this.cameraEntity.addChild(this.envSphere);
                        } else {
                            // parent to scene root
                            this.app.root.addChild(this.envSphere);
                        }
                    }

                    // TODO add rigid body to envSphere so we can determine if user clicked envSphere
                    // const envSphere = renderRootEntity.findByName("EnvSphere");
                    // TODO needs ammo.js for picking:
                    //envSphere.addComponent("rigidbody");
                    // envSphere.addComponent('collision');
                    console.info("Loading and Env added to Scene");
                    this.app.root.addChild(renderRootEntity);
                    resolve(true);
                }
            })
        })
    }


}

export default Loader