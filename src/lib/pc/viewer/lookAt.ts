
import { ScriptType, Entity } from 'playcanvas';

export default class LookAt extends ScriptType {

    //cameraEntity: Entity;

    public update(dt: number): void {
        if (!this.cameraEntity) {
            return
        }
        const cameraPosition = this.cameraEntity.getPosition();

        // Always face the camera
        this.entity.lookAt(cameraPosition);
    }


}

