import { get } from 'svelte/store';
import { xrActive, xrSupported, pixelScaleMult, loadingSpinner, loadingError, appStarted } from '../../store';
import ftConfig from '../../../../ft-pc-config';

import {
    ADDRESS_CLAMP_TO_EDGE,
    FILLMODE_NONE,
    FILTER_LINEAR,
    FILTER_LINEAR_MIPMAP_LINEAR,
    FILTER_NEAREST,
    LAYERID_DEPTH,
    LAYERID_SKYBOX,
    PIXELFORMAT_DEPTH,
    PIXELFORMAT_RGBA8,
    RENDERSTYLE_SOLID,
    RENDERSTYLE_WIREFRAME,
    RESOLUTION_AUTO,
    TEXTURETYPE_DEFAULT,
    TEXTURETYPE_RGBM,
    // TONEMAP_LINEAR,
    // TONEMAP_FILMIC,
    // TONEMAP_HEJL,
    TONEMAP_ACES,
    XRSPACE_LOCALFLOOR,
    XRTYPE_AR,
    math,
    path,
    reprojectTexture,
    AnimEvents,
    AnimTrack,
    Asset,
    BoundingBox,
    Color,
    Entity,
    EnvLighting,
    GraphNode,
    Mat4,
    Mesh,
    MeshInstance,
    MorphInstance,
    MorphTarget,
    Mouse,
    Quat,
    RenderComponent,
    RenderTarget,
    Texture,
    TouchDevice,
    Vec3,
    Vec4,
    WebglGraphicsDevice
} from 'playcanvas';

import { App } from './app';

//import { Observer } from '@playcanvas/observer';

//import { MiniStats } from 'playcanvas-extras';

// import { getAssetPath } from './helpers';
// import { DropHandler } from './drop-handler';
import type { MorphTargetData, File, HierarchyNode } from './types';
// import { DebugLines } from './debug';

import { Multiframe } from './multiframe'; // Multisample AA
import { ReadDepth } from './read-depth';
import { OrbitCamera, OrbitCameraInputMouse, OrbitCameraInputTouch } from './orbit-camera';
// import { PngExporter } from './png-exporter';

import ShotHandler from './shotHandler';

const defaultSceneBounds = new BoundingBox(new Vec3(0, 1, 0), new Vec3(1, 1, 1));

class Viewer {
    app: App;
    dWrapper: HTMLDivElement;
    api: Api | null = null;
    prevCameraMat: Mat4;
    camera: Entity;
    orbitCamera: OrbitCamera;
    orbitCameraInputMouse: OrbitCameraInputMouse;
    orbitCameraInputTouch: OrbitCameraInputTouch;
    cameraFocusBBox: BoundingBox | null;
    cameraPosition: Vec3 | null;
    light: Entity;
    sceneRoot: Entity;
    debugRoot: Entity;
    entities: Array<Entity>;
    entityAssets: Array<{ entity: Entity, asset: Asset }>;
    assets: Array<Asset>;
    meshInstances: Array<MeshInstance>;
    animTracks: Array<AnimTrack>;
    animationMap: Record<string, string>;
    firstFrame: boolean;
    skyboxLoaded: boolean;
    skyboxRotation = new Quat();
    animSpeed: number;
    animTransition: number;
    animLoops: number;
    showWireframe: boolean;
    showBounds: boolean;
    showSkeleton: boolean;
    showAxes: boolean;
    showGrid: boolean;
    normalLength: number;
    skyboxMip: number;
    dirtyWireframe: boolean;
    dirtyBounds: boolean;
    dirtySkeleton: boolean;
    dirtyGrid: boolean;
    dirtyNormals: boolean;
    sceneBounds: BoundingBox;
    miniStats: MiniStats;
    suppressAnimationProgressUpdate: boolean;

    selectedNode: GraphNode | null;

    // see https://github.com/playcanvas/model-viewer/pull/156
    multiframe: Multiframe | null;
    multiframeBusy = false;
    multisample = false;
    multisampleSupported: boolean;
    readDepth: ReadDepth = null;
    cursorWorld = new Vec3();

    loadTimestamp?: number = null;

    /////// Shot specific
    shotHandler: ShotHandler | null = null;
    camTarget = new Vec3(); // Stores the current camera yaw, pitch, distance

    rect;

    resizeTimer;



    constructor(canvas: HTMLCanvasElement, dWrapperEl: HTMLDivElement) {
        // create the application
        const app = new App(canvas, {
            mouse: new Mouse(canvas),
            touch: new TouchDevice(canvas),
            graphicsDeviceOptions: {
                preferWebGl2: true,
                alpha: true,
                // the following aren't needed since we're rendering to an offscreen render target
                // and would only result in extra memory usage.
                antialias: false,
                depth: false,
                preserveDrawingBuffer: true
            }
        });
        this.app = app;
        this.dWrapper = dWrapperEl;
        this.rect = this.dWrapper.getBoundingClientRect();
        this.dWrapper.addEventListener("mousedown", this.mouseDownHandler);
        this.dWrapper.addEventListener("mouseup", this.mouseUpHandler);
        // clustered not needed and has faster startup on windows
        this.app.scene.clusteredLightingEnabled = false;


        // xr is supported
        if (this.app.xr.supported) {
            app.xr.on("available:" + XRTYPE_AR, (available) => {
                // .set or .update?
                // !! is boolean casting
                xrSupported.set(!!available)
            });

            app.xr.on("start", () => {
                console.log("Immersive AR session has started");
                xrActive.set(true)
                this.app.scene.layers.getLayerById(LAYERID_SKYBOX).enabled = false;
            });

            app.xr.on("end", () => {
                console.log("Immersive AR session has ended");
                xrActive.set(false)
                this.setSkyboxMip(1);
                const modelScale = this.shotHandler?.loader?.modelScale;
                const xrScale = this.shotHandler?.loader?.modelXRScale;
                const gltfRoot = this.shotHandler?.loader?.gltfRoot;

                if (modelScale != null && xrScale != null && gltfRoot != null) {
                    // Coming out of xr, scale factor is modelScale/xrScale
                    const scale = new Vec3(modelScale / xrScale, modelScale / xrScale, modelScale / xrScale);

                    gltfRoot.setLocalScale(scale);
                }
                const envSphere = this.shotHandler?.loader?.envSphere;
                if (envSphere) {
                    envSphere.enabled = true;
                }
            });
        }

        // monkeypatch the mouse and touch input devices to ignore touch events
        // when they don't originate from the canvas.
        const origMouseHandler = app.mouse._moveHandler;
        app.mouse.detach();
        app.mouse._moveHandler = (event: MouseEvent) => {
            if (event.target === canvas) {
                origMouseHandler(event);
            }
        };
        app.mouse.attach(canvas);

        const origTouchHandler = app.touch._moveHandler;
        app.touch.detach();
        app.touch._moveHandler = (event: MouseEvent) => {
            if (event.target === canvas) {
                origTouchHandler(event);
            }
        };
        app.touch.attach(canvas);

        const multisampleSupportedTest = app.graphicsDevice.maxSamples > 1;
        console.info("Multisample supported: ", multisampleSupportedTest)
        this.multisampleSupported = multisampleSupportedTest;
        this.multisample = multisampleSupportedTest && this.multisample;

        // Disable I-bar cursor on click+drag
        canvas.onselectstart = function () { return false; };

        // Disable long-touch select on iOS devices
        canvas.style['-webkit-user-select'] = 'none';


        // Set the canvas to fill the window and automatically change resolution to be the same as the canvas size
        const canvasSize = this.getCanvasSize();
        app.setCanvasFillMode(FILLMODE_NONE, canvasSize.width, canvasSize.height);
        app.setCanvasResolution(RESOLUTION_AUTO);
        window.addEventListener("resize", () => {
            this.resizeCanvas();
            // after resize, there is sometimes a bug
            // where canvas will get stuck and not fill 
            // the full width. Workaround:

            //if (this.resizeTimer != null) {
            //    window.clearTimeout(this.resizeTimer);
            //    this.resizeTimer = null;
            //    console.info("Clearing Timer")
            //}
            //else {
            if (!this.resizeTimer) {

                this.resizeTimer = setTimeout(() => {
                    this.resizeCanvas();
                    console.info("Delayed resize");
                    this.resizeTimer = null;
                }, 200);
            }
            //}


        });

        window.addEventListener("orientationchange", () => {
            this.resizeCanvas();
            console.info("Orientation Change")
        });

        // Depth layer is where the framebuffer is copied to a texture to be used in the following layers.
        // Move the depth layer to take place after World and Skydome layers, to capture both of them.
        const depthLayer = app.scene.layers.getLayerById(LAYERID_DEPTH);
        app.scene.layers.remove(depthLayer);
        app.scene.layers.insertOpaque(depthLayer, 2);

        // create the orbit camera
        const camera = new Entity("Camera");
        camera.addComponent("camera", {
            fov: 75,
            frustumCulling: true,
            clearColor: new Color(0, 0, 0, 0)
        });
        camera.camera.requestSceneColorMap(true);

        this.orbitCamera = new OrbitCamera(camera, 0.25);
        this.orbitCameraInputMouse = new OrbitCameraInputMouse(this.app, this.orbitCamera);
        this.orbitCameraInputTouch = new OrbitCameraInputTouch(this.app, this.orbitCamera);
        //this.orbitCamera.cameraNode.addComponent('script');

        this.orbitCamera.focalPoint.snapto(new Vec3(ftConfig.initFocalPoint.x, ftConfig.initFocalPoint.y, ftConfig.initFocalPoint.z));

        app.root.addChild(camera);



        // create the light
        /* TODO remove?
        const light = new Entity();
        light.addComponent("light", {
            type: "directional",
            color: new Color(1, 1, 1),
            castShadows: true,
            intensity: 1,
            shadowBias: 0.2,
            shadowDistance: 5,
            normalOffsetBias: 0.05,
            shadowResolution: 2048
        });
        light.setLocalEulerAngles(45, 30, 0);
        app.root.addChild(light);
        */

        // disable autorender
        app.autoRender = false;
        this.prevCameraMat = new Mat4();
        app.on('update', this.update, this);
        // app.on('prerender', this.onPrerender, this);
        app.on('postrender', this.onPostrender, this);
        app.on('frameend', this.onFrameend, this);
        app.once('start', () => appStarted.set(true));
        // create the scene and debug root nodes
        const sceneRoot = new Entity("sceneRoot", app);
        app.root.addChild(sceneRoot);

        // store app things
        this.camera = camera;
        this.cameraFocusBBox = null;
        this.cameraPosition = null;
        // this.light = light;
        this.sceneRoot = sceneRoot;
        this.entities = [];
        this.entityAssets = [];
        this.assets = [];
        this.meshInstances = [];
        this.animTracks = [];
        this.animationMap = {};
        this.firstFrame = false;
        this.skyboxLoaded = false;

        // this.animSpeed = observer.get('animation.speed');
        // this.animTransition = observer.get('animation.transition');
        // this.animLoops = observer.get('animation.loops');
        // this.showWireframe = observer.get('show.wireframe');
        // this.showBounds = observer.get('show.bounds');
        // this.showSkeleton = observer.get('show.skeleton');
        // this.showAxes = observer.get('show.axes');
        // this.normalLength = observer.get('show.normals');

        this.setTonemapping();
        // this.setBackgroundColor(observer.get('lighting.env.backgroundColor'));

        this.dirtyWireframe = false;
        this.dirtyBounds = false;
        this.dirtySkeleton = false;
        this.dirtyGrid = false;
        this.dirtyNormals = false;

        this.sceneBounds = null;

        const device = this.app.graphicsDevice as WebglGraphicsDevice;

        // multiframe
        this.multiframe = new Multiframe(device, this.camera.camera, 5);
        this.multiframe.enabled = false;

        this.resizeCanvas();

        // construct the depth reader
        this.readDepth = new ReadDepth(device);
        this.cursorWorld = new Vec3();

        // double click handler
        // Disabled, so we can not doubleclick the envSphere
        /*
        canvas.addEventListener('dblclick', (event) => {
            console.info(event)
            const camera = this.camera.camera;
            const x = event.offsetX / canvas.clientWidth;
            const y = 1.0 - event.offsetY / canvas.clientHeight;

            
            // The pc.Vec3 to raycast from (the position of the camera)
            const from = this.camera.getPosition();
            // The pc.Vec3 to raycast to (the click position projected onto the camera's far clip plane)
            const to = camera.screenToWorld(screenX, screenY, this.camera.farClip);
            // Raycast between the two points and return the closest hit result
            const result = this.app.systems.rigidbody.raycastFirst(from, to);
            // If there was a hit, store the entity
            if (result) {
                const hitEntity = result.entity;
                console.log('You selected ' + hitEntity.name);
            }
            


            // read depth
            const depth = this.readDepth.read(camera.renderTarget.depthBuffer, x, y);
            console.info(depth)
            if (depth < 1) {
                const pos = new Vec4(x, y, depth, 1.0).mulScalar(2.0).subScalar(1.0);            // clip space
                camera.projectionMatrix.clone().invert().transformVec4(pos, pos);                   // homogeneous view space
                pos.mulScalar(1.0 / pos.w);                                                         // perform perspective divide
                this.cursorWorld.set(pos.x, pos.y, pos.z);
                this.camera.getWorldTransform().transformPoint(this.cursorWorld, this.cursorWorld); // world space

                // move camera towards focal point
                console.info("move camera towards focal point from dbl click")
                this.orbitCamera.focalPoint.transitionTime = 1.25;
                this.orbitCamera.focalPoint.goto(this.cursorWorld);
            }
        });
        */

        // this.setBackgroundColor({ r: 0, g: 0, b: 0 })
        this.loadDefaultSkybox();
        if (this.app?.scene?.layers && this.app.scene.layers.getLayerById(LAYERID_SKYBOX) != null) {
            this.app.scene.layers.getLayerById(LAYERID_SKYBOX).enabled = ftConfig.showSkybox;
        }
        // Initialize shotHandler
        this.shotHandler = new ShotHandler(this.app, this.orbitCamera.cameraNode, this.dWrapper);


    }

    /**
     * Load loading.glb
     * @returns 
     */
    async finalizeInit() {
        if (!this.shotHandler) {
            console.info("No shotHandler")
            return;
        }
        // await load loading.glb
        this.shotHandler.loader.loaderAndEnv().then((res) => {
            console.info("finalize Initialisation")
            // start the application
            this.app.start();
        })

    }


    // collects all mesh instances from entity hierarchy
    /* TODO remove?
    private collectMeshInstances(entity: Entity) {
        const meshInstances: Array<MeshInstance> = [];
        if (entity) {
            if (entity.name === "EnvSphere") {
                console.info("Found envSphere")
                return meshInstances;
            }
            const components = entity.findComponents("render");
            for (let i = 0; i < components.length; i++) {
                const render = components[i] as RenderComponent;
                if (render.meshInstances) {
                    for (let m = 0; m < render.meshInstances.length; m++) {
                        const meshInstance = render.meshInstances[m];
                        meshInstances.push(meshInstance);
                    }
                }
            }
        }
        return meshInstances;
    }
    */
    /* TODO remove?
    private updateMeshInstanceList() {

        this.meshInstances = [];
        for (let e = 0; e < this.entities.length; e++) {
            const meshInstances = this.collectMeshInstances(this.entities[e]);
            this.meshInstances = this.meshInstances.concat(meshInstances);
        }
    }
    */
    // calculate the bounding box of the given mesh
    /* TODO remove
    private static calcMeshBoundingBox(meshInstances: Array<MeshInstance>) {
        const bbox = new BoundingBox();
        for (let i = 0; i < meshInstances.length; ++i) {
            if (i === 0) {
                bbox.copy(meshInstances[i].aabb);
            } else {
                bbox.add(meshInstances[i].aabb);
            }
        }
        return bbox;
    }
    */

    // calculate the bounding box of the graph-node hierarchy
    /* TODO remove?
    private static calcHierBoundingBox(rootNode: Entity) {
        const position = rootNode.getPosition();
        let min_x = position.x;
        let min_y = position.y;
        let min_z = position.z;
        let max_x = position.x;
        let max_y = position.y;
        let max_z = position.z;

        const recurse = (node: GraphNode) => {
            const p = node.getPosition();
            if (p.x < min_x) min_x = p.x; else if (p.x > max_x) max_x = p.x;
            if (p.y < min_y) min_y = p.y; else if (p.y > max_y) max_y = p.y;
            if (p.z < min_z) min_z = p.z; else if (p.z > max_z) max_z = p.z;
            for (let i = 0; i < node.children.length; ++i) {
                console.info(node.children[i].name)
                recurse(node.children[i]);
            }
        };
        recurse(rootNode);

        const result = new BoundingBox();
        result.setMinMax(new Vec3(min_x, min_y, min_z), new Vec3(max_x, max_y, max_z));
        return result;
    }
    */

    // calculate the intersection of the two bounding boxes
    /* TODO remove?
    private static calcBoundingBoxIntersection(bbox1: BoundingBox, bbox2: BoundingBox) {
        // bounds don't intersect
        if (!bbox1.intersects(bbox2)) {
            return null;
        }
        const min1 = bbox1.getMin();
        const max1 = bbox1.getMax();
        const min2 = bbox2.getMin();
        const max2 = bbox2.getMax();
        const result = new BoundingBox();
        result.setMinMax(new Vec3(Math.max(min1.x, min2.x), Math.max(min1.y, min2.y), Math.max(min1.z, min2.z)),
            new Vec3(Math.min(max1.x, max2.x), Math.min(max1.y, max2.y), Math.min(max1.z, max2.z)));
        return result;
    }

    */
    private clearSkybox() {
        this.app.scene.envAtlas = null;
        this.app.scene.setSkybox(null);
        this.renderNextFrame();
        this.skyboxLoaded = false;
    }

    // initialize the faces and prefiltered lighting data from the given
    // skybox texture, which is either a cubemap or equirect texture.
    private initSkyboxFromTextureNew(env: Texture) {
        const skybox = EnvLighting.generateSkyboxCubemap(env);
        const lighting = EnvLighting.generateLightingSource(env);
        // The second options parameter should not be necessary but the TS declarations require it for now
        const envAtlas = EnvLighting.generateAtlas(lighting, {});
        lighting.destroy();
        this.app.scene.envAtlas = envAtlas;
        this.app.scene.skybox = skybox;

        this.renderNextFrame();
    }

    // initialize the faces and prefiltered lighting data from the given
    // skybox texture, which is either a cubemap or equirect texture.
    private initSkyboxFromTexture(skybox: Texture) {
        if (EnvLighting) {
            return this.initSkyboxFromTextureNew(skybox);
        }

        const app = this.app;
        const device = app.graphicsDevice;

        const createCubemap = (size: number) => {
            return new Texture(device, {
                name: `skyboxFaces-${size}`,
                cubemap: true,
                width: size,
                height: size,
                type: TEXTURETYPE_RGBM,
                addressU: ADDRESS_CLAMP_TO_EDGE,
                addressV: ADDRESS_CLAMP_TO_EDGE,
                fixCubemapSeams: true,
                mipmaps: false
            });
        };

        const cubemaps = [];

        cubemaps.push(EnvLighting.generateSkyboxCubemap(skybox));

        const lightingSource = EnvLighting.generateLightingSource(skybox);

        // create top level
        const top = createCubemap(128);
        reprojectTexture(lightingSource, top, {
            numSamples: 1
        });
        cubemaps.push(top);

        // generate prefiltered lighting data
        const sizes = [128, 64, 32, 16, 8, 4];
        const specPower = [1, 512, 128, 32, 8, 2];
        for (let i = 1; i < sizes.length; ++i) {
            const level = createCubemap(sizes[i]);
            reprojectTexture(lightingSource, level, {
                numSamples: 1024,
                specularPower: specPower[i],
                distribution: 'ggx'
            });

            cubemaps.push(level);
        }

        lightingSource.destroy();

        // assign the textures to the scene
        app.scene.setSkybox(cubemaps);
        this.renderNextFrame();
    }

    // load the image files into the skybox. this function supports loading a single equirectangular
    // skybox image or 6 cubemap faces.
    /* TODO remove?
    private loadSkybox(files: Array<File>) {
        const app = this.app;

        if (files.length !== 6) {
            // load equirectangular skybox
            const textureAsset = new Asset('skybox_equi', 'texture', {
                url: files[0].url,
                filename: files[0].filename
            });
            textureAsset.ready(() => {
                const texture = textureAsset.resource;
                if (texture.type === TEXTURETYPE_DEFAULT && texture.format === PIXELFORMAT_RGBA8) {
                    // assume RGBA data (pngs) are RGBM
                    texture.type = TEXTURETYPE_RGBM;
                }
                this.initSkyboxFromTexture(texture);
            });
            app.assets.add(textureAsset);
            app.assets.load(textureAsset);
        } else {
            // sort files into the correct order based on filename
            const names = [
                ['posx', 'negx', 'posy', 'negy', 'posz', 'negz'],
                ['px', 'nx', 'py', 'ny', 'pz', 'nz'],
                ['right', 'left', 'up', 'down', 'front', 'back'],
                ['right', 'left', 'top', 'bottom', 'forward', 'backward'],
                ['0', '1', '2', '3', '4', '5']
            ];

            const getOrder = (filename: string) => {
                const fn = filename.toLowerCase();
                for (let i = 0; i < names.length; ++i) {
                    const nameList = names[i];
                    for (let j = 0; j < nameList.length; ++j) {
                        if (fn.indexOf(nameList[j] + '.') !== -1) {
                            return j;
                        }
                    }
                }
                return 0;
            };

            const sortPred = (first: File, second: File) => {
                const firstOrder = getOrder(first.filename);
                const secondOrder = getOrder(second.filename);
                return firstOrder < secondOrder ? -1 : (secondOrder < firstOrder ? 1 : 0);
            };

            files.sort(sortPred);

            // construct an asset for each cubemap face
            const faceAssets = files.map((file, index) => {
                const faceAsset = new Asset('skybox_face' + index, 'texture', file);
                app.assets.add(faceAsset);
                app.assets.load(faceAsset);
                return faceAsset;
            });

            // construct the cubemap asset
            const cubemapAsset = new Asset('skybox_cubemap', 'cubemap', null, {
                textures: faceAssets.map(faceAsset => faceAsset.id)
            });
            cubemapAsset.loadFaces = true;
            cubemapAsset.on('load', () => {
                this.initSkyboxFromTexture(cubemapAsset.resource);
            });
            app.assets.add(cubemapAsset);
            app.assets.load(cubemapAsset);
        }
        this.skyboxLoaded = true;
    }
    */

    // load the built in default cubemap
    private loadDefaultSkybox() {
        const app = this.app;

        const cubemap = new Asset('default', 'cubemap', {
            url: "d/cubemaps/default.dds"
        }, {
            magFilter: FILTER_LINEAR,
            minFilter: FILTER_LINEAR_MIPMAP_LINEAR,
            anisotropy: 1,
            type: TEXTURETYPE_RGBM
        });
        cubemap.on('load', () => {
            app.scene.setSkybox(cubemap.resources);
            this.renderNextFrame();
        });
        app.assets.add(cubemap);
        app.assets.load(cubemap);
        this.skyboxLoaded = true;
    }

    private getCanvasSize() {
        return {
            width: this.dWrapper.offsetWidth,//document.body.clientWidth - document.getElementById("panel-left").offsetWidth, 
            height: this.dWrapper.offsetHeight//document.body.clientHeight
        };
    }

    resizeCanvas() {

        const device = this.app.graphicsDevice as WebglGraphicsDevice;
        const canvasSize = this.getCanvasSize();

        device.maxPixelRatio = window.devicePixelRatio;
        this.app.resizeCanvas(canvasSize.width, canvasSize.height);
        this.renderNextFrame();

        const createTexture = (width: number, height: number, format: number) => {
            return new Texture(device, {
                width: width,
                height: height,
                format: format,
                mipmaps: false,
                minFilter: FILTER_NEAREST,
                magFilter: FILTER_NEAREST,
                addressU: ADDRESS_CLAMP_TO_EDGE,
                addressV: ADDRESS_CLAMP_TO_EDGE
            });
        };

        // out with the old
        const old = this.camera.camera.renderTarget;
        if (old) {
            old.colorBuffer.destroy();
            old.depthBuffer.destroy();
            old.destroy();
        }

        // in with the new
        const pixelScale = get(pixelScaleMult)
        const w = Math.floor(canvasSize.width * window.devicePixelRatio / pixelScale);
        const h = Math.floor(canvasSize.height * window.devicePixelRatio / pixelScale);
        const colorBuffer = createTexture(w, h, PIXELFORMAT_RGBA8);
        const depthBuffer = createTexture(w, h, PIXELFORMAT_DEPTH);
        console.info("Resizin with multisample ", this.multisample," and device maxSamples ", device.maxSamples)
        const renderTarget = new RenderTarget({
            colorBuffer: colorBuffer,
            depthBuffer: depthBuffer,
            flipY: false,
            samples: this.multisample ? device.maxSamples : 1,
            autoResolve: false
        });
        this.camera.camera.renderTarget = renderTarget;
    }

    // reset the viewer, unloading resources
    resetScene() {
        const app = this.app;

        this.entities.forEach((entity) => {
            this.sceneRoot.removeChild(entity);
            entity.destroy();
        });
        this.entities = [];
        this.entityAssets = [];

        this.assets.forEach((asset) => {
            app.assets.remove(asset);
            asset.unload();
        });
        this.assets = [];

        this.meshInstances = [];

        // reset animation state
        this.animTracks = [];
        this.animationMap = {};

        this.updateSceneInfo();

        this.dirtyWireframe = this.dirtyBounds = this.dirtySkeleton = this.dirtyGrid = this.dirtyNormals = true;
        this.renderNextFrame();
    }

    updateSceneInfo() {
        let meshCount = 0;
        let vertexCount = 0;
        let primitiveCount = 0;
        let variants: string[] = [];

        // update mesh stats
        this.assets.forEach((asset) => {
            variants = variants.concat(asset.resource.getMaterialVariants());
            asset.resource.renders.forEach((renderAsset: Asset) => {
                renderAsset.resource.meshes.forEach((mesh: Mesh) => {
                    meshCount++;
                    vertexCount += mesh.vertexBuffer.getNumVertices();
                    primitiveCount += mesh.primitive[0].count;
                });
            });
        });

        const mapChildren = function (node: GraphNode): Array<HierarchyNode> {
            return node.children.map((child: GraphNode) => ({
                name: child.name,
                path: child.path,
                children: mapChildren(child)
            }));
        };

        const graph: Array<HierarchyNode> = this.entities.map((entity) => {
            return {
                name: entity.name,
                path: entity.path,
                children: mapChildren(entity)
            };
        });

    }



    startXr() {
        if (this.app.xr.isAvailable(XRTYPE_AR)) {
            const envSphere = this.shotHandler?.loader?.envSphere;
            const modelScale = this.shotHandler?.loader?.modelScale;
            const xrScale = this.shotHandler?.loader?.modelXRScale;
            const gltfRoot = this.shotHandler?.loader?.gltfRoot;
            const camera = this.orbitCamera;

            this.camera.camera.startXr(XRTYPE_AR, XRSPACE_LOCALFLOOR, {
                callback: function (err) {
                    if (err) {
                        // failed to start XR session
                        console.log(err)
                    } else {
                        // in XR

                        // Set camera position
                        camera.focalPoint.snapto(new Vec3(0, 0, 0));
                        camera.azimElevDistance.goto(new Vec3(35, -35, 3));

                        // Set Scale
                        if (xrScale != null && modelScale != null && gltfRoot != null) {
                            // Entering xr, scale factor is xrScale/modelScale
                            const scale = new Vec3(xrScale / modelScale, xrScale / modelScale, xrScale / modelScale);
                            gltfRoot.setLocalScale(scale);
                        }

                        if (envSphere) {
                            envSphere.enabled = false;
                        }
                    }
                }
            });
        }
    }

    // TODO remove?
    // move the camera to view the loaded object
    /*
    focusCamera() { console.info("Focus Camera")
        const camera = this.camera.camera;

        const bbox = this.calcSceneBounds();

        if (this.cameraFocusBBox) {
            const intersection = Viewer.calcBoundingBoxIntersection(this.cameraFocusBBox, bbox);
            if (intersection) {
                const len1 = bbox.halfExtents.length();
                const len2 = this.cameraFocusBBox.halfExtents.length();
                const len3 = intersection.halfExtents.length();
                if ((Math.abs(len3 - len1) / len1 < 0.1) &&
                    (Math.abs(len3 - len2) / len2 < 0.1)) {
                    return;
                }
            }
        }

        // calculate scene bounding box
        const radius = bbox.halfExtents.length();
        const distance = (radius * 1.4) / Math.sin(0.5 * camera.fov * camera.aspectRatio * math.DEG_TO_RAD);

        if (this.cameraPosition) {
            const vec = bbox.center.clone().sub(this.cameraPosition);
            this.orbitCamera.vecToAzimElevDistance(vec, vec);
            this.orbitCamera.azimElevDistance.snapto(vec);
            this.cameraPosition = null;
        } else {
            const aed = this.orbitCamera.azimElevDistance.target.clone();
            aed.z = distance;
            this.orbitCamera.azimElevDistance.snapto(aed);
        }
        this.orbitCamera.focalPoint.snapto(bbox.center);
        camera.nearClip = distance / 100;
        camera.farClip = distance * 10;

        const light = this.light;
        light.light.shadowDistance = distance * 2;

        this.cameraFocusBBox = bbox;
    }
    */

    setTonemapping() {
        this.app.scene.toneMapping = TONEMAP_ACES;
        this.renderNextFrame();
    }

    setBackgroundColor(color: { r: number, g: number, b: number }) {
        const cnv = (value: number) => Math.max(0, Math.min(255, Math.floor(value * 255)));
        // document.getElementById('canvas-wrapper').style.backgroundColor = `rgb(${cnv(color.r)}, ${cnv(color.g)}, ${cnv(color.b)})`;
        this.dWrapper.style.backgroundColor = `rgb(${cnv(color.r)}, ${cnv(color.g)}, ${cnv(color.b)})`;
    }

    setSkyboxMip(mip: number) {
        this.app.scene.layers.getLayerById(LAYERID_SKYBOX).enabled = ftConfig.showSkybox ? (mip !== 0) : false;
        this.app.scene.skyboxMip = mip - 1;
        this.renderNextFrame();
    }

    update(deltaTime: number) {

        this.updateTweenStore();
        this.updateHotspots();

        // update the orbit camera
        this.orbitCamera.update(deltaTime);

        const maxdiff = (a: Mat4, b: Mat4) => {
            let result = 0;
            for (let i = 0; i < 16; ++i) {
                result = Math.max(result, Math.abs(a.data[i] - b.data[i]));
            }
            return result;
        };

        // if the camera has moved since the last render
        const cameraWorldTransform = this.camera.getWorldTransform();
        if (maxdiff(cameraWorldTransform, this.prevCameraMat) > 1e-04) {
            this.prevCameraMat.copy(cameraWorldTransform);
            this.renderNextFrame();
        }

        // always render during xr sessions
        // if no xr, render if anim is playing
        if (get(xrActive)) {
            this.renderNextFrame();
        }
        else if (this.shotHandler?.gltfRoot?.anim?.baseLayer?.playing || this.shotHandler?.loader?.loading_rig?.anim?.baseLayer?.playing) {
            this.renderNextFrame();
        }
    }

    updateHotspots() {
        if (!this.shotHandler?.hotspotStore) {
            return
        }
        if (Object.keys(this.shotHandler.hotspotStore).length) {

            Object.keys(this.shotHandler.hotspotStore).forEach(objectName => {

                const hotspotData = this.shotHandler.hotspotStore[objectName];
                const worldPos = hotspotData.target.getPosition();
                this.camera.camera.worldToScreen(worldPos, hotspotData.screenPos);

                // check if the entity is in front of the camera
                if (hotspotData.screenPos.z > 0) {
                    // Figure out depth
                    if (hotspotData.div.matches(':hover')) {
                        hotspotData.div.style.zIndex = 1000
                    } else {
                        // To figure out depth, need to scale the distance first (depending on scene bounds?).
                        // This ensures, there is enough space between the numbers
                        hotspotData.div.style.zIndex = Math.round(this.fitNumberInRange(0, 1000, 100, 10, hotspotData.screenPos.z * 100));
                    }
                    // hotspotData.div.style.opacity = this.fitNumberInRange(0, 1000, 1, 0, hotspotData.screenPos.z * 100);
                    hotspotData.div.style.left = hotspotData.screenPos.x + 'px';
                    hotspotData.div.style.top = hotspotData.screenPos.y + 'px';
                }
            })
        }
    }

    /**
     * Handles tween animations
     */
    updateTweenStore() {
        Object.keys(this.shotHandler?.tweenStore).forEach(tweenType => {
            const tween = this.shotHandler?.tweenStore[tweenType]
            if (tween.progress >= 1) {
                // Remove from tweenStore
                delete this.shotHandler?.tweenStore[tweenType];
                console.info("Deleted tween ", tweenType);
            }
            tween.progress = tween.progress + (1 / tween.duration);


            switch (tweenType) {
                case 'camera':
                    // vec is azim(yaw), elev(pitch), distance
                    //this.camTarget = this.orbitCamera.azimElevDistance.value.copy;
                    this.camTarget.copy(this.orbitCamera.azimElevDistance.value);
                    console.info("Camera azimElevDist:");
                    console.info(this.orbitCamera.azimElevDistance);

                    if (tween.yaw) {
                        this.camTarget.x = tween.yaw;
                    }
                    if (tween.pitch) {
                        this.camTarget.y = tween.pitch;
                    }
                    if (tween.distance) {
                        this.camTarget.z = tween.distance;
                    }
                    this.orbitCamera.azimElevDistance.transitionTime = 2;
                    this.orbitCamera.azimElevDistance.goto(this.camTarget);


                    if (tween.pivotPoint) {
                        // PivotPoint can also be string. In this case, we want
                        // to look for an object with string as name, and use
                        // its localPosition as pivotPoint
                        if (typeof tween.pivotPoint === "string") {
                            const found = this.app.root.findByName(tween.pivotPoint);
                            if (found) {
                                const pos = found.getPosition();
                                if (pos) {
                                    // Replace string pivotPoint in shotdata
                                    tween.pivotPoint = pos;
                                    this.orbitCamera.focalPoint.transitionTime = 1.25;
                                    this.orbitCamera.focalPoint.goto(tween.pivotPoint);
                                }
                            }
                        } else {
                            this.orbitCamera.focalPoint.transitionTime = 1.25;
                            this.orbitCamera.focalPoint.goto(tween.pivotPoint);
                        }
                    }

                    // Delete the camera right away, orbitCamera does
                    // the rest.
                    delete this.shotHandler?.tweenStore[tweenType];
                    break;

                case 'fov':
                    tween.currentValue = ((tween.valueDelta * this.easing(tween.ease, tween.progress)) + tween.valueStart);
                    // Update
                    if (this.camera?.camera) {
                        this.camera.camera.fov = tween.currentValue;
                    }
                    break;

                case 'skyboxIntensity':
                    tween.currentValue = ((tween.valueDelta * this.easing(tween.ease, tween.progress)) + tween.valueStart);
                    // Update
                    this.app.scene.skyboxIntensity = tween.currentValue;
                    break
                case 'skyboxRotation':
                    tween.currentValue = (tween.valueDelta * this.easing(tween.ease, tween.progress))
                    this.app.scene.skyboxRotation = this.skyboxRotation.slerp(tween.valueStart, tween.valueEnd, tween.currentValue);
                    break
                default:
                    null;
            }
        })
    }


    easing(type = "easeInOutSine", x: number): number {
        let c4;
        switch (type) {
            case ("easeOutCirc"):
                return Math.sqrt(1 - Math.pow(x - 1, 2));
                break;
            case ("easeOutElastic"):
                c4 = (2 * Math.PI) / 3;
                return x === 0
                    ? 0
                    : x === 1
                        ? 1
                        : Math.pow(2, -10 * x) * Math.sin((x * 10 - 0.75) * c4) + 1;
                break;
            default:
                //easeInOutSine
                return -(Math.cos(Math.PI * x) - 1) / 2;
        }
    }

    renderNextFrame() {
        this.app.renderNextFrame = true;
        //if (this.multiframe) {
        //    this.multiframe.moved();
        //}
    }

    fitNumberInRange(sourceMin: number, sourceMax: number, destMin: number, destMax: number, inputNumber: number): number {
        // (5 - 0) / (1000-0)  *  (100-10)+10
        const percent = (inputNumber - sourceMin) / (sourceMax - sourceMin);
        const result = percent * (destMax - destMin) + destMin;
        return result < 0 ? 0 : result;
    }


    // TODO remove?
    // add a loaded asset to the scene
    // asset is a container asset with renders and/or animations
    /*
    private addToScene(err: string, asset: Asset) {
        loadingSpinner.set(false);
        if (err) {
            loadingError.set(true)
            return;
        }

        const resource = asset.resource;
        const meshesLoaded = resource.renders && resource.renders.length > 0;
        const animsLoaded = resource.animations && resource.animations.length > 0;
        const prevEntity: Entity = this.entities.length === 0 ? null : this.entities[this.entities.length - 1];

        let entity: Entity;

        // create entity
        if (!meshesLoaded && prevEntity && prevEntity.findComponent("render")) {
            entity = prevEntity;
        } else {
            entity = asset.resource.instantiateRenderEntity();
            this.entities.push(entity);
            this.entityAssets.push({ entity: entity, asset: asset });
            this.sceneRoot.addChild(entity);
        }

        // create animation component
        if (animsLoaded) {
            // append anim tracks to global list
            resource.animations.forEach((a: any) => {
                this.animTracks.push(a.resource);
            });
        }

        // rebuild the anim state graph
        if (this.animTracks.length > 0) {
            this.rebuildAnimTracks();
        }

        // make a list of all the morph instance target names
        const morphs: Record<string, { name: string, targets: Record<string, MorphTargetData> }> = {};

        const morphInstances: Record<string, MorphInstance> = {};
        // get all morph targets
        const meshInstances = this.collectMeshInstances(entity);
        meshInstances.forEach((meshInstance, i) => {
            if (meshInstance.morphInstance) {
                const morphInstance = meshInstance.morphInstance;
                morphInstances[i] = morphInstance;

                // mesh name line
                const meshName = (meshInstance && meshInstance.node && meshInstance.node.name) || "Mesh " + i;
                morphs[i] = {
                    name: meshName,
                    targets: {}
                };

                // morph targets
                morphInstance.morph.targets.forEach((target: MorphTarget, targetIndex: number) => {
                    morphs[i].targets[targetIndex] = {
                        name: target.name,
                        targetIndex: targetIndex
                    };
                });
            }
        });



        // store the loaded asset
        this.assets.push(asset);

        // update
        this.updateSceneInfo();

        // construct a list of meshInstances so we can quickly access them when configuring wireframe rendering etc.
        this.updateMeshInstanceList();


        // dirty everything
        this.dirtyWireframe = this.dirtyBounds = this.dirtySkeleton = this.dirtyGrid = this.dirtyNormals = true;

        // we can't refocus the camera here because the scene hierarchy only gets updated
        // during render. we must instead set a flag, wait for a render to take place and
        // then focus the camera.
        this.firstFrame = true;
        this.renderNextFrame();
    }
    */

    // TODO remove?
    // rebuild the animation state graph
    /*
    private rebuildAnimTracks() {
        this.entities.forEach((entity) => {
            // create the anim component if there isn't one already
            if (!entity.anim) {
                entity.addComponent('anim', {
                    activate: true,
                    speed: this.animSpeed
                });
                entity.anim.rootBone = entity;
            } else {
                // clean up any previous animations
                entity.anim.removeStateGraph();
            }

            this.animTracks.forEach((t: any, i: number) => {
                // add an event to each track which transitions to the next track when it ends
                t.events = new AnimEvents([
                    {
                        name: "transition",
                        time: t.duration,
                        nextTrack: "track_" + (i === this.animTracks.length - 1 ? 0 : i + 1)
                    }
                ]);
                entity.anim.assignAnimation('track_' + i, t);
                this.animationMap[t.name] = 'track_' + i;
            });
            // if the user has selected to play all tracks in succession, then transition to the next track after a set amount of loops
            entity.anim.on('transition', (e) => {
                const animationName: string = this.observer.get('animation.selectedTrack');
                if (animationName === 'ALL_TRACKS' && entity.anim.baseLayer.activeStateProgress >= this.animLoops) {
                    entity.anim.baseLayer.transition(e.nextTrack, this.animTransition);
                }
            });
        });

        // let the controls know about the new animations, set the selected track and immediately start playing the animation
        // const animationKeys = Object.keys(this.animationMap);
        // const animationState = this.observer.get('animation');
        // animationState.list = JSON.stringify(animationKeys);
        // animationState.selectedTrack = animationKeys[0];
        // animationState.playing = true;
    }
    */

    /**
     * TODO Remove?
     * @returns 
     */
    /*
    private calcSceneBounds() {
        return this.meshInstances.length ?
            Viewer.calcMeshBoundingBox(this.meshInstances) :
            (this.sceneRoot.children.length ?
                Viewer.calcHierBoundingBox(this.sceneRoot) : defaultSceneBounds);
    }
    */


    private onPostrender() {
        // resolve the (possibly multisampled) render target
        if (this.camera.camera.renderTarget._samples > 1) {
            this.camera.camera.renderTarget.resolve();
        }

        // perform mulitiframe update. returned flag indicates whether more frames
        // are needed.
        this.multiframeBusy = this.multiframe.update();
    }

    private onFrameend() {
        if (this.firstFrame) {
            this.firstFrame = false;

            // focus camera after first frame otherwise skinned model bounding
            // boxes are incorrect
            this.focusCamera();
            this.renderNextFrame();
        } else if (this.loadTimestamp !== null) {
            this.loadTimestamp = null;
        }

        if (this.multiframeBusy) {
            this.app.renderNextFrame = true;
        }
    }

    // to change samples at runtime execute in the debugger 'viewer.setSamples(5, false, 2, 0)'
    // setSamples(numSamples: number, jitter = false, size = 1, sigma = 0) {
    //     this.multiframe.setSamples(numSamples, jitter, size, sigma);
    //     this.renderNextFrame();
    // }

    // handleMultiframe(mutliframe: boolean) {
    //     if (this.multiframe === null) {
    //         return;
    //     }
    //     console.info("Handle Multiframe ", mutliframe)
    //     this.multiframe.enabled = mutliframe;
    //     this.multiframe.update();
    // }

    handleMultisample(multisample: boolean) {
        if (this.multisample === null) {
            return;
        }
        console.info("Handle Multisample ", multisample)
        this.multisample = multisample;
        this.resizeCanvas();
    }

}

export default Viewer;
