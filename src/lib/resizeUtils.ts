/**
 * https://css-tricks.com/the-trick-to-viewport-units-on-mobile/
 */
export class resizeUtils {


    recalcVH(): void {
        document.documentElement.style.setProperty('--vh', `${window.innerHeight * 0.01}px`);
    }


}