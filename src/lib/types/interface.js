var EVisType;
(function (EVisType) {
    EVisType["show"] = "show";
    EVisType["hide"] = "hide";
})(EVisType || (EVisType = {}));
var EEasing;
(function (EEasing) {
    EEasing["Linear"] = "Linear";
    EEasing["QuadraticIn"] = "QuadraticIn";
    EEasing["QuadraticOut"] = "QuadraticOut";
    EEasing["QuadraticInOut"] = "QuadraticInOut";
    EEasing["CubicIn"] = "CubicIn";
    EEasing["CubicOut"] = "CubicOut";
    EEasing["CubicInOut"] = "CubicInOut";
    EEasing["QuarticIn"] = "QuarticIn";
    EEasing["QuarticOut"] = "QuarticOut";
    EEasing["QuarticInOut"] = "QuarticInOut";
    EEasing["QuinticIn"] = "QuinticIn";
    EEasing["QuinticOut"] = "QuinticOut";
    EEasing["QuinticInOut"] = "QuinticInOut";
    EEasing["SineIn"] = "SineIn";
    EEasing["SineOut"] = "SineOut";
    EEasing["SineInOut"] = "SineInOut";
    EEasing["ExponentialIn"] = "ExponentialIn";
    EEasing["ExponentialOut"] = "ExponentialOut";
    EEasing["ExponentialInOut"] = "ExponentialInOut";
    EEasing["CircularIn"] = "CircularIn";
    EEasing["CircularOut"] = "CircularOut";
    EEasing["CircularInOut"] = "CircularInOut";
    EEasing["BackIn"] = "BackIn";
    EEasing["BackOut"] = "BackOut";
    EEasing["BackInOut"] = "BackInOut";
    EEasing["BounceIn"] = "BounceIn";
    EEasing["BounceOut"] = "BounceOut";
    EEasing["BounceInOut"] = "BounceInOut";
    EEasing["ElasticIn"] = "ElasticIn";
    EEasing["ElasticOut"] = "ElasticOut";
    EEasing["ElasticInOut"] = "ElasticInOut";
})(EEasing || (EEasing = {}));
var EEntityTypes;
(function (EEntityTypes) {
    EEntityTypes["geo"] = "geo";
    EEntityTypes["light"] = "light";
    EEntityTypes["camera"] = "camera";
    EEntityTypes["env"] = "env";
})(EEntityTypes || (EEntityTypes = {}));
var EAxis;
(function (EAxis) {
    EAxis["x"] = "x";
    EAxis["y"] = "y";
    EAxis["z"] = "z";
})(EAxis || (EAxis = {}));
var EMaterialSides;
(function (EMaterialSides) {
    EMaterialSides["front"] = "front";
    EMaterialSides["back"] = "back";
    EMaterialSides["double"] = "double";
})(EMaterialSides || (EMaterialSides = {}));
var EMaterialBlendingMode;
(function (EMaterialBlendingMode) {
    EMaterialBlendingMode["noBlending"] = "noBlending";
    EMaterialBlendingMode["normalBlending"] = "normalBlending";
    EMaterialBlendingMode["additiveBlending"] = "additiveBlending";
    EMaterialBlendingMode["subtractiveBlending"] = "subtractiveBlending";
    EMaterialBlendingMode["multiplyBlending"] = "multiplyBlending";
})(EMaterialBlendingMode || (EMaterialBlendingMode = {}));
export {};
