

export interface IData {
    pageID: string,
    shotID: string,
    pageContent: object,
    pageLayout: string,
    shotContent: IShot,
    pageGenericPath?: string,
    shotGenericPath?: string
}

interface IRGB {
    r: number | string,
    g: number | string,
    b: number | string
}

interface IEulerTransform {
    x: number | string,
    y: number | string,
    z: number | string
}



enum EVisType {
    show = "show",
    hide = "hide"
}
enum EEasing {
    Linear = "Linear",
    QuadraticIn = "QuadraticIn",
    QuadraticOut = "QuadraticOut",
    QuadraticInOut = "QuadraticInOut",
    CubicIn = "CubicIn",
    CubicOut = "CubicOut",
    CubicInOut = "CubicInOut",
    QuarticIn = "QuarticIn",
    QuarticOut = "QuarticOut",
    QuarticInOut = "QuarticInOut",
    QuinticIn = "QuinticIn",
    QuinticOut = "QuinticOut",
    QuinticInOut = "QuinticInOut",
    SineIn = "SineIn",
    SineOut = "SineOut",
    SineInOut = "SineInOut",
    ExponentialIn = "ExponentialIn",
    ExponentialOut = "ExponentialOut",
    ExponentialInOut = "ExponentialInOut",
    CircularIn = "CircularIn",
    CircularOut = "CircularOut",
    CircularInOut = "CircularInOut",
    BackIn = "BackIn",
    BackOut = "BackOut",
    BackInOut = "BackInOut",
    BounceIn = "BounceIn",
    BounceOut = "BounceOut",
    BounceInOut = "BounceInOut",
    ElasticIn = "ElasticIn",
    ElasticOut = "ElasticOut",
    ElasticInOut = "ElasticInOut",
}

export interface IAnimGraphTriggerData {
    name: string,
    speed?: number,
    value?: number, // playcanvas
    loop?: boolean, // remove
    crossfade?: number,
    repetition?: number // remove
}

export interface IAnimGraphTrigger {
    [key: string]: Array<IAnimGraphTriggerData>
}






interface IDriver {
    target: string,
    targerChannel: string,
    source: string,
    sourceChannel: string
}
interface IMaterialAnimation {
    diffuse?: IRGB,
    glossiness?: number,
    emissive?: IRGB
}

interface IAnimationKey {
    duration?: number,
    ease?: EEasing, loop?: boolean, yoyo?: boolean,

    geo?: {
        position?: IEulerTransform,
        rotation?: IEulerTransform,
        enabled?: boolean,
        material?: IMaterialAnimation
    },

    light?: {
        position?: IEulerTransform,
        rotation?: IEulerTransform,
        color?: IRGB,
        float?: {
            intensity?: number,
            range?: number,
            innerConeAngle?: number,
            outerConeAngle?: number,
        }
    }

    camera?: {
        pivotPoint?: IEulerTransform | string, // can be name of object
        fov?: number,
        // minFov?: number,
        // maxFov?: number,
        float?: {
            distance?: number,
            minDistance?:number,
            maxDistance?:number,
            pitch?: number,
            minPitch?:number,
            maxPitch?:number,
            yaw?: number,
            minYaw?:number,
            maxYaw?:number,
        }
    },

    env?: {
        skyboxIntensity?: number,
        skyboxRotation?: IEulerTransform
    }
}


interface IEntityAnimationData {
    //entity: string,
    disinherit?: boolean, // Exclude from inheritance
    type: EEntityTypes,
    keys: Array<IAnimationKey>
}

interface IEntityAnimation {
    [key: string]: IEntityAnimationData
}

enum EEntityTypes {
    geo = "geo",
    light = "light",
    camera = "camera",
    env = "env"

}

enum EAxis {
    x = "x",
    y = "y",
    z = "z"
}
interface IuvOffset {
    materialName: string,
    sourceNode: string,
    animateOpacity?:boolean
    // speed: number,
    // sourceAxisOffset: EAxis,
    // sourceAxisAlpha:EAxis,
    // targetOffsetAxis: EAxis
}

interface IAnimation {
    path: string,
    name: string,
    loop: boolean,
    transitionDuration?: number
}


enum EMaterialSides{
    front="front",
    back="back",
    double="double"
}

enum EMaterialBlendingMode{
    noBlending="noBlending", 
    normalBlending="normalBlending", 
    additiveBlending="additiveBlending",
    subtractiveBlending="subtractiveBlending", 
    multiplyBlending="multiplyBlending"
}
interface IMaterialParams {
    materialName:string,
    toneMapped?:boolean,
    flatShading?:boolean,
    emissiveIntensity?:number,
    side?:EMaterialSides,
    blending?:EMaterialBlendingMode,
    depthTest?:boolean,
    depthWrite?:boolean,
    transparent?:boolean,
    opacity?:number

}
export interface IModel {
    model: string,
    scale?: number,
    xrScale?: number,
    animation?: Array<IAnimation>,
    lookAt?: Array<string>,
    material?: Array<IMaterialParams>
}
export interface IHotspotData {
    page?: string,
    shot?: string,
    content?: string
    size?: string,
    expanded?: boolean,
    classes?: Array<string>,
}

export interface ITimelineItem {
    shot?: string,
    page?: string,
    title: {
        [key: string]: string
    } | string
}

export interface IHotspotMeta {
    disinherit?: boolean, // Exclude from inheritance
    modifier?: IHotspotModifier // modifier is used to apply changes to all hotspots in ancestor data
}

export interface IHotspotModifier {
    size?: string,
    expanded?: boolean,
    classes?: Array<string>
}
export interface IHotspot {
    [key: string]: IHotspotData
}


export interface IStatus {
    cameraPivotPoint?: boolean
    cameraFov?: boolean
    cameraDistance?: boolean
    cameraYaw?: boolean
    cameraPitch?: boolean
    setEnabled?: boolean
    animGraph?: boolean
    hotspots?: boolean
    timeline?: boolean
    load?: boolean
    isMaster?: boolean
    env?: boolean
    EnvSphere?: boolean
    background?: boolean
}

interface IBackground {
    X: number,
    Y: number,
    colors: Array<string>
}
export interface IShot {
    animation?: IEntityAnimation,
    fadeDuration?:number,
    fadeCollections?: Array<string>,
    fadeAmount?: number,
    setEnabled?: Array<string> | "disinherit", // List of objects to get a enabled state change
    enabledState?: boolean | "disinherit",
    animGraph?: IAnimGraphTrigger | "disinherit",
    inlineOverride?: string,
    hotspotsMeta?: IHotspotMeta,
    hotspots?: IHotspot,
    load?: Array<string>, // gltf paths which are keys in gltf_load.json
    timeline?: Array<ITimelineItem> | "disinherit",
    shotGenericPath?: string,
    background?: IBackground | "disinherit",
    materialTweaks?:Array<IMaterialParams>, // only applied right after gltf load
    uvOffset?: Array<IuvOffset>, // only applied right after gltf load
    rawData?: IShot,
}