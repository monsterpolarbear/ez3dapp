import { readable, writable } from 'svelte/store';
// import ftConfig from '../../ft-pc-config';
export const currentModel = writable("");
export const shotDataEditable = writable({});
export const language = writable("en-US");
export const highlightClass = writable(null);
export const shotID = writable(null);
export const xrSupported = writable(false);
export const xrActive = writable(false);
// export const multisampleSupported = writable(false);
// export const multisample = writable(false);
export const pixelScaleMult = writable(1);
export const loadingSpinner = writable(false); // playcanvas only
export const loadingError = writable(false)
export const appStarted = writable(false);
export const devMessage = writable("dev messages");
export const userInPage = writable(false);
export const isLoading = writable(true); // modelviewer loading

export const cameraValues = writable({ yaw: 0, pitch: 0, distance: 0, fov: 0, x: 0, y: 0, z: 0 });


/** Check if the browser support srcset attribute, so we can use webp.
 * This is done by creating a temporary img tag and test srcset attribute is 
 * available in the img tag
 */
export const supportsSrcSet = readable(false, function start(set) {
    // This is run when the first component subscribes
    const tempImg = document.createElement('img');
    set('srcset' in tempImg);
    console.log("INFO: Supports srcset: ", ('srcset' in tempImg));
});

export const supportsWebp = readable(false, function start(set) {
    async function supportsWebp() {
        if (!self.createImageBitmap) return false;

        const webpData = 'data:image/webp;base64,UklGRioAAABXRUJQVlA4IB4AAABwAQCdASoBAAEAAoBCJZQCdAGIQAD+6tRWkQ6VBAA=';
        const blob = await fetch(webpData).then(r => r.blob());
        return createImageBitmap(blob).then(() => true, () => false);
    }

    (async () => {
        if (await supportsWebp()) {
            console.log('WebP supported');
            set(true);
        } else {
            console.log('WebP not supported');
        }
    })();

});