//import { pageUtils } from './pageUtils';
import cloneDeep from 'lodash.clonedeep';
// import mergeWith from 'lodash.mergewith';
// import merge from 'lodash.merge';
import { language, highlightClass } from './store';
import { get } from 'svelte/store';
import type { IShot, IData } from './types/interface';
// import ftConfig from '../ft-pc-config';
import utilsConfig from '../../utils/utils-config';
const dev = import.meta.env.DEV;
//let pageHandler = new pageUtils();

interface IHeap {
    pages: Array<object>,
    shots: Array<object>,
    shotMaster: Array<object>
}


export class navigator {
    ftConfig;
    data: IData = { pageID: null, shotID: null, pageContent: null, shotContent: null, pageLayout: "", pageGenericPath: null, shotGenericPath: null };
    target: HTMLElement;
    history: Array<object> = [];
    historyPointer: number = 0;
    dataHeap: IHeap = { pages: [], shots: [], shotMaster: [] };


    constructor(config) {
        this.ftConfig = config;
        this.history.length = 0;
        this.historyPointer = 0;
    }

    /**
     * A customizer for lodash mergeWith.
     * See lodash docs
     * @param objValue 
     * @param srcValue 
     * @returns 
     */
    customizer(objValue, srcValue) {
        if (Array.isArray(objValue)) {
            return objValue.concat(srcValue);
        }
    }

    /**
     * Extract pageID and shotID from event
     * @param event 
     * @returns 
     */
    async process(event) {
        // If triggered from observer, the clicked Element is in
        // event.detail.target, because it is send through dispatch()
        this.target = event.target ? event.target : event.detail.target;

        // If parent of target is svg (if a path was clicked),
        // then make the svg the target.
        // THIS IS FOR PAGES. SVG in HOTSPOTS are dealt with differently. See View.svelte -> handleHotspotAnchor
        if (this.target.parentElement?.nodeName === "svg") {
            // Make the svg the new target
            console.info("new target because svg");
            this.target = this.target.parentElement;
        }

        // Return if target or its parent has no href (e.g. svg inside anchor)
        if (!this.target.hasAttribute("href") && !this.target.parentElement?.hasAttribute("href")) {
            return;
        }
        let c;

        // Get href from element or parent:
        if (this.target.getAttribute('href')) {
            c = this.target.getAttribute('href');
        } else if (this.target.parentElement?.getAttribute('href')) {
            c = this.target.parentElement.getAttribute('href');
        }

        this.data.pageID = c?.split(',')[0]?.length ? c?.split(',')[0] : null;
        // Shots triggered through observer start with '!'. We need to
        // remove this.
        const shotIDprep = c?.split(',')[1]?.length ? c?.split(',')[1] : null;
        if (shotIDprep != null) {
            this.data.shotID = shotIDprep.replace('!', '');

            // Shot ID starting with $ is a inline shot-override!
            // In this case, we do not fetch any data, but send the 
            // inline string to PC right away
        }
        const d = await this.getDataAsync();
        return d;
    }



    getDataAsync() {

        return new Promise((resolve, reject) => {


            // Determine what to fetch

            if (!this.data.pageID && !this.data.shotID) {
                return resolve(this.history[this.historyPointer]);
            }

            // is __back__ or __forward__ early return

            if (this.data.pageID === "__back__") {
                return resolve(this.historyBack());
            }
            if (this.data.pageID === "__forward__") {
                return resolve(this.historyForward());
            }


            // is pageID #, it is a placeholder to just add 
            // classnames. Keep current page and shot, early return
            if (this.data.pageID == '#') {
                // __TODO__ does this keep the current shot and page?
                return resolve(this.history[this.historyPointer]);
            }


            // Extract highlight class in page
            // pageID text after # is class to highlight
            if (this.data.pageID) {
                // If pageID startswith #, there is just the highlightclass
                if (this.data.pageID.startsWith("#")) {
                    highlightClass.set(this.data.pageID.replace("#", ""));
                    // There is no shot:
                    this.data.pageID = null;
                } else {

                    const hclass = this.data.pageID.split('#');
                    if (hclass.length > 1) {
                        // Clean pageID by removing highlight class:
                        highlightClass.set(hclass[1]);
                        this.data.pageID = hclass[0];
                    } else {
                        // No highlight class specified. Clear highlight class
                        highlightClass.set(null);
                    }
                }
            }


            // If shotID is inline override (starts with $), just copy the string
            // into shotContent. If there is inlineOverride key
            // the PC API will not replace the shot stored. 
            let hasInlineShotOverride: boolean = false
            if (this.data.shotID && this.data.shotID.startsWith('$')) {
                // Copy override into shotContent
                this.data.shotContent = { inlineOverride: this.data.shotID };
                hasInlineShotOverride = true;

            }

            // if !pageID and shotID and hasInlineShotOverride
            //      no fetch, set pageLayout + content null
            if (!this.data.pageID && this.data.shotID && hasInlineShotOverride) {
                this.data.pageContent = null;
                this.data.pageLayout = "";
                return resolve(this.data);
            }
            let pageURL: string;
            let shotURL: string;
            if (dev) {
                const utilsServer = `http://${utilsConfig.HOST}:${utilsConfig.PORT}/`;
                pageURL = utilsServer + 'p?name=' + this.data.pageID + '&locale=' + get(language);
                shotURL = utilsServer + 's?name=' + this.data.shotID + '&locale=' + get(language);
            } else {
                pageURL = `p/${get(language) + '/' + this.data.pageID}.json`;
                shotURL = `s/${get(language) + '/' + this.data.shotID}.json`;
            }

            // if pageID and shotID and hasInlineShotOverride
            //      __fetch__ page only
            // if pageID and !shotID
            //      __fetch__ page only
            if (this.data.pageID &&
                (!this.data.shotID || (this.data.shotID && hasInlineShotOverride))) {
                fetch(pageURL)
                    .then((response) => response.json())
                    .then((data) => {
                        this.data.pageLayout = data.template;
                        this.data.pageContent = data.blocks;
                        this.data.shotContent = null;
                        this.data.pageGenericPath = data.genericPath;
                        this.historyAdd();
                        return resolve(this.data);
                    })
                    .catch((error) => {
                        this.data.pageLayout = null;
                        this.data.pageContent = null;
                        this.data.shotContent = null;
                        console.info(error);
                    })
            }

            // if pageID and shotID and !hasInlineShotOverride
            //      __fetch__ page and shot
            if (this.data.pageID && this.data.shotID && !hasInlineShotOverride) {
                fetch(pageURL)
                    .then((response) => response.json())
                    .then((data) => {
                        this.data.pageLayout = data.template;
                        this.data.pageContent = data.blocks;
                        this.data.pageGenericPath = data.genericPath;
                    })
                    .catch((error) => {
                        console.info(error);
                        this.data.pageLayout = null;
                        this.data.pageContent = null;
                    })
                    .finally(() => {
                        fetch(shotURL)
                            .then((response) => response.json())
                            .then((data) => {
                                this.data.shotContent = data;
                                this.data.shotGenericPath = data.shotGenericPath;
                                this.historyAdd();
                                return resolve(this.data);
                            })
                            .catch((error) => {
                                console.info(error);
                                this.data.shotContent = null;
                                return resolve(this.data);
                            })
                    })
            }


            // if !pageID and shotID and !hasInlineShotOverride
            //      __fetch__ shot
            if (!this.data.pageID && this.data.shotID && !hasInlineShotOverride) {
                fetch(shotURL)
                    .then((response) => response.json())
                    .then((data) => {
                        this.data.pageLayout = null;
                        this.data.pageContent = null;
                        this.data.shotContent = data;
                        this.data.shotGenericPath = data.shotGenericPath;
                        this.historyAdd();
                        return resolve(this.data);
                    })
                    .catch((error) => {
                        console.info(error);
                        this.data.pageLayout = null;
                        this.data.pageContent = null;
                        this.data.shotContent = null;
                        return resolve(this.data);
                    })
            }
        })
    }

    historyAdd() {
        // If pointer is not at end of history,
        // delete everything after pointer before adding.
        if (this.historyPointer != this.history.length - 1) {
            this.history.length = this.historyPointer + 1;
        }

        // If history.length > X, remove first
        if (this.history.length >= 10) {
            this.history.shift();
            this.historyPointer = this.history.length - 1;
        }
        this.history.push(cloneDeep(this.data));
        this.historyPointer = this.history.length - 1;
    }

    historyBack() {
        // move pointer - 1
        if (!this.historyPointer) { // is at 0
            return null;
        }
        this.historyPointer--;
        return this.history[this.historyPointer];

    }

    historyForward() {
        // move pointer + 1
        // Cannot go beyond history.length - 1

        if (this.historyPointer >= this.history.length - 1) {
            return null;
        }
        this.historyPointer++;
        return this.history[this.historyPointer];
    }

}