import { get } from 'svelte/store';
import {supportsWebp, supportsSrcSet} from './store';

function updateLazyLoading(observer) {
    // Check if observer is valid
    if (observer.rootMargin) { // unobserve all
        console.log("INFO: Removing all targets from lazy loading observer");
        observer.disconnect();
        // observe
        const lazyImg = document.querySelectorAll('[id^="lazy_"]');
        console.log(`INFO: Lazy images: ${lazyImg}`);
        lazyImg.forEach((target) => {
            console.log(`INFO: Observing ${target.id}`);
            observer.observe(target);
        }); }
    else {console.log("WARN: No observer");
        
    }

}

/**
 * 
 */
function lazyLoadingSetup() {
    const lazyImg = document.querySelectorAll('[id^="lazy_"]');
    lazyImg.forEach((target) => { console.log(`Found Lazy ${target}`); })

    //console.log(`LAZYLOADING: supports srcset ${supportsSrcSet}, supports webp ${supportsWebp}`);

    const options = {
        root: null,
        threshold: 0.5,
    };

    let callback = (entries, observer) => { //TODO this thing should not be called 'observer'?!
        entries.forEach((entry) => {
            //setTimeout(() => {

                // Early return if not intersecting
                if (!entry.isIntersecting) {
                    return;
                }
                if (get(supportsWebp)){
                    entry.target.srcset = entry.target.dataset.srcsetwebp;

                }else{
                    entry.target.srcset = entry.target.dataset.srcset;
                }
                console.log(`INFO: lazyLoading ${entry.target.id}`);
                if (!get(supportsSrcSet)) {
                    console.log("INFO: Browser does not support srcset. Updating src tag.");
                    entry.target.src = entry.target.dataset.src;
                }

            //}, 2000);
        });
    };


    const observer = new IntersectionObserver(callback, options);
    console.log("INFO: Created new intersectionObserver");

    lazyImg.forEach((target) => {
        console.log(`INFO: Observing ${target.id}`);
        observer.observe(target);
    });

    return observer;

}

export { lazyLoadingSetup, updateLazyLoading };