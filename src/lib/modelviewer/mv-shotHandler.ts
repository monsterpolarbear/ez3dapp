import { get } from 'svelte/store';
import type { IShot, IAnimGraphTrigger, IAnimGraphTriggerData } from '../types/interface';
import type { ModelViewerElement } from '@google/model-viewer';
// import ucxMeshMapping from '../../../pre/data/mv_ucx_mesh_mapping.json';
// importthis.gltfLoadData from '../../../pre/data/gltf_load.json';
import { currentModel, isLoading, cameraValues, shotID } from '../store';
import utilsConfig from '../../../utils/utils-config';
// import type * as THREE from 'three';
import cloneDeep from 'lodash.clonedeep';
import type { SphericalPosition } from '@google/model-viewer/lib/features/controls';
import type { Vector3D } from '@google/model-viewer/lib/model-viewer-base';

class ShotHandler {
    dev;
    modelviewer: ModelViewerElement;
    shotData: IShot;
    scene;
    progressScreen: HTMLDivElement;
    progressBarWrapper: HTMLDivElement;
    progressBar: HTMLDivElement;
    progressAmount: HTMLSpanElement;
    lineRenderElements: Array<HTMLOrSVGElement> = [];
    currentOrbit: SphericalPosition;
    nextOrbit: SphericalPosition;
    currentCameraTarget: Vector3D;
    nextCameraTarget: Vector3D;
    oCollections; // object collections
    animGraphCurrent: IAnimGraphTrigger = {}; // a clone of the animGraph to compare if already playing
    animGraphToPlay: IAnimGraphTrigger = {}; // The altered animGraph. The first one will always be played and removed
    ucxMeshMapping;
    gltfLoadData;
    ezTweenStore = {};

    constructor(v: ModelViewerElement, s: IShot, gltfLoadData, collections, ucxMEshMapping, dev) {
        this.dev = dev;
        this.modelviewer = v;
        this.shotData = s;
        this.ucxMeshMapping = ucxMEshMapping;
        this.gltfLoadData = gltfLoadData;
        this.oCollections = collections;
        this.modelviewer.exposure = 0;
        this.modelviewer.environmentImage = "d/cubemaps/default.hdr";

        // Add the progressbar
        this.progressScreen = document.createElement('div');
        this.progressScreen.classList.add("__pscreen__");

        this.progressBarWrapper = document.createElement('div');
        this.progressBarWrapper.classList.add("__pbar__");
        this.progressBar = document.createElement('div');
        this.progressBarWrapper.appendChild(this.progressBar);

        this.progressAmount = document.createElement('span');
        this.progressAmount.innerText = "0%";

        this.progressScreen.appendChild(this.progressBarWrapper);
        this.progressScreen.appendChild(this.progressAmount);

        this.progressScreen.slot = `progress-bar`;

        this.modelviewer.appendChild(this.progressScreen);

        this.modelviewer.addEventListener('camera-change', this.handleCameraInteraction.bind(this));
        this.modelviewer.addEventListener('progress', this.updateProgressbar);
        this.modelviewer.addEventListener('finished', this.handleAnimationFinished);


    }

    /**
     * Catch if shot has .load data
     * @returns 
     */
    handle() {
        // console.info(this.shotData);
        if (this.shotData.load) {
            this.loadModel(cloneDeep(this.shotData));
            return;
        }
        // If no model loaded and no .load data in shot, do nothing
        if (!get(currentModel)) {
            // No model loaded. TODO sollte trotzdem den modelviewer starten.
            console.info("No model loaded and no model to load.")
            return;
        }
        this.applyShot();
    }

    updateProgressbar = (event) => {
        if (!this.progressScreen || !get(isLoading)) {
            return;
        }
        this.progressBar.style.width = event.detail.totalProgress * 100 + "%";
        this.progressAmount.innerText = Math.trunc(event.detail.totalProgress * 100) + "%";

    }

    removeAllChildNodes() {
        // Remove all annotations and hotspots
        const annot = document.querySelectorAll('model-viewer > [slot^="hotspot"]');
        const svg = document.querySelectorAll('model-viewer > svg');
        if (annot.length) {
            for (const el of annot) {
                // console.log(el);
                el.parentNode.removeChild(el);
            }
        }

        if (svg.length) {
            for (const el of svg) {
                console.info("Removing SVG: ")
                console.log(el);
                el.parentNode.removeChild(el);
            }
        }


        this.lineRenderElements.length = 0;
    }

    loadLoadingModel() {
        console.info("Loading loading model")
        this.modelviewer.src = "d/loading.glb";
        this.modelviewer.minCameraOrbit = `auto auto 0.1m`;
        this.modelviewer.maxCameraOrbit = `auto auto auto`;
        this.modelviewer.maxFieldOfView = '150deg'; //data.fov;
        this.modelviewer.minFieldOfView = '10deg';
    }

    async loadModel(s: IShot) {
        console.info("Load model!")

        const loadFinishedHandler = () => {
            console.log('New model loaded');
            currentModel.set(loadInfo.model);

            const $scene = Object.getOwnPropertySymbols(this.modelviewer).filter(symbol => symbol.description === 'scene')[0];
            this.scene = this.modelviewer[$scene];

            this.scene.name

            isLoading.set(false);

            if (this.progressScreen) {
                this.progressScreen.style.width = "0rem";
            }


            this.currentOrbit = this.modelviewer.getCameraOrbit();
            this.currentCameraTarget = this.modelviewer.getCameraTarget();


            // Currently all blendfiles for a project export into this
            // one collections.json. 
            // Since it is not yet implemented that we can switch the project
            // in a running ez3D app, we could do the following once at the start. 
            this.scene.echtzeit.collections = this.oCollections;


            this.scene.echtzeit.clearAndCollect();

            if (this.dev) {
                this.scene.echtzeit.sceneHierarchy();
                this.scene.echtzeit.visualizeNodes();
                this.modelviewer.addEventListener('camera-change', (e) => {

                    // yaw
                    // pitch
                    // distance
                    this.modelviewer.getCameraOrbit().radius
                    cameraValues.set(
                        {
                            yaw: Math.round(this.radiansToDegrees(this.modelviewer.getCameraOrbit().theta) * 100) / 100, //theta=azimuth am äquator rechts links
                            pitch: Math.round(this.radiansToDegrees(this.modelviewer.getCameraOrbit().phi) * 100) / 100,// phi=polar
                            distance: Math.round(this.modelviewer.getCameraOrbit().radius * 100) / 100,
                            fov: Math.round(this.modelviewer.getFieldOfView() * 100) / 100,
                            x: Math.round(this.modelviewer.getCameraTarget().x * 100) / 100,
                            y: Math.round(this.modelviewer.getCameraTarget().y * 100) / 100,
                            z: Math.round(this.modelviewer.getCameraTarget().z * 100) / 100,
                        }
                    )

                })
            }

            // Apply shot with inital anim
            this.applyShot(true);


        };


        // Shot.load is array for additive loading. For now, we can only load one model. Therefore
        // we only load the first one.
        if (!s?.load?.length) {
            console.info("No model to load");
            this.applyShot();
            return
        }
        if (!this.gltfLoadData) {
            console.info("No gltfLoadData");
            return
        }

        const loadInfo = this.gltfLoadData[s.load[0]];


        if (!loadInfo) {
            console.info("Model to load is not in list")
            this.applyShot();
            return;
        }

        if (get(currentModel) === loadInfo.model) {
            console.log('Model already loaded');
            this.applyShot();
            return;
        }
        isLoading.set(true);

        this.removeAllChildNodes();


        // set the new source
        this.modelviewer.src = loadInfo.model.replace(".glb", "_merged.glb");

        this.modelviewer.minCameraOrbit = `auto auto 0.1m`;
        this.modelviewer.maxCameraOrbit = `auto auto auto`;
        this.modelviewer.maxFieldOfView = '150deg'; //data.fov;
        this.modelviewer.minFieldOfView = '10deg';


        this.modelviewer.removeEventListener('load', loadFinishedHandler.bind(this));
        this.modelviewer.addEventListener('load', loadFinishedHandler.bind(this));

    }



    degreesToRadians(degrees) {
        const pi = Math.PI;
        return degrees * (pi / 180);
    }

    radiansToDegrees(radians) {
        return radians * 180 / Math.PI;
    }

    handleCameraInteraction(event) {
        if (event.detail.source === 'user-interaction') {
            // Camera changed by user
            this.modelviewer.interpolationDecay = 50;
        } else {
            this.modelviewer.interpolationDecay = 200;
        }
    }

    handleAnimationFinished = (event) => {
        // console.info(this.modelviewer.animationName)
        // console.info("Loop count ", event.detail.count);

        // split animationname by @ to get rigname 
        // this.animGraphToPlay
        if (!this.modelviewer.animationName) {
            return;
        }
        const rigName = this.modelviewer.animationName.split('@')[0];
        this.handleAnimation(this.animGraphToPlay[rigName]);

    }

    applyShot(initialAnim = false) {
        console.info("Apply Shot");

        const sData: IShot = cloneDeep(this.shotData);

        if (initialAnim) {
            // When model is loaded, apply materialTweaks and uvOffsets:
            if (sData?.materialTweaks) {
                console.info("MaterialTweaks")
                this.scene.echtzeit.materialTweaks(sData.materialTweaks);
            }

            if (sData?.uvOffset) {
                this.scene.echtzeit.uvOffset(sData.uvOffset);
            }
        }


        this.scene.echtzeit.isShotNew(get(shotID));

        // set camera and env
        if (sData?.animation) {
            Object.keys(sData.animation).forEach(entityName => {
                const anim = sData?.animation[entityName];


                switch (anim.type) {
                    case 'camera':
                        this.currentOrbit = this.modelviewer.getCameraOrbit();
                        this.nextOrbit = cloneDeep(this.currentOrbit);
                        this.currentCameraTarget = this.modelviewer.getCameraTarget();
                        this.nextCameraTarget = cloneDeep(this.currentCameraTarget);
                        // azimuthal = theta
                        // polar = phi (phi is measured down from the top)
                        if (anim.keys[0]?.camera?.float?.yaw != null) {
                            this.nextOrbit.theta = this.degreesToRadians(anim.keys[0].camera.float.yaw);
                        }
                        if (anim.keys[0]?.camera?.float?.pitch != null) {
                            this.nextOrbit.phi = this.degreesToRadians(anim.keys[0].camera.float.pitch);
                        }
                        if (anim.keys[0]?.camera?.float?.distance != null) {
                            this.nextOrbit.radius = anim.keys[0].camera.float.distance;
                        }



                        if (anim.keys[0]?.camera?.pivotPoint != null) {


                            if (typeof anim.keys[0].camera.pivotPoint === "string") {
                                // find object in scene and get its position
                                const s = anim.keys[0].camera.pivotPoint;
                                console.info("Is string ", s)
                                const foundPosition = this.scene.echtzeit.worldPosFromObjectName(s)
                                if (foundPosition) {
                                    anim.keys[0].camera.pivotPoint = foundPosition;
                                }

                            }
                            if (anim.keys[0]?.camera?.pivotPoint?.x != null) {
                                this.nextCameraTarget.x = anim.keys[0].camera.pivotPoint.x;
                                this.nextCameraTarget.y = anim.keys[0].camera.pivotPoint.y;
                                this.nextCameraTarget.z = anim.keys[0].camera.pivotPoint.z;
                            }
                        }

                        if (anim.keys[0]?.camera?.fov != null) {
                            //if (anim.keys[0]?.camera?.minFov != null) {
                            //    this.modelviewer.minFieldOfView=anim.keys[0].camera.minFov.toString() + "deg";
                            //}
                            //if (anim.keys[0]?.camera?.maxFov != null) {
                            //    this.modelviewer.maxFieldOfView=anim.keys[0].camera.maxFov.toString() + "deg";
                            //}
                            this.modelviewer.fieldOfView = anim.keys[0].camera.fov.toString() + "deg";

                        }

                        // this.modelviewer.interpolationDecay = 50;
                        this.modelviewer.cameraOrbit = this.nextOrbit.toString();
                        this.modelviewer.cameraTarget = this.nextCameraTarget.toString();
                        break
                    case 'env':
                        console.info("Current exposure: ", this.modelviewer.exposure)
                        if (anim.keys[0]?.env?.skyboxIntensity != null) {
                            this.scene.echtzeit.exposure(anim.keys[0]?.env?.skyboxIntensity, anim.keys[0]?.duration ? anim.keys[0].duration : 80)
                        }
                        break;

                    default:
                        console.info("No Animation Handler for type ", anim.type)
                }
            })

        }

        // Handle visibilities. Will set enableNodes and fading objects
        this.scene.echtzeit.handleVisibilities(sData, get(currentModel))


        // Check if animGraph in shot is already playing.
        // Compare
        let sameAnimGraph = false;
        if (
            (JSON.stringify(this.animGraphCurrent) === JSON.stringify(sData.animGraph))) {
            console.info("Shot has same animGraph.")
            sameAnimGraph = true;

        }

        if (initialAnim) {

            // When model is loaded, run its first animation.

            // For each gltf in .load, add its first anim at start of animGraph Array.
            // There can be an existing animGraph, or not.

            if (sData?.load?.length) {
                sData.load.forEach(gltf => {
                    if (!Array.isArray(this.gltfLoadData[gltf].animation)) {
                        return;
                    }
                    const rigName = this.gltfLoadData[gltf].rigname;
                    const initAnimName = this.gltfLoadData[gltf].animation[0].name;

                    // Does shot have animGraph.rigname?
                    if (Array.isArray(sData.animGraph[rigName])) {
                        // True -> add init anim at start
                        sData.animGraph[rigName].unshift({ name: initAnimName });
                    } else {
                        // False -> add rigname:[{anim}]
                        if (!sData.animGraph) {
                            sData.animGraph = {}
                        }
                        sData.animGraph[rigName] = [{ name: initAnimName }]
                    }
                })
            }
        }

        if (sData?.animGraph && !sameAnimGraph) {

            this.animGraphCurrent = cloneDeep(sData.animGraph);
            this.animGraphToPlay = cloneDeep(sData.animGraph);
            // Loop over rignames:
            Object.keys(this.animGraphToPlay).forEach(rigName => {
                const graph: Array<IAnimGraphTriggerData> = this.animGraphToPlay[rigName];
                this.handleAnimation(graph);
            })
        }


        if (sData.background) {

            const container = document.getElementById("ezGradientContainer");


            if (!container) {
                return
            }

            // early return if gradients are the same.


            // Add new element, fade in
            const x = sData.background.X
            const y = sData.background.Y
            const cols = sData.background.colors.join(",")

            const newBG = document.createElement("div");

            // Class set in app.scss
            newBG.classList.add("ezBG");

            newBG.style.backgroundImage = `radial-gradient(circle at ${x}% ${y}%, ${cols}),url("./grain.jpg")`;
            newBG.style.width = "100%";
            newBG.style.height = "100%";
            newBG.style.animationDuration = "2s";
            newBG.style.animationName = "fadeIn";
            newBG.style.animationTimingFunction = "ease-out";
            container.appendChild(newBG);

            // Remove previousSibling when anim done
            newBG.addEventListener("animationend", () => { if (event.target.previousSibling) { event.target.previousSibling.remove() } });

            // add fadeIn class
            // newBG.classList.add("ezGradientFade");

        }

        if (!sData?.keepHotspots) {
            this.handleHotspots(sData);
        }

        if (initialAnim) {
            // Remove poster frame
            console.info("Dismiss poster")
            this.modelviewer.dismissPoster();
        }
        //await this.modelviewer.updateComplete;
        //this.modelviewer.play();
        this.scene.isDirty = true;
    }



    async handleAnimation(animGraph: Array<IAnimGraphTriggerData>) {
        // handle no Array
        if (!Array.isArray(animGraph)) {
            console.info("animGraph not Array")
            return;
        }
        // handle no length
        if (!animGraph.length) {
            // console.info("End of animGraph")
            return;
        }


        // Play first in list and remove it from list.
        // PLAY ONCE and LOOP
        // Modelviewer might be too slow to catch the end of an animation before it loops.
        // Therefore we need to set every anim that is not the last in the graph to only play ONCE!

        const animToPlay = animGraph.shift();
        // Still entries left in animGraph?
        let loop = false;
        if (!animGraph.length) {
            loop = true;
        }

        this.modelviewer.animationName = animToPlay.name;
        if (animToPlay.speed != null) {
            this.modelviewer.timeScale = animToPlay.speed;
        }

        if (animToPlay.crossfade != null) {
            this.modelviewer.animationCrossfadeDuration = animToPlay.crossfade;
        }
        await this.modelviewer.updateComplete;

        if (loop) {
            this.modelviewer.play();
        } else {
            this.modelviewer.play({ repetitions: 1 });
        }
        console.info("Playing anim ", animToPlay.name, " as loop:", loop);
        return;
    };

    startSVGRenderLoop = () => {
        // If no lines to draw, return;
        if (!this.lineRenderElements.length) {
            console.info("SVG Renderloop stopped.")
            return;
        }
        // set x1, y1 and x2,y2 on line
        this.lineRenderElements.forEach((line) => {
            // get data start and end
            const start = this.modelviewer.queryHotspot('hotspot-' + line.dataset.start);
            const end = this.modelviewer.queryHotspot('hotspot-' + line.dataset.end);
            line.setAttribute('x1', start?.canvasPosition?.x);
            line.setAttribute('y1', start?.canvasPosition?.y);
            line.setAttribute('x2', end?.canvasPosition?.x);
            line.setAttribute('y2', end?.canvasPosition?.y);

        })
        requestAnimationFrame(this.startSVGRenderLoop);
    };

    async handleHotspots(sData: IShot) {
        if (!this.ucxMeshMapping) {
            console.info("No ucxMeshMapping");
            return;
        }

        console.info("Handle Hotspots");
        this.removeAllChildNodes();

        //  <div slot="hotspot-hoof" class="anchor" data-surface="0 0 752 733 735 0.132 0.379 0.489"></div>

        // construct data-surface
        if (!sData?.hotspots) {
            return
        }

        Object.keys(sData.hotspots).forEach(hName => {
            //ucxMeshMapping[hName];
            if (!this.ucxMeshMapping[get(currentModel)] && !this.ucxMeshMapping[get(currentModel)][hName]) {
                return
            };

            const dataSurface = this.ucxMeshMapping[get(currentModel)][hName] + " 0 0 1 2 1 1 1"
            console.info("Hotspots ", hName, dataSurface);

            const hDiv = document.createElement('div');
            hDiv.classList.add('anchor', 'h_container');
            hDiv.slot = `hotspot-${hName}`;
            //data-visibility-attribute="visible"
            hDiv.setAttribute("data-visibility-attribute", "visible");
            hDiv.dataset.surface = `${dataSurface}`
            hDiv.innerHTML = sData.hotspots[hName].content;

            if (sData?.hotspots[hName]?.classes) {
                hDiv.classList.add(...sData.hotspots[hName].classes);
            }

            const targetName = hName.replace("UCX_", "UCXTARGET_");
            if (this.ucxMeshMapping[get(currentModel)][targetName]) {
                console.info("Hotspot has target ", targetName);

                // Add SVG Container if not exists:
                let svgContainer = this.modelviewer.querySelector('#hotspotLineContainer');
                if (svgContainer !== null && svgContainer.nodeName.toLowerCase() === "svg") {
                    console.info("Has svg container");
                } else {
                    console.info("Adding new svg container")
                    // create new svg container
                    // const svgEl = document.createElement('svg');
                    const svgEl = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                    svgEl.id = "hotspotLineContainer";
                    svgEl.setAttribute("width", "100%");
                    svgEl.setAttribute("height", "100%");
                    // svgEl.setAttribute("xmlns", "http://www.w3.org/2000/svg");
                    // svgEl.classList.add("hotspotLineContainer");
                    this.modelviewer.appendChild(svgEl);
                    svgContainer = svgEl;

                }

                // Add UCXTARGET as hotspot div
                const dataSurface = this.ucxMeshMapping[get(currentModel)][targetName] + " 0 0 1 2 1 1 1"
                const hTargetDiv = document.createElement('div');
                // hTargetDiv.classList.add('anchor', 'h_container');
                hTargetDiv.slot = `hotspot-${targetName}`
                hTargetDiv.dataset.surface = `${dataSurface}`
                // hDiv.innerHTML = sData.hotspots[hName].content;
                this.modelviewer.appendChild(hTargetDiv);

                // Add line Element to SVG Container
                //<line class="line"></line>
                // const lineEl = document.createElement('line');
                const lineEl = document.createElementNS(
                    'http://www.w3.org/2000/svg',
                    'line'
                );
                lineEl.dataset.start = hName; // name of start
                lineEl.dataset.end = targetName;
                lineEl.classList.add("hotspotLine");
                svgContainer?.appendChild(lineEl);
                this.lineRenderElements.push(lineEl);

            } else { console.info("Has no target") }


            if (sData?.hotspots[hName]?.expanded) {
                hDiv.classList.add("expanded");
            }

            this.modelviewer.appendChild(hDiv);
            this.startSVGRenderLoop();
        })
    }
}


export default ShotHandler