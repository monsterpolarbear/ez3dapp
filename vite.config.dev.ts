import { sveltekit } from '@sveltejs/kit/vite';
import { SvelteKitPWA } from '@vite-pwa/sveltekit';
import type { UserConfig } from 'vite';

// Dynamic allow list so scss can import other scss files from 
// content folder during dev
const contentPath = process.env.VITE_CONTENT_PATH;
let allowList = ['pre','utils' ];
if(contentPath){
	allowList.push(contentPath);
}

const config: UserConfig = {
	define: {
		__DATE__: `'${new Date().toISOString()}'`,
		__RELOAD_SW__: false,
	},
	plugins: [sveltekit(),
	SvelteKitPWA({
		srcDir: './src',
		mode: 'development',
		strategies: 'injectManifest',
		filename: 'prompt-sw.ts',
		scope: '/',
		base: '/',
		manifest: {
			short_name: 'SvelteKit PWA',
			name: 'SvelteKit PWA',
			start_url: '/',
			scope: '/',
			display: 'standalone',
			theme_color: "#ffffff",
			background_color: "#ffffff",
			icons: [
				{
					src: '/pwa-192x192.png',
					sizes: '192x192',
					type: 'image/png',
				},
				{
					src: '/pwa-512x512.png',
					sizes: '512x512',
					type: 'image/png',
				},
				{
					src: '/pwa-512x512.png',
					sizes: '512x512',
					type: 'image/png',
					purpose: 'any maskable',
				},
			],
		},
		injectManifest: {
			globPatterns: ['client/**/*.{js,css,ico,png,svg,webp,woff,woff2}']
		},
		devOptions: {
			enabled: true,
			type: 'module',
			navigateFallback: '/',
		}
		// if you have shared info in svelte config file put in a separate module and use it also here
		//kit: {}
	})],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	},
	css: {
		preprocessorOptions: {
			scss: {
				additionalData: '@use "src/variables.scss" as *;'
			}
		}
	},
	server: {
		fs: {
			allow: allowList
		}
	}
};

export default config;
