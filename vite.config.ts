import { sveltekit } from '@sveltejs/kit/vite';
// import { SvelteKitPWA } from '@vite-pwa/sveltekit';
import type { UserConfig } from 'vite';

const config: UserConfig = {
	define: {
		__DATE__: `'${new Date().toISOString()}'`,
		__RELOAD_SW__: false,
	},
	plugins: [
		sveltekit(),
		//SvelteKitPWA({
		//	srcDir: './src',
		//	mode: 'development',
		//	strategies: 'injectManifest',
		//	filename: 'prompt-sw.ts',
		//	scope: '/',
		//	base: '/',
		//	manifest: {
		//		short_name: 'echtzeit3D',
		//		name: 'echtzeit3D',
		//		start_url: '/',
		//		scope: '/',
		//		display: 'standalone',
		//		theme_color: "#581c87",
		//		background_color: "#581c87",
		//		icons: [
		//			{
		//				src: '/pwa-192x192.png',
		//				sizes: '192x192',
		//				type: 'image/png',
		//			},
		//			{
		//				src: '/pwa-512x512.png',
		//				sizes: '512x512',
		//				type: 'image/png',
		//			},
		//			{
		//				src: '/pwa-512x512.png',
		//				sizes: '512x512',
		//				type: 'image/png',
		//				purpose: 'any maskable',
		//			},
		//		],
		//	},
		//	injectManifest: {
		//		globPatterns: ['client/**/*.{js,css,ico,png,svg,webp,woff,woff2,json,glb,dds,wasm}'],
		//		maximumFileSizeToCacheInBytes: 100000000
		//	},
		//	devOptions: {
		//		enabled: true,
		//		type: 'module',
		//		navigateFallback: '/',
		//	}
		//	// if you have shared info in svelte config file put in a separate module and use it also here
		//	//kit: {}
		//})
	],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}']
	},
	css: {
		preprocessorOptions: {
			scss: {
				additionalData: '@use "src/variables.scss" as *;'
			}
		}
	},
	esbuild: {
		drop: ["console"],
		legalComments: "eof",

	},
	server: {
		fs: {
			allow: ['pre', 'utils']
		}
	}
};

export default config;
