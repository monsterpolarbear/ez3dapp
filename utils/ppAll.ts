import preprocess from "./preprocessing.js";
import clonedeep from 'lodash.clonedeep';
// import ftConfig from "../src/ft-pc-config.js";

const pp = new preprocess("./pre", "./static");

// Delete and create empty temp.scss file. This file is used during dev to import scss from content folders.
// vite.config will need same folder in allow list.
pp.createTmpScss();


pp.prettyPrintJson = false;

pp.getFiles();

// Process Shots   
pp.processAllShots();


// Process Pages
 Object.keys(pp.pageMapping).forEach(pageID =>{
    Object.keys(pp.pageMapping[pageID].paths).forEach(localeOfPath=>{ 
        if(pp.ftConfig.locales.includes(localeOfPath)){

            console.info("\n______Processing Page ", pp.pageMapping[pageID].cleanname);
            const pagePath = pp.pageMapping[pageID].paths[localeOfPath];

            const { outputPathLocalized, locale, mdObject } = pp.buildPageContent(pagePath, pageID);
            console.info(outputPathLocalized)
            pp.savePage(outputPathLocalized, locale, mdObject);
           
        }
    })
});




// pp.compileHotspotCss();

