import preprocess from "./preprocessing.js";
// import ftConfig from "../src/ft-pc-config.js";
var pp = new preprocess("./pre", "./static");
// Delete and create empty temp.scss file. This file is used during dev to import scss from content folders.
// vite.config will need same folder in allow list.
pp.createTmpScss();
pp.prettyPrintJson = false;
pp.getFiles();
// Process Shots   
pp.processAllShots();
// Process Pages
Object.keys(pp.pageMapping).forEach(function (pageID) {
    Object.keys(pp.pageMapping[pageID].paths).forEach(function (localeOfPath) {
        if (pp.ftConfig.locales.includes(localeOfPath)) {
            console.info("\n______Processing Page ", pp.pageMapping[pageID].cleanname);
            var pagePath = pp.pageMapping[pageID].paths[localeOfPath];
            var _a = pp.buildPageContent(pagePath, pageID), outputPathLocalized = _a.outputPathLocalized, locale = _a.locale, mdObject = _a.mdObject;
            console.info(outputPathLocalized);
            pp.savePage(outputPathLocalized, locale, mdObject);
        }
    });
});
// pp.compileHotspotCss();
