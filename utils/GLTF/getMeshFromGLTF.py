import json
import os
from ucx import getExportLocation
from pathlib import Path

def getMeshFromGLTF(gltf_filepath):
    # import gltf file as json
    with open(gltf_filepath) as f:
        d = json.load(f)
    mapping = {}
    # in nodes, find UCX
    print("nodes" in d)
    if not "nodes" in d:
        print("No nodes in file")
        return

    for n in d["nodes"]:
        if "name" in n:
            if n["name"].startswith("UCX_"):
                # print("Found ", n["name"], n["mesh"])
                mapping[n["name"]]=n["mesh"]

    # write mapping UCX:prim
    
    return mapping

def write_mapping_to_file(data):
    export_location = getExportLocation()
    out_file = os.path.join(export_location, "pre","data","mv_ucx_mesh_mapping.json")
    Path(os.path.dirname(out_file)).mkdir(parents=True, exist_ok=True)
    with open(out_file, "w", encoding="utf8") as f:
        json.dump(data, f, indent=4)  


gltf_filepath="/tmp/foo.gltf"
mapping = getMeshFromGLTF(gltf_filepath)
print(mapping)
# write_mapping_to_file(mapping)