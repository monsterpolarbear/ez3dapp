import bpy
import sys
import json
from pathlib import Path
import os

def export(fileoutpath, format):
    bpy.ops.export_scene.gltf(
            filepath=fileoutpath, 
            check_existing=True, 
            export_format=format, 
            ui_tab='GENERAL', 
            export_copyright='', 
            export_image_format='AUTO', 
            export_texture_dir='', 
            export_keep_originals=False, 
            export_texcoords=True, 
            export_normals=True, 
            export_draco_mesh_compression_enable=True, 
            export_draco_mesh_compression_level=6, 
            export_draco_position_quantization=14, 
            export_draco_normal_quantization=10, 
            export_draco_texcoord_quantization=12, 
            export_draco_color_quantization=10, 
            export_draco_generic_quantization=12, 
            export_tangents=False, 
            export_materials='EXPORT', 
            # export_original_specular=False, 
            export_colors=True, 
            use_mesh_edges=True, 
            use_mesh_vertices=True, 
            export_cameras=False, 
            use_selection=False, 
            use_visible=False, 
            use_renderable=False, 
            use_active_collection=False, 
            use_active_scene=True, 
            export_extras=False, 
            export_yup=True, 
            export_apply=True, 
            export_animations=True, 
            export_frame_range=True, 
            export_frame_step=1, 
            export_force_sampling=True, 
            export_nla_strips=True, 
            # export_nla_strips_merged_animation_name='Animation', 
            export_def_bones=False, 
            # export_optimize_animation_size=False, 
            # export_anim_single_armature=True, 
            export_current_frame=False, 
            export_skins=True, 
            export_all_influences=False, 
            export_morph=True, 
            export_morph_normal=True, 
            export_morph_tangent=False, 
            export_lights=False, 
            will_save_settings=False, 
            filter_glob='*.glb;*.gltf')


def getMeshFromGLTF(gltf_filepath):
    # import gltf file as json
    with open(gltf_filepath) as f:
        d = json.load(f)
    mapping = {}
    # in nodes, find UCX
    print("nodes" in d)
    if not "nodes" in d:
        print("No nodes in file")
        return

    for n in d["nodes"]:
        if "name" in n:
            if n["name"].startswith("UCX_") or n["name"].startswith("UCXTARGET_"):
                # print("Found ", n["name"], n["mesh"])
                mapping[n["name"]]=n["mesh"]

    # write mapping UCX:prim
    
    return mapping

def write_data_to_file(output_folder, data, filename):
    out_file = os.path.join(output_folder,filename)
    Path(os.path.dirname(out_file)).mkdir(parents=True, exist_ok=True)
    with open(out_file, "w", encoding="utf8") as f:
        json.dump(data, f, indent=4)  


def mergeAnims(folder, geo_gltf,armature_name, fps, data_output):
    
    to_import = os.path.join(folder,geo_gltf)
    print(to_import)
    if not to_import:
        return

    # Export Geo will use Filename without version. This is because there can be more than just an armature.
    # Animation name is rigname@animname. So the rig needs to be named as the file.
    # Find all animations
    animfiles = [filename for filename in os.listdir(folder) if filename.startswith(armature_name + "@")]
    # print(animfiles)
    if not animfiles:
        return

    # Create entry for animData file that will contain modelname and all anims. 
    # Put "init" anim first.
    animList =  []
    for a in animfiles:
        # name = a[a.find("@")+len("@"):a.rfind(".glb")] # find string between substrings
        name = os.path.splitext(a)[0]
        print("Found anim " + name)
        # Init anim at start of list:
        if a.endswith("@init.glb"):
            animList.insert(0,{"name":name, "loop":False, "path": "d/" + a})
        else:
            animList.append({"name":name, "loop":False, "path": "d/" + a})
    load_entry = {"model":"d/" + geo_gltf, "rigname":armature_name,"animation":animList}
    # Write at the end when export has finised...



    # For modelviewer we need a 'rest'
    found_rest = False
    for anm in animfiles:
        if anm.endswith("@rest.glb"):
            found_rest = True
    
    if not found_rest:
        print("\n\nERROR: No Rest animation\n")
        print("Make sure to include rigname@rest.glb")
        return

    # Open new file
    bpy.context.scene.render.fps = fps

    # empty the scene
    objs = bpy.data.objects
    for o in objs:
        objs.remove(o, do_unlink=True)

    # import geo gltf
    bpy.ops.import_scene.gltf(
        filepath=to_import
        )

    # Create a scene to import anims into
    bpy.ops.scene.new(type='NEW')
    cloneScene = bpy.context.scene
    cloneScene.name = 'anims'
    bpy.context.scene.render.fps = fps


    # import animations
    for a in animfiles:
        anim_path = os.path.join(folder, a)
        bpy.ops.import_scene.gltf(
            filepath=anim_path
            )
    
    # Switch back to main scene
    bpy.context.window.scene =bpy.data.scenes['Scene']

    # Switch to REST POSE
    bpy.data.armatures[armature_name].pose_position="REST"

    # load actions onto armature
    bpy.data.objects[armature_name].animation_data_create()
    
    # clean action names
    rest_action = None
    for ac in bpy.data.actions:
        # Blender seems to add "_rigname.iterator" to actions.
        # remove everything from right until _
        splitname = ac.name.rsplit("_",1)
        cleaned_actionname = splitname[0]

        
        ac.name = cleaned_actionname
        print("Cleaned action name ", cleaned_actionname)
        # If name endswith @rest, save the action so we can set this action later as default
        if ac.name.endswith("@rest"):
            rest_action = ac

    if not rest_action:
        print("ERROR: Could not find rest action in scene after naming cleanup")
        return
    

    for ac in bpy.data.actions:
        bpy.data.objects[armature_name].animation_data.action = ac
        print("Copied anim ", ac.name)

    # Set rest anim as active
    bpy.data.objects[armature_name].animation_data.action = rest_action

    # Switch to POSE
    bpy.data.armatures[armature_name].pose_position="POSE"

    # Save this blendfile for troubleshooting
    bpy.ops.wm.save_as_mainfile(filepath="/tmp/mergeAnims.blend")

    # exports active scene only
    fileoutpath = os.path.join(folder, geo_gltf.replace(".glb","") + "_merged.glb")
    format = "GLB"
    print("Writing merged to ", fileoutpath  )

    export(fileoutpath, format)
    print("______ MERGED SAVED ________ \n\n\n\n")
    
    # export the json version too
    fileoutpath = os.path.join("/tmp/", "foo.gltf")
    format = "GLTF_EMBEDDED"
    print("Writing merged to ", fileoutpath  )

    export(fileoutpath, format)

    mapping = getMeshFromGLTF(fileoutpath)
    
    #
    # Write mesh mapping file:
    # If mv_ucx_mesh_mapping.json exists, load the data.
    mesh_mapping_filepath  = os.path.join(data_output, "mv_ucx_mesh_mapping.json")
    if Path(mesh_mapping_filepath).exists():
        # load and update, save
        with open(mesh_mapping_filepath) as f:
            d = json.load(f)
        # Add or update entry of this gltf file
        d[load_entry["model"]]=mapping
        with open(mesh_mapping_filepath, "w", encoding="utf8") as f:
            json.dump(d, f, indent=4)  
    else:
        # create
        d={}
        d[load_entry["model"]]=mesh_mapping_filepath
        write_data_to_file(data_output, d,"mv_ucx_mesh_mapping.json")
    
    #
    # Write gltf load file:
    # If gltf_load.json exists, load the data.
    gltf_load_filepath  = os.path.join(data_output, "gltf_load.json")
    if Path(gltf_load_filepath).exists():
        # load and update, save
        with open(gltf_load_filepath) as f:
            d = json.load(f)
        # Add or update entry of this gltf file
        d[load_entry["model"]]=load_entry
        with open(gltf_load_filepath, "w", encoding="utf8") as f:
            json.dump(d, f, indent=4)  
    else:
        # create
        d={}
        d[load_entry["model"]]=load_entry
        write_data_to_file(data_output, d,"gltf_load.json")




running_path = os.path.dirname(os.path.realpath(__file__))
folder = os.path.realpath(os.path.join(running_path, '..','..', "static","d"))
data_output =  os.path.realpath(os.path.join(running_path, '..','..', "pre","data"))

print("Folder: ", folder)
# folder = sys.argv[7]
geo_gltf = sys.argv[7] # named based on blendfile name
armature_name = sys.argv[8]
fps = int(sys.argv[9])
print("Folder: ",folder)
print("Geo GLTF: ",geo_gltf)

# folder = "/home/oli/dev/ft-pc-projects/xemu/static/d/"
# geo_gltf = "xemu.glb"
# armature_name ="xemu"
# fps = 30

mergeAnims(folder, geo_gltf,armature_name, fps, data_output)