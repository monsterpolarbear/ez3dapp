import * as fs from 'fs';
//const fs = require("fs");
import * as path from 'path';
import { IShot, IHotspot, IStatus } from '../src/lib/types/interface';
// import ftConfig from "../src/ft-pc-config.js";

// import * as idPathMapping from "../pre/menu/id_path_mapping.json" assert { type: "json" };
import clonedeep from 'lodash.clonedeep';
import merge from 'lodash.merge';
import { marked } from 'marked';
import DOMPurify from "isomorphic-dompurify";
import { minify } from 'html-minifier';
import sass from 'sass';

enum EMarkdownType {
    hotspot = "h",
    page = "p"
}

interface IBlocks {
    [key: number]: {
        template: string | null,
        start?: number,
        end?: number,
        params: {
            [key: string]: string
        }
    }
}
interface IMarkdownParts {
    template: string,
    blocks: IBlocks,
    genericPath?: string
}

interface IEntityMapping {
    [key: number]: {
        paths: {
            generic: string,
            [key: string]: string
        }
        clearname: string
    }
}

interface INameToIDMapping {
    [key: string]: string
}

export default class preprocess {
    ftConfig;
    templateString: string;
    found: Array<string> = [];
    shotFiles: Array<string> = [];
    shotMapping: IEntityMapping;
    shotNameToIDMapping: INameToIDMapping;
    hotspotFiles: Array<string> = [];
    hotspotMapping: IEntityMapping;
    hotspotNameToIDMapping: INameToIDMapping;
    pageFiles: Array<string> = [];
    pageMapping: IEntityMapping;
    pageNameToIDMapping: INameToIDMapping;
    shotMasterDir: string;
    sourceRootDir: string;
    outputRootDir: string;
    hotspotTemplateDir: string;
    iconsDir: string;
    prettyPrintJson: boolean = false;
    dev: boolean;

    constructor(sourceRootDir: string, outDir: string, dev = false) {
        this.dev = dev;

        // path.join turns './pre' into 'pre'
        this.sourceRootDir = path.join(sourceRootDir);
        this.outputRootDir = path.join(outDir);
        this.shotMasterDir = path.join(sourceRootDir, "s", "_master");

        // dirs not in content
        this.hotspotTemplateDir = path.join('./pre', "h", "_templates");
        this.iconsDir = path.join('./pre', "icons");

        this.loadConfig();

    }

    getFiles() {
        this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
        this.getAllHotspotFiles(path.join(this.sourceRootDir, "h"));
        this.getAllPageFiles(path.join(this.sourceRootDir, "p"));
    }

    /**
     * Need this list during dev to compile css
     */
    getHotspotFilesDev() {
        this.getAllHotspotFiles(path.join(this.sourceRootDir, "h"));
    }

    /**
     * Need this list during dev to compile css
     */
    getShotFilesDev() {
        this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
    }

    /**
     * Need this list during dev 
     */
    getPathFilesDev() {
        this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
        this.getAllPageFiles(path.join(this.sourceRootDir, "p"));
    }

    private getFilesRecursive(dir: string) {
        fs.readdirSync(dir).forEach(file => {
            const absolute = path.join(dir, file);
            if (fs.statSync(absolute).isDirectory()) return this.getFilesRecursive(absolute);
            else return this.found.push(absolute);
        });
    }

    /**
     * For easier lookup, map id to clearname and all paths, and
     * clearname to id in another object
     * @returns IEntityMapping
     */
    private fromFoundListGenerateEntityMapping() {
        const entityMap: IEntityMapping = {};
        const nameToIDMap: INameToIDMapping = {};

        var localRegex = /_([a-z]*-[A-Z]*).md$/;

        this.found.forEach(f => {
            const filename = path.basename(f);
            const dirname = path.dirname(f)

            const localeMatch = filename.match(localRegex);
            let locale: string = null;
            if (localeMatch) {
                locale = localeMatch[1].trim();
            }



            const splitID = filename.split("$__")
            if (splitID.length < 2) {
                return
            }
            const id = splitID[0]
            const name = splitID[1]


            const extension = name.split('.').pop();
            let clearname
            switch (extension) {
                case "md":
                    clearname = name.replace(/_([a-z]*-[A-Z]*).md$/, "");
                    break;
                case "json":
                    clearname = name.replace('.json', "");
                    break
            }

            nameToIDMap[clearname] = id;
            if (!(id in entityMap)) {
                entityMap[id] = {
                    paths: {
                        generic: path.join(dirname, id + "$__" + clearname) // without locale and extension

                    },
                    cleanname: clearname
                }
            }


            if (locale) {
                entityMap[id].paths[locale] = f
            }

        })

        return { entityMap, nameToIDMap };
    }

    getAll(dir: string) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        return clonedeep(this.found);
    }

    getAllShotFiles(dir: string) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        const { entityMap, nameToIDMap } = this.fromFoundListGenerateEntityMapping();
        this.shotMapping = clonedeep(entityMap);
        this.shotNameToIDMapping = clonedeep(nameToIDMap);
        this.shotFiles = clonedeep(this.found);

    }


    getAllHotspotFiles(dir: string) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        const { entityMap, nameToIDMap } = this.fromFoundListGenerateEntityMapping();
        this.hotspotMapping = clonedeep(entityMap);
        this.hotspotNameToIDMapping = clonedeep(nameToIDMap);
        this.hotspotFiles = clonedeep(this.found);

    }

    getAllPageFiles(dir: string) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        // From Found List generate Mapping
        const { entityMap, nameToIDMap } = this.fromFoundListGenerateEntityMapping();
        this.pageMapping = clonedeep(entityMap);
        this.pageNameToIDMapping = clonedeep(nameToIDMap);
        this.pageFiles = clonedeep(this.found);


    }


    processAllShots() {

        Object.keys(this.shotMapping).forEach(sID => {


            const f = this.shotMapping[sID].paths.generic + '.json'


            console.info("\n______Processing Shot ", f);
            const shotDataRaw = this.loadShotJSON(f);
            if (!shotDataRaw) {
                console.info(f, "has no data.")
                return;
            }
            const shotData = this.mergeShotWithMaster(shotDataRaw, sID);

            this.resolveTimelineItems(shotData);
            this.ftConfig.locales.forEach(locale => {
                this.localizeTimelineItemTitles(locale, shotData)
            })


            // Save shot for each locale if there are no hotspots
            if (!shotData.hotspots) {
                console.info(f, "has no hotspots.")
                this.ftConfig.locales.forEach(locale => {
                    const pathLocalized = path.join(this.outputRootDir, "s", locale, sID + '.json')
                    this.saveShot(shotData, pathLocalized)
                })
                return;
            }


            // Only process hotspots if there is at least one .content field
            // const contentCount = this.hotspotsHaveContent(shotData.hotspots);
            // if (contentCount == 0) {
            //     console.info(f, " has hotspots but no content fields.")
            //     // Save unlocalized to public
            //     this.saveShot(shotData, f)
            //     return;
            // }

            // For each hotspot, process markdown for each locale
            // and store in tmp object
            const localeContent = this.buildHotspotAllLocaleContent(shotData.hotspots);


            // Save localized versions.


            // Build shot for each locale
            this.ftConfig.locales.forEach(locale => {

                // Make a copy of shotData
                const tmpShotData = clonedeep(shotData);

                // Each hotspot with .content, replace .content with localized html.
                Object.keys(tmpShotData.hotspots).forEach(hname => {
                    const hData = tmpShotData.hotspots[hname];

                    // Not every hotspot has content
                    if (!hData.content) {
                        return;
                    }

                    hData.content = localeContent[hname][locale];

                });
                const pathLocalized = path.join(this.outputRootDir, "s", locale, sID + '.json')
                this.saveShot(tmpShotData, pathLocalized)
            });

        })
        return;
    }

    loadShotJSON(file: string): IShot | null {
        // Only parse if valid
        const data = fs.readFileSync(file, 'utf8');
        try {
            return JSON.parse(data);
        } catch (e) {
            return null;
        }

    }



    /**
     * 
     */
    mergeShotWithMaster(shotData: IShot, shotID: string) {
        // Merge ancestors of shot down
        // Get ancestors list from id_path_mapping

        //idPathMapping[shotid]
        //const idPathMapping = JSON.parse(fs.readFileSync('pre/menu/id_path_mapping.json', 'utf8'));

        const mp = path.join(this.sourceRootDir, "menu", "id_path_mapping.json")
        const idPathMapping = JSON.parse(fs.readFileSync(mp, "utf8"))


        console.info("Merging shot ", shotID);
        if (!idPathMapping || !idPathMapping[shotID]) {
            console.info("No id_path_mapping");
        };

        const shotHierarchyPath = idPathMapping[shotID];
        if (!shotHierarchyPath) {
            console.info("WARN: Shot not in id_path_mapping. Can not merge with ancestors.")
            return shotData;
        }

        // Load shot data for each shotID in id_path_mapping
        const ancestors = {};
        for (let i = 0; i < shotHierarchyPath.length; i++) {
            console.info("hierarchy ", i)
            console.info(shotHierarchyPath[i]);
            const idToLoad = shotHierarchyPath[i];
            const shotFilePath = this.shotMapping[idToLoad].paths.generic + '.json';
            const data = this.loadShotJSON(shotFilePath);
            ancestors[idToLoad] = data;
        }

        // After each parent->child Merge, the result is the new parent.
        // We start with the oldest ancestor:
        const oldestAncestorID = shotHierarchyPath[0]
        const mergedAncestorData: IShot = clonedeep(ancestors[oldestAncestorID]);
        // console.info("Starting with oldest ancestor:")
        // console.info(mergedAncestorData)

        // Loop over the ancestors and process parameters
        for (let i = 0; i < shotHierarchyPath.length; i++) {
            console.info("\n\nProcessing hierarchy path ", i)
            const parentID = shotHierarchyPath[i];
            const childID = shotHierarchyPath[i + 1];


            if (!childID) {
                // Reached the end of the path.
                // In this case, parentID needs to be the same as shotID (the shot we are processing)
                console.info("-> Reached end of path")
                if (!(parentID == shotID)) {
                    console.info("WARN: End of hierarchy is not current shot!")
                }
                break;
            }

            const childData = ancestors[childID];

            // Parameter situation
            // PARENT       CHILD
            // hasparam    hasparam  -> inherit (merge/replace)
            // noparam     noparam   -> empty object or false
            // hasparam     noparam  -> copy from ancestor
            // noparam      hasparam  -> copy from child
            //             disinherit -> clone from child, REMOVE disinherit

            // Processing .load
            console.info("\nAncestor merge: Load")
            const ancestorLoadData = mergedAncestorData?.load;
            const childLoadData = childData?.load;
            const loadData = this.inheritParam(ancestorLoadData, childLoadData);
            mergedAncestorData.load = loadData;

            // Processing .animation
            // .Camera
            console.info("\nAncestor merge: Camera")
            const ancestorCamData = mergedAncestorData?.animation?.Camera;
            const childCamData = childData?.animation?.Camera;
            const camData = this.inheritObject(ancestorCamData, childCamData);

            // .env
            console.info("\nAncestor merge: env")
            const ancestorEnvData = mergedAncestorData?.animation?.env;
            const childEnvData = childData?.animation?.env;
            const envData = this.inheritObject(ancestorEnvData, childEnvData);


            if (!mergedAncestorData.animation) {
                mergedAncestorData.animation = {}
            }
            mergedAncestorData.animation.env = envData;
            mergedAncestorData.animation.Camera = camData;


            // Build background
            console.info("\nAncestor merge: background")
            const ancestorBackground = mergedAncestorData?.background;
            const childBackground = childData?.background;
            const backgroundData = this.inheritParam(ancestorBackground, childBackground);
            mergedAncestorData.background = backgroundData;


            // Processing .animGraph
            // .animGraph is an object, but it should never be merged with the ancestor.
            console.info("\nAncestor merge: animGraph")
            const ancestorAnimGraph = mergedAncestorData?.animGraph;
            const childAnimGraph = childData?.animGraph;
            const AnimGraph = this.inheritObject(ancestorAnimGraph, childAnimGraph, false);
            mergedAncestorData.animGraph = AnimGraph;


            // Build materialTweaks - is Array, using inheritParam therefore will not merge
            // with ancestor
            console.info("\nAncestor inherit: materialTweaks")
            const ancestorMaterialTweaks = mergedAncestorData?.materialTweaks;
            const childMaterialTweaks = childData?.materialTweaks;
            const materialTweaksData = this.inheritParam(ancestorMaterialTweaks, childMaterialTweaks);
            mergedAncestorData.materialTweaks = materialTweaksData;


            // Build uvOffset - is Array, using inheritParam therefore will not merge
            // with ancestor
            console.info("\nAncestor inherit: uvOffset")
            const ancestorUvOffset = mergedAncestorData?.uvOffset;
            const childUvOffset = childData?.uvOffset;
            const UvOffsetData = this.inheritParam(ancestorUvOffset, childUvOffset);
            mergedAncestorData.uvOffset = UvOffsetData;

            // Build setEnabled
            console.info("\nAncestor merge: setEnabled")
            const ancestorSetEnabled = mergedAncestorData?.setEnabled;
            const childSetEnabled = childData?.setEnabled;
            const setEnabledData = this.inheritParam(ancestorSetEnabled, childSetEnabled);
            mergedAncestorData.setEnabled = setEnabledData;
            console.info("\nAncestor merge: enabledState")
            const ancestorEnabledState = mergedAncestorData?.enabledState;
            const childEnabledState = childData?.enabledState;
            const enabledStateData = this.inheritParam(ancestorEnabledState, childEnabledState);
            mergedAncestorData.enabledState = enabledStateData;

            // Build fadeCollections
            console.info("\nAncestor merge: fadeCollections")
            const ancestorfadeCollections = mergedAncestorData?.fadeCollections;
            const childfadeCollections = childData?.fadeCollections;
            const fadeCollectionsData = this.inheritParam(ancestorfadeCollections, childfadeCollections);
            mergedAncestorData.fadeCollections = fadeCollectionsData;
            console.info("\nAncestor merge: fadeAmount")
            const ancestorfadeAmount = mergedAncestorData?.fadeAmount;
            const childfadeAmount = childData?.fadeAmount;
            const fadeAmountData = this.inheritParam(ancestorfadeAmount, childfadeAmount);
            mergedAncestorData.fadeAmount = fadeAmountData;

            // Build hotspots
            // Hotspots can have an "Ancestor Modifiers". If present, it will be applied to the
            // ancestor object. 
            console.info("\nAncestor merge: hotspots")
            const hotspotMetadata = childData?.hotspotsMeta
            const ancestorHotspots = mergedAncestorData?.hotspots;
            let childHotspots = childData?.hotspots;

            if (!childHotspots) {
                // Cant have undefined, since we need to copy .disinherit into hotpots object:
                childHotspots = {};
            }

            console.info(" - ancestorHotspots")
            console.info(ancestorHotspots);
            console.info(" - childHotspots");
            console.info(childHotspots)
            let hotspotModifier = {};
            // Hotspots can have metadata. In this case, .disinherit is in metadata and needs to 
            // be copied into data.
            if (hotspotMetadata) {
                childHotspots.disinherit = hotspotMetadata.disinherit;
                hotspotModifier = hotspotMetadata.modifier;
            }

            const mergedHotspotData = this.inheritObject(ancestorHotspots, childHotspots, true, hotspotModifier);
            delete mergedHotspotData.disinherit; // Just to be sure
            mergedAncestorData.hotspots = mergedHotspotData;
        }
        // console.info("\n\nReturning merged data:")
        // console.info(JSON.stringify(mergedAncestorData, null, 4));
        // console.info("\n\n\n\n")

        return mergedAncestorData;

        // OLD STUFF:
        /*
                if (!shotData.master) {
                    return shotData;
                }
                if (shotData.master == "isMaster") {
                    // Shot is a master shot and can not
                    // import data from another shot
                    return shotData;
                }
        
        
        
                if (shotData.master.camera) {
                    console.info("master camera")
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.camera);
                    // if shot has no camera, throw
                    if (!baseData?.animation["Camera"]) {
                        throw "Shot " + shotData.master.camera + " has no camera data to import";
                    }
        
                    console.info("Merging camera data from ", shotData.master.camera)
                    // We only want to merge the camera data
        
                    // shotData has no animation
                    if (!shotData.animation) {
                        shotData["animation"] = { "Camera": baseData.animation["Camera"] }
        
                    }
                    // shotData has no "Camera"
                    else if (!shotData.animation["Camera"]) {
                        shotData.animation["Camera"] = baseData.animation["Camera"];
        
                    }
                    else {
                        // shotData has animation and camera
                        const cameraBaseData = clonedeep(baseData.animation["Camera"]);
                        merge(cameraBaseData, shotData.animation["Camera"]);
                        shotData.animation["Camera"] = cameraBaseData;
                    }
        
                }
        
                if (shotData.master.EnvSphere) {
        
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.EnvSphere);
        
                    if (!baseData?.animation["EnvSphere"]) {
                        throw "Shot " + shotData.master.EnvSphere + " has no EnvSphere data to import";
                    }
        
                    console.info("Merging EnvSphere data from ", shotData.master.camera)
        
                    // shotData has no animation
                    if (!shotData.animation) {
                        shotData["animation"] = { "EnvSphere": baseData.animation["EnvSphere"] }
        
                    }
                    // shotData has no "EnvSphere"
                    else if (!shotData.animation["EnvSphere"]) {
                        shotData.animation["EnvSphere"] = baseData.animation["EnvSphere"];
        
                    }
                    else {
                        // shotData has animation and EnvSphere
                        const envSphereBaseData = clonedeep(baseData.animation["EnvSphere"]);
                        merge(envSphereBaseData, shotData.animation["EnvSphere"]);
                        shotData.animation["EnvSphere"] = envSphereBaseData;
                    }
        
                }
        
                if (shotData.master.env) {
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.env);
        
                    if (!baseData?.animation["env"]) {
                        throw "Shot " + shotData.master.env + " has no env data to import";
                    }
        
                    console.info("Merging env data from ", shotData.master.env)
        
                    // shotData has no animation
                    if (!shotData.animation) {
                        shotData["animation"] = { "env": baseData.animation["env"] }
        
                    }
                    // shotData has no "env"
                    else if (!shotData.animation["env"]) {
                        shotData.animation["env"] = baseData.animation["env"];
        
                    }
                    else {
                        // shotData has animation and env
                        const envBaseData = clonedeep(baseData.animation["env"]);
                        merge(envBaseData, shotData.animation["env"]);
                        shotData.animation["env"] = envBaseData;
                    }
                }
        
                if (shotData.master.animGraph) {
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.animGraph);
        
                    if (!baseData?.animGraph) {
                        throw "Shot " + shotData.master.animGraph + " has no animGraph data to import";
                    }
        
                    console.info("Merging animGraph data from ", shotData.master.animGraph)
        
                    // shotData has no animGraph
                    if (!shotData.animGraph) {
                        shotData["animGraph"] = baseData.animGraph;
        
                    } else {
                        // shotData has animGraph
                        const animGraphBaseData = clonedeep(baseData.animGraph);
                        merge(animGraphBaseData, shotData.animGraph);
                        shotData.animGraph = animGraphBaseData;
                    }
                }
        
                if (shotData.master.hotspots) {
                    const baseData = this.lookUpShotAndLoad(shotData.master.hotspots.shot);
        
        
        
                    if (!baseData?.hotspots) {
                        throw "Shot " + shotData.master.hotspots + " has no hotspots data to import";
                    }
        
                    console.info("Merging hotspots data from ", shotData.master.hotspots)
        
                    // shotData has no hotspots
                    if (!shotData.hotspots) {
                        shotData["hotspots"] = {};
        
                    }
                    // shotData has hotspots
                    const hotspotsBaseData = clonedeep(baseData.hotspots);
        
                    // Apply the modifiers to the baseData
                    if (shotData.master.hotspots.modifier) {
                        if (shotData.master.hotspots.modifier.expanded != null) {
                            const expandedVal = shotData.master.hotspots.modifier.expanded
                            Object.keys(hotspotsBaseData).forEach(k => {
                                hotspotsBaseData[k].expanded = expandedVal
                            })
                        }
                    }
        
                    merge(hotspotsBaseData, shotData.hotspots);
                    shotData.hotspots = hotspotsBaseData;
        
        
                    // delete keepHotspots
                    if (shotData.keepHotspots != null) {
                        delete shotData.keepHotspots;
                    }
        
                }
        
                if (shotData.master.setEnabled) {
                    // merge arrays and copy enabledState
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.setEnabled);
        
                    if (!baseData?.setEnabled) {
                        throw "Shot " + shotData.master.setEnabled + " has no setEnabled data to import";
                    }
        
                    console.info("Merging setEnabled data from ", shotData.master.setEnabled)
        
                    // shotData has no setEnabled
                    if (!shotData.setEnabled) {
                        shotData["setEnabled"] = baseData.setEnabled;
                        shotData["enabledState"] = baseData.enabledState;
        
                    } else {
                        // shotData has setEnabled
                        const setEnabledBaseData = clonedeep(baseData.setEnabled);
                        merge(setEnabledBaseData, shotData.setEnabled);
                        shotData.setEnabled = setEnabledBaseData;
                        shotData["enabledState"] = baseData.enabledState;
                    }
        
                }
        
        
                if (shotData.master.timeline) {
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.timeline);
        
                    if (!baseData?.timeline) {
                        throw "Shot " + shotData.master.timeline + " has no timeline data to import";
                    }
        
                    console.info("Merging timeline data from ", shotData.master.timeline)
        
                    // shotData has no timeline
                    if (!shotData.timeline) {
                        shotData["timeline"] = baseData.timeline;
        
                    } else {
                        // shotData has timeline
                        const timelineBaseData = clonedeep(baseData.timeline);
                        merge(timelineBaseData, shotData.timeline);
                        shotData.timeline = timelineBaseData;
                    }
                }
        */
        return shotData

    }

    /**
     * 
     * @param ancestorData 
     * @param childData 
     * @param doInherit set false if data should NOT be merged with ancestor
     * @param modifier 
     * @returns 
     */
    inheritObject(ancestorData, childData, doInherit = true, modifier: boolean | object = false) {

        // parent noParam, child noParam
        if (!childData && !ancestorData) {
            // ancestor and child do not have param. Object will not exist on shot. 
            console.info("parent noParam, child noParam -> Object will not exist on shot.")
            return false;
        }

        // disinherit
        if (childData?.disinherit) {
            console.info("Child does not want to inherit object");
            // Use the data from the child
            // BUT REMOVE disinherit! Otherwise the next child will never inherit.
            const ob = clonedeep(childData);
            delete ob.disinherit;
            return ob;
        }

        // child hasParam, parent hasParam   (merge/replace)
        if (childData && ancestorData) {
            if (doInherit) {
                console.info("child hasParam, parent hasParam -> Merge Object")
                // Apply modifier
                if (modifier) {
                    console.info("Applying modifier");
                    // Maybe this only works with hotspots for now?
                    // For each key,value pair of ancestorData, merge value with modifier
                    for (const [key, value] of Object.entries(ancestorData)) {
                        //console.log(`${key}: ${value}`);
                        const toModifiy = clonedeep(ancestorData[key]);
                        console.info("Before: "); console.info(toModifiy);
                        merge(toModifiy, modifier);
                        console.info("After: "); console.info(toModifiy);
                        ancestorData[key] = toModifiy;
                    }
                }
                return merge(ancestorData, childData);
            } else {
                console.info("child hasParam, parent hasParam, but doInherit is false -> use Child Object")
                return clonedeep(childData);
            }
        }


        // child hasParam, parent noParam   copy from child
        if (childData && !ancestorData) {
            console.info("child hasParam, parent noParam -> use Child Object")
            return clonedeep(childData);
        }
        // child noParam, parent hasParam    copy from ancestor
        if (!childData && ancestorData) {
            console.info("child noParam, parent hasParam -> use Ancestor Object")
            // Apply modifier
            if (modifier) {
                console.info("Applying modifier");
                for (const [key, value] of Object.entries(ancestorData)) {
                    const toModifiy = clonedeep(ancestorData[key]);
                    //console.info("Before: ");console.info(toModifiy);
                    merge(toModifiy, modifier);
                    //console.info("After: ");console.info(toModifiy);
                    ancestorData[key] = toModifiy;
                }
            }
            return clonedeep(ancestorData);
        }
        console.info("None of the cases match -> set object to false")
        return false;
    }

    inheritParam(ancestorData, childData) {
        // parent noParam, child noParam
        if (childData == null && ancestorData == null) {
            // ancestor and child do not have param. Param will not exist on shot. 
            console.info("parent noParam, child noParam -> Param will not exist on shot.")
            return false;
        }
        // disinherit
        if (childData?.disinherit) {
            console.info("Child does not want to inherit parameter");
            // Param will not exist on shot
            return false;
        }

        // child hasParam, parent hasParam   use childs param
        if (childData && ancestorData) {
            console.info("child hasParam, parent hasParam -> use Child Param")
            return childData;
        }

        // child hasParam, parent noParam   use childs param
        if (childData && ancestorData == null) {
            console.info("child hasParam, parent noParam -> use childs param")
            return childData;
        }
        // child noParam, parent hasParam    use ancestors param
        if (childData == null && ancestorData) {
            console.info("child noParam, parent hasParam  ->  use ancestors param")
            return clonedeep(ancestorData);
        }
        console.info("None of the cases match, set param to false")
        return false;
    }

    /**
     * replace shot and page names with IDs in timeline items 
     */
    resolveTimelineItems(shotData: IShot): IShot {
        if (shotData.timeline === "disinherit" || !shotData.timeline || !shotData.timeline.length) {
            return shotData;
        }

        shotData.timeline.forEach(item => {
            if (item.shot) {
                const shotID = this.shotNameToIDMapping[item.shot]
                item.shot = shotID;
            }
            if (item.page) {
                if (item.page.includes("#")) {
                    const pageAndClassSplit = item.page.split("#")
                    const pageID = this.pageNameToIDMapping[pageAndClassSplit[0]];
                    item.page = pageID + "#" + pageAndClassSplit[1];
                } else {
                    const pageID = this.pageNameToIDMapping[item.page];
                    item.page = pageID;
                }

            }
        })

        return shotData
    }

    localizeTimelineItemTitles(locale: string, shotData: IShot): IShot {
        if (shotData.timeline === "disinherit" || !shotData.timeline || !shotData.timeline.length) {
            return shotData;
        }

        shotData.timeline.forEach(item => {
            const localizedTitle = item.title[locale]
            item.title = localizedTitle;

        })
        return shotData

    }

    /**
     * Count hotspots with .content field
     */
    hotspotsHaveContent(hotspots: IHotspot) {
        let contentCount = 0;
        Object.keys(hotspots).forEach(hname => {
            const hData = hotspots[hname];
            if (!hData.content) {
                return;
            } else {
                contentCount++;
            }
        })
        return contentCount;

    }

    /**
     * Map localized html to each hotspot for each locale specified
     * in ft-config
     * @param hotspots 
     */
    buildHotspotAllLocaleContent(hotspots: IHotspot) {
        const localeContent = {};

        Object.keys(hotspots).forEach(hname => {
            const hData = hotspots[hname];
            if (!hData.content) {
                return;
            }
            localeContent[hname] = {};
            // process markdown for each locale, save in temp object
            // [hname]:{locale:html, locale:html}
            this.ftConfig.locales.forEach(locale => {
                const hotspotID = this.hotspotNameToIDMapping[hData.content]
                if (!hotspotID) {
                    return localeContent;
                }
                const hotspotPath = this.hotspotMapping[hotspotID].paths[locale]

                if (!hotspotPath) {
                    return localeContent;
                }

                const mdObject = this.processMarkdownToObject(hotspotPath, locale, EMarkdownType.hotspot, hotspotID);
                const hotspotHtml = this.markdownObjectToHtml(mdObject);
                localeContent[hname][locale] = hotspotHtml;
            })
        })

        return localeContent;
    }

    buildHotspotOneLocaleContent(hotspots: IHotspot, locale: string) {
        const localeContent = {};

        Object.keys(hotspots).forEach(hname => {

            const hData = hotspots[hname];
            if (!hData.content) {
                return;
            }
            localeContent[hname] = {};

            const hotspotID = this.hotspotNameToIDMapping[hData.content]

            if (!hotspotID) {
                return localeContent;
            }
            const hotspotPath = this.hotspotMapping[hotspotID].paths[locale]

            if (!hotspotPath) {
                return localeContent;
            }
            // const mdFilePath = path.join(this.sourceRootDir, "h", hData.content + "_" + locale + ".md");
            const mdObject = this.processMarkdownToObject(hotspotPath, locale, EMarkdownType.hotspot, hotspotID);
            const hotspotHtml = this.markdownObjectToHtml(mdObject);
            localeContent[hname][locale] = hotspotHtml;

        })

        return localeContent;
    }

    /**
     * 
     * @param shotID id of shot
     */
    shotContentFromShot(shotID: string, locale: string) {

        const shotPath = this.shotMapping[shotID].paths.generic + '.json';
        if (!shotPath) {
            return { error: "No file for shot " + this.shotMapping[shotID].clearname };
        }

        const filepath = shotPath; //path.join(this.sourceRootDir, "s", shotName + '.json');

        // Return error if file does not exist
        if (!fs.existsSync(filepath)) {
            return { error: "Could not find file " + filepath };
        }

        //if (shotName.includes('/_master')) {
        //    return { error: "Can not process _master shot" };
        //}

        console.info("\n______Processing Shot ", filepath);
        const shotDataRaw = this.loadShotJSON(filepath);
        if (!shotDataRaw) {
            return { error: "Has no data" };;
        }
        const shotData = this.mergeShotWithMaster(shotDataRaw, shotID);
        shotData.rawData = shotDataRaw; // Inject raw data
        this.resolveTimelineItems(shotData);
        // Keep only localized title in timeline Items
        if (locale != null) {
            this.localizeTimelineItemTitles(locale, shotData)
        }


        shotData.shotGenericPath = this.shotMapping[shotID].paths.generic;
        // Return shot if there are no hotspots
        if (!shotData.hotspots) {
            return { data: shotData };
        }


        // Only process hotspots if there is at least one .content field
        const contentCount = this.hotspotsHaveContent(shotData.hotspots);
        if (contentCount == 0) {
            return { data: shotData };
        }

        // If there are hotspots (if locale is specified), for each hotspot, process markdown for each locale
        // and store in tmp object.
        let localeContent;
        if (locale != null) {
            localeContent = this.buildHotspotOneLocaleContent(shotData.hotspots, locale);
        }
        // Make a copy of shotData
        const tmpShotData = clonedeep(shotData);
        // If shot has hotspots:
        // Each hotspot with .content, replace .content with localized html.
        if (locale != null) {
            Object.keys(tmpShotData.hotspots).forEach(hname => {
                const hData = tmpShotData.hotspots[hname];

                // Not every hotspot has content
                if (!hData.content) {
                    return;
                }

                hData.content = localeContent[hname][locale];

            });
        }
        return { data: tmpShotData }

    }

    /**
     * 
     * @param pageID pageID
     * @param locale 
     * @returns 
     */
    pageContentFromPageAndLocale(pageID: string, locale: string) {


        if (!pageID) {
            console.info("No id for page ", pageID)
            return { outputPathLocalized: pageID, locale, mdObject: null }
        }

        // handle status page
        if (pageID === "__status__") {
            const statusPath = path.join('pre', 'status', 'shotStatus_en-US.md');
            const { outputPathLocalized, locale: l, mdObject } = this.buildPageContent(statusPath, pageID);
            mdObject.genericPath = "__status__";
            return { outputPathLocalized, locale, mdObject };
        }

        const pagePath = this.pageMapping[pageID].paths[locale]
        if (!pagePath) {
            console.info("No file for page ", pageID)
            return { outputPathLocalized: pageID, locale, mdObject: null }
        }

        const filepath = pagePath;

        // Return error if file does not exist
        if (!fs.existsSync(filepath)) {
            return { pageMdFilepath: filepath, locale, mdObject: null };
        }

        const { outputPathLocalized, locale: l, mdObject } = this.buildPageContent(filepath, pageID);
        mdObject.genericPath = this.pageMapping[pageID].paths.generic;
        return { outputPathLocalized, locale, mdObject };
    }

    /**
     * 
     * @param pageMdFilepath filepath with root e.g. pre/p/...
     * @returns 
     */
    buildPageContent(pageMdFilepath: string, pageID: string) {

        // extract locale from filepath
        // _([a-z]*)-([A-Z]*).md$
        var localRegex = /_([a-z]*-[A-Z]*).md$/;
        const layoutMatch = pageMdFilepath.match(localRegex);

        // ok if it crashes here -> user needs to fix source files
        const locale = layoutMatch[1].trim();
        const mdFilePath = path.join(pageMdFilepath);

        const mdObject = this.processMarkdownToObject(mdFilePath, locale, EMarkdownType.page);

        //const pageID = this.pageNameToIDMapping[pageMdFilepath]
        const idName = pageID + '.json';
        const outputPathLocalized = path.join(this.outputRootDir, "p", locale, idName) //f.replace('.json', '_' + locale + '.json');

        return { outputPathLocalized, locale, mdObject };
    }

    /**
     * Turns ft markdown into Object
     * @param markdownFilePath 
     */
    processMarkdownToObject(markdownFilePath: string, locale: string, type: EMarkdownType, hotspotID: string | null = null): IMarkdownParts {
        let markdownContent = fs.readFileSync(markdownFilePath, 'utf8');
        // In markdown file, replace imports, then split everything into
        // an object before processing.

        if (this.dev && hotspotID) {
            // add block to visualize hotspot path

            const devHotspotPath = `<!--@dev-->\n<!--::dev-->\n${this.hotspotMapping[hotspotID].paths.generic}`;
            markdownContent = markdownContent + devHotspotPath;
        }

        // Import and replace linked markdown
        const replacedImports = this.templateImports(markdownContent, locale, type);

        const buildingParts: IMarkdownParts = { template: "", blocks: [] };

        buildingParts.template = this.templateLayout(replacedImports);
        buildingParts.blocks = this.templateBlocks(replacedImports);

        return buildingParts;

    }

    /**
     * For Hotspots: takes markdown-converted object and builds the html string
     * @param buildingParts 
     */
    markdownObjectToHtml(buildingParts: IMarkdownParts): string {
        const allBlocksHtml: Array<string> = [];

        // Process each block. Load the template and fill it with content from params.
        Object.keys(buildingParts.blocks).forEach(index => {
            const blockData = buildingParts.blocks[index];
            // Load html template
            const blockHtmlTemplate = fs.readFileSync(path.join(this.hotspotTemplateDir, "blocks", blockData.template + '.html'), 'utf8');

            // Replace params in html template with content from object
            const blockHtml = blockHtmlTemplate.replace(/%(.*)%/g, function (str, param) { return blockData.params[param] });
            allBlocksHtml.push(blockHtml);

        })

        // Load the Layout template and replace
        // If no template was specified, just return the blocks

        let pureHtml;

        if (!buildingParts.template) {
            pureHtml = DOMPurify.sanitize(allBlocksHtml.join(''));
        } else {
            const layoutHtmlTemplate = fs.readFileSync(path.join(this.hotspotTemplateDir, "layout", buildingParts.template + '.html'), 'utf8');
            const html = layoutHtmlTemplate.replace('%blocks%', allBlocksHtml.join(''));
            pureHtml = DOMPurify.sanitize(html);

        }

        var result = minify(pureHtml, {
            removeAttributeQuotes: true,
            collapseWhitespace: true,
            minifyCSS: true,
            quoteCharacter: "'"
        });

        return result;
    }

    /**
     * Save the rawData into pre/s
     */
    saveRawShot(shotData: IShot, filepath: string) {

        const json = JSON.stringify(shotData, null, 4);
        // fs.mkdirSync(path.dirname(filepath), { recursive: true });
        fs.writeFileSync(filepath + ".json", json);
    }

    /**
     * 
     * @param shotData 
     * @param path 
     */
    saveShot(shotData: IShot, filepath: string): void {
        // Replace first dir in path with outputRoot
        const outFilePath = filepath //.replace(this.sourceRootDir, this.outputRootDir);
        let json
        if (this.prettyPrintJson) {
            json = JSON.stringify(shotData, null, 4);
        } else {
            json = JSON.stringify(shotData)
        }

        // Make folders and write
        fs.mkdirSync(path.dirname(outFilePath), { recursive: true });
        fs.writeFileSync(outFilePath, json);

    }

    /**
     * 
     * @param pageMdFilepath 
     * @param locale 
     * @param mdObject 
     */
    savePage(pageMdFilepath, locale, mdObject): void {
        // Replace sourceRoot with outputRoot
        // Replace .md with .json
        const outFilePath = pageMdFilepath //.replace(this.sourceRootDir, this.outputRootDir);
        let json
        if (this.prettyPrintJson) {
            json = JSON.stringify(mdObject, null, 4);
        } else {
            json = JSON.stringify(mdObject)
        }
        // Make folders and write
        fs.mkdirSync(path.dirname(outFilePath), { recursive: true });
        fs.writeFileSync(outFilePath, json);

    }


    templateLayout(md: string) {
        var layoutRegex = /<!--\$(.*)-->/; // Not global, will only match first occurence
        const layoutMatch = md.match(layoutRegex);
        if (layoutMatch && layoutMatch[1]) {
            return layoutMatch[1].trim();
        } else {
            return null;
        }
    }

    /**
     * Replace import annotations <!--@>importMe--> with markdown. It will not replace imports IN imported markdowns!
     * @param md 
     * @param locale 
     */
    templateImports(md: string, locale: string, type: EMarkdownType): string {
        var importRegex = /<!--@>(.*)-->/g;
        const matches = [...md.matchAll(importRegex)];

        // Collect all paths to md files. Paths are just the cleanname of the file, e.g. toc
        const toImport: Array<string> = [];

        // Extract paths from matches 
        for (let i = 0; i < matches.length; i++) {
            toImport.push(matches[i][1]);
        }

        // Return if nothing to import
        if (!toImport.length) {
            return md;
        }

        // Remove doubles in Array of paths
        let uniqueToImport = [...new Set(toImport)];

        // Initialise object to map content to its filename/import annotation
        const mapMdFileToContent = {}

        uniqueToImport.forEach(clearname => {


            const pageID = this.pageNameToIDMapping[clearname]
            if (!pageID) {
                return
            }
            const pagePath = this.pageMapping[pageID].paths[locale]
            if (!pagePath) {
                return
            }
            // Import and map to annotation
            if (pagePath) {
                mapMdFileToContent[clearname] = fs.readFileSync(pagePath, 'utf8');
            } else {
                mapMdFileToContent[clearname] = ""
            }

        })

        // Replace each import in the md file
        const replaced = md.replace(/<!--@>(.*)-->/g, function (str, nme) { return mapMdFileToContent[nme] })//`${mdContent[$1]}`)
        return replaced;
    }

    /**
     * Build array of blocks
     * @param md 
     * @returns 
     */
    templateBlocks(md: string): IBlocks {
        //const blocks:Array<IBlocks>=[];

        var blockRegex = /<!--@(.*)-->/g;
        const matches = [...md.matchAll(blockRegex)];
        const blocks: IBlocks = {};


        // Collect start and end of each block annotation
        for (let i = 0; i < matches.length; i++) {
            const match = matches[i];

            blocks[i] = {
                template: match[1],
                start: match.index,
                end: match.index + match[0].length,
                params: {}
            };
        }

        // Extract content between block annotations
        for (const order in blocks) {
            const startOfContent = blocks[order].end + 1;

            // If there is no next template, use end of string
            const endOfContent = blocks[parseInt(order) + 1] ? blocks[parseInt(order) + 1].start - 1 : md.length - 1;
            const blockContent = md.substring(startOfContent, endOfContent);

            // Extract parameters of each blockContent
            blocks[order].params = this.extractParamsFromBlock(blockContent);
            delete blocks[order].start;
            delete blocks[order].end;
        }
        return blocks;
    }


    /**
     * From a markdown string with parameter decorators
     * <<!--::param-->>
     * extract param and its content
     * @param blockContent string
     */
    private extractParamsFromBlock(blockContent: string) {
        var paramRegex = /<!--::(.*)-->/g;
        const matches = [...blockContent.matchAll(paramRegex)];
        const temp = {} // storing start and end for each match
        const content = {}

        for (let i = 0; i < matches.length; i++) {
            const match = matches[i];

            // Collect start and end of each decorator.
            // Key need to be the order, so we can get the 
            // text between each decorator in the next step.
            temp[i] = {
                param: match[1].trim(),
                start: match.index,
                end: match.index + match[0].length,
            };
        }

        // Extract the content between each decorator
        // Content is between end of current and start of next decorator
        for (const order in temp) {
            const startOfContent = temp[order].end + 1;

            // If there is no next decorator, use end of string
            const endOfContent = temp[parseInt(order) + 1] ? temp[parseInt(order) + 1].start - 1 : blockContent.length - 1;

            // Translate markdown and replace links.
            content[temp[order].param] = this.ftAnchorToHTML(blockContent.substring(startOfContent, endOfContent))


        }
        return content;
    }

    replaceIcons(content: string): string {
        // match all __SVG__
        var iconRegex = /__SVG__(\S*)/g;
        const matches = [...content.matchAll(iconRegex)];

        const mapSvgNameToMarkup = {};

        for (let i = 0; i < matches.length; i++) {
            const match = matches[i];
            const iconFilePath = path.join(this.iconsDir, match[1] + '.html')
            const markdownContent = fs.readFileSync(iconFilePath, 'utf8');

            // Map svg name to imported markup 
            mapSvgNameToMarkup[match[1]] = markdownContent;
        }
        const replaced = content.replace(/__SVG__(\S*)/g, function (str, nme) { return mapSvgNameToMarkup[nme] });
        return replaced;
    }


    /**
     * Replace ft anchor {{#classnames[page,shot]text}} with html link.
     * page and shot are clearnames, need to be converted to ids.
     * Then generate html.
     * @param md 
     */
    ftAnchorToHTML(md: string) {
        const replaceWithClass = md.replace(/{{#(.*)\[(\S+)]/g, `<a data-sveltekit-preload-data="off" class="$1" REPLACEMEhref="$2" >`);

        // Space before closing tag is important for the match to work!
        const tailReplace = replaceWithClass.replace(/}}/g, " </a>");

        // Go through each href, split by ',' and find its id
        const hrefReplaced = tailReplace.replace(/REPLACEMEhref=(["'])(.*?)\1/g, function (str, foo, anchor) {
            const links = anchor.split(",");
            console.info(links)
            if (!links.length) {
                return "";
            }

            // get id from name
            let pageID: string = "";
            let shotID: string = "";

            if (links[0] && links[0] !== "") {
                if (links[0].startsWith("#")) {
                    pageID = links[0]; // keep highlight class
                } else if (links[0].includes("#")) {
                    const pageAndClassSplit = links[0].split("#");
                    pageID = this.pageNameToIDMapping[pageAndClassSplit[0]] + "#" + pageAndClassSplit[1];

                } else if (links[0] === "__back__" || links[0] === "__forward__") {
                    pageID = links[0];
                } else {
                    pageID = this.pageNameToIDMapping[links[0]]
                }

            }

            if (links[1] && links[1] !== "") {

                if (links[1].startsWith("$")) {
                    // Is inline shot override.
                    shotID = links[1]
                }
                else if (links[1].startsWith("!")) {
                    // Is scroll trigger.
                    // We need to determine, if what comes after, is shot override or shot name
                    if (links[1].startsWith("!$")) {
                        // Is scroll trigger and override.
                        // We simply keep the thing as is:
                        shotID = links[1]
                    }
                    else {
                        // Is scroll trigger, but we need to 
                        // get the id from name (removing the '!' in the lookup)
                        shotID = "!" + this.shotNameToIDMapping[links[1].replace("!", "", 1)];
                    }
                } else if (links[1] === "__back__" || links[1] === "__forward__") {
                    shotID = links[1];
                } else {
                    shotID = this.shotNameToIDMapping[links[1]]
                }
            }

            return ("href=" + pageID + ',' + shotID)
        }.bind(this))




        // Replace all __SVG__ with imported svg file.
        const iconReplace = this.replaceIcons(hrefReplaced);


        const html = marked.parse(iconReplace);

        // const pureHtml = DOMPurify.sanitize(html);
        var result = minify(html, {
            removeAttributeQuotes: true,
            collapseWhitespace: true,
            minifyCSS: true,
            quoteCharacter: "'"
        });

        return result;
    }

    // compileHotspotCss(): void {
    //     // Each in hotspots files ending with scss, process
    //     console.info("\n______Processing Hotspots SCSS")
    //     const cssOutputCollect = [];

    //     this.hotspotFiles.forEach(filename => {
    //         if (!filename.endsWith("scss")) {
    //             return;
    //         }
    //         //const hotspotsScssFilePath = path.join(this.hotspotTemplateDir, "hotspots.scss");
    //         const hotspotsScssFilePath = path.join(filename);
    //         const result = sass.compile(hotspotsScssFilePath, { style: "compressed" });
    //         cssOutputCollect.push(result.css);
    //     })

    //     const outPath = path.join(this.outputRootDir, 'd', "hotspots.css")

    //     fs.mkdirSync(path.dirname(outPath), { recursive: true });
    //     fs.writeFileSync(outPath, cssOutputCollect.join(''));
    // }

    buildShotsStatus(locale) {
        // this.getAllShotFiles(path.join(this.sourceRootDir, "s"));


        // open each paths.generic 
        // read data
        // 
        const statusData = clonedeep(this.shotMapping) as IStatus;


        for (let id of Object.keys(statusData)) {
            const { data } = this.shotContentFromShot(id, locale);

            if (data?.animation?.env) {
                statusData[id].env = true;
            }

            if (data?.animation?.EnvSphere) {
                statusData[id].EnvSphere = true;
            }


            if (data?.animation?.Camera?.keys[0]?.camera?.pivotPoint) {
                statusData[id].cameraPivotPoint = true;
            }
            if (data?.animation?.Camera?.keys[0]?.camera?.fov) {
                statusData[id].cameraFov = true;
            }

            if (data?.animation?.Camera?.keys[0]?.camera?.float?.distance) {
                statusData[id].cameraDistance = true;
            }

            if (data?.animation?.Camera?.keys[0]?.camera?.float?.yaw != null) {
                statusData[id].cameraYaw = true;
            }

            if (data?.animation?.Camera?.keys[0]?.camera?.float?.pitch != null) {
                statusData[id].cameraPitch = true;
            }

            if (data?.animGraph) {
                if (Object.keys(data.animGraph).length) {
                    statusData[id].animGraph = true;
                }
            }

            if (data?.setEnabled && data?.enabledState != null) {
                statusData[id].setEnabled = true;
            }

            if (data?.hotspots) {
                statusData[id].hotspots = true;
            }

            if (data?.timeline) {
                statusData[id].timeline = true;
            }

            if (data?.load) {
                statusData[id].load = true;
            }

            if (data?.master && data.master == "isMaster") {
                statusData[id].isMaster = true;
            }
            if (data.background) {
                statusData[id].background = true;
            }

        }
        return { data: statusData }

    }

    // In the contentPath, create a symlink to modelviewer
    createSymlinks(contentPath = "") {

        const contentMvPath = contentPath + "static/mv";

        const contentDracoPath = contentPath + "static/loader";

        const contentTypesPath = contentPath + "src/lib/types";

        // Delete existing symlink
        if (fs.existsSync(contentMvPath)) {
            fs.unlinkSync(contentMvPath)
        }

        if (fs.existsSync(contentDracoPath)) {
            fs.unlinkSync(contentDracoPath)
        }

        if (fs.existsSync(contentTypesPath)) {
            fs.unlinkSync(contentTypesPath)
        }

        const mvPath = path.join(path.resolve(), "static", "mv")

        const dracoPath = path.join(path.resolve(), "static", "loader")

        const typesPath = path.join(path.resolve(), "src", "lib", "types");

        fs.symlinkSync(mvPath, contentMvPath);

        fs.symlinkSync(dracoPath, contentDracoPath);
        
        
        fs.mkdirSync(path.dirname(contentTypesPath), { recursive: true });
        fs.symlinkSync(typesPath, contentTypesPath);
    }

    // Delete and create a tmp.scss file. This file is empty during build. 
    // In dev, this file contains imports from content path
    createTmpScss(contentPath = "", dev = false) {

        const scssPath = './src/tmp.scss'

        const scssDevString = `@use '${contentPath}src/globalStyles/_export.scss';`
        const scssProdString = `@use './globalStyles/_export.scss';`

        if (fs.existsSync(scssPath)) {
            // Delete existing file
            fs.unlinkSync(scssPath)
        }

        if (dev) {
            fs.writeFileSync(scssPath, scssDevString);
        } else {
            fs.writeFileSync(scssPath, scssProdString);
        }

    }

    loadConfig() {
        console.info("Is dev: ", this.dev);

        if (this.dev) {
            const ftConfigPath = path.join(this.sourceRootDir, "ft-pc-config.json");
            if (!fs.existsSync(ftConfigPath)) {
                return { error: "Could not find file " + ftConfigPath };
            }
            this.ftConfig = this.loadShotJSON(ftConfigPath);
        } else {
            this.ftConfig = this.loadShotJSON("./pre/ft-pc-config.json");
        }
    }


    getDataFromContentPath() {
        // const ftConfigPath = path.join(this.sourceRootDir, "ft-pc-config");
        const hierarchyPath = path.join(this.sourceRootDir, "menu/back_hierarchy.json");
        const menuPath = path.join(this.sourceRootDir, "menu/menu.json");
        const collectionsPath = path.join(this.sourceRootDir, "collections/collections.json");
        const ucxMeshMappingPath = path.join(this.sourceRootDir, "data/mv_ucx_mesh_mapping.json");
        const gltfLoadDataPath = path.join(this.sourceRootDir, "data/gltf_load.json");

        if (!fs.existsSync(hierarchyPath)) {
            return { error: "Could not find file " + hierarchyPath };
        }
        if (!fs.existsSync(collectionsPath)) {
            return { error: "Could not find file " + collectionsPath };
        }
        if (!fs.existsSync(ucxMeshMappingPath)) {
            return { error: "Could not find file " + ucxMeshMappingPath };
        }
        if (!fs.existsSync(gltfLoadDataPath)) {
            return { error: "Could not find file " + gltfLoadDataPath };
        }

        if (!fs.existsSync(menuPath)) {
            return { error: "Could not find file " + menuPath };
        }




        const hierarchyData = this.loadShotJSON(hierarchyPath)
        const collectionsData = this.loadShotJSON(collectionsPath)
        const ucxMeshMappingData = this.loadShotJSON(ucxMeshMappingPath)
        const gltfLoadDataData = this.loadShotJSON(gltfLoadDataPath)
        const menuData = this.loadShotJSON(menuPath)


        if (!hierarchyData || !collectionsData || !ucxMeshMappingData || !gltfLoadDataData || !menuData) {
            return { error: "Has no data" };;
        }

        return {
            data: {
                ftConfig: this.ftConfig,
                backHierarchy: hierarchyData,
                collections: collectionsData,
                ucxMeshMapping: ucxMeshMappingData,
                gltfLoadData: gltfLoadDataData,
                menuData : menuData
            }
        };
    }
}
