var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
import * as fs from 'fs';
//const fs = require("fs");
import * as path from 'path';
// import ftConfig from "../src/ft-pc-config.js";
// import * as idPathMapping from "../pre/menu/id_path_mapping.json" assert { type: "json" };
import clonedeep from 'lodash.clonedeep';
import merge from 'lodash.merge';
import { marked } from 'marked';
import DOMPurify from "isomorphic-dompurify";
import { minify } from 'html-minifier';
var EMarkdownType;
(function (EMarkdownType) {
    EMarkdownType["hotspot"] = "h";
    EMarkdownType["page"] = "p";
})(EMarkdownType || (EMarkdownType = {}));
var preprocess = /** @class */ (function () {
    function preprocess(sourceRootDir, outDir, dev) {
        if (dev === void 0) { dev = false; }
        this.found = [];
        this.shotFiles = [];
        this.hotspotFiles = [];
        this.pageFiles = [];
        this.prettyPrintJson = false;
        this.dev = dev;
        // path.join turns './pre' into 'pre'
        this.sourceRootDir = path.join(sourceRootDir);
        this.outputRootDir = path.join(outDir);
        this.shotMasterDir = path.join(sourceRootDir, "s", "_master");
        // dirs not in content
        this.hotspotTemplateDir = path.join('./pre', "h", "_templates");
        this.iconsDir = path.join('./pre', "icons");
        this.loadConfig();
    }
    preprocess.prototype.getFiles = function () {
        this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
        this.getAllHotspotFiles(path.join(this.sourceRootDir, "h"));
        this.getAllPageFiles(path.join(this.sourceRootDir, "p"));
    };
    /**
     * Need this list during dev to compile css
     */
    preprocess.prototype.getHotspotFilesDev = function () {
        this.getAllHotspotFiles(path.join(this.sourceRootDir, "h"));
    };
    /**
     * Need this list during dev to compile css
     */
    preprocess.prototype.getShotFilesDev = function () {
        this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
    };
    /**
     * Need this list during dev
     */
    preprocess.prototype.getPathFilesDev = function () {
        this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
        this.getAllPageFiles(path.join(this.sourceRootDir, "p"));
    };
    preprocess.prototype.getFilesRecursive = function (dir) {
        var _this = this;
        fs.readdirSync(dir).forEach(function (file) {
            var absolute = path.join(dir, file);
            if (fs.statSync(absolute).isDirectory())
                return _this.getFilesRecursive(absolute);
            else
                return _this.found.push(absolute);
        });
    };
    /**
     * For easier lookup, map id to clearname and all paths, and
     * clearname to id in another object
     * @returns IEntityMapping
     */
    preprocess.prototype.fromFoundListGenerateEntityMapping = function () {
        var entityMap = {};
        var nameToIDMap = {};
        var localRegex = /_([a-z]*-[A-Z]*).md$/;
        this.found.forEach(function (f) {
            var filename = path.basename(f);
            var dirname = path.dirname(f);
            var localeMatch = filename.match(localRegex);
            var locale = null;
            if (localeMatch) {
                locale = localeMatch[1].trim();
            }
            var splitID = filename.split("$__");
            if (splitID.length < 2) {
                return;
            }
            var id = splitID[0];
            var name = splitID[1];
            var extension = name.split('.').pop();
            var clearname;
            switch (extension) {
                case "md":
                    clearname = name.replace(/_([a-z]*-[A-Z]*).md$/, "");
                    break;
                case "json":
                    clearname = name.replace('.json', "");
                    break;
            }
            nameToIDMap[clearname] = id;
            if (!(id in entityMap)) {
                entityMap[id] = {
                    paths: {
                        generic: path.join(dirname, id + "$__" + clearname) // without locale and extension
                    },
                    cleanname: clearname
                };
            }
            if (locale) {
                entityMap[id].paths[locale] = f;
            }
        });
        return { entityMap: entityMap, nameToIDMap: nameToIDMap };
    };
    preprocess.prototype.getAll = function (dir) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        return clonedeep(this.found);
    };
    preprocess.prototype.getAllShotFiles = function (dir) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        var _a = this.fromFoundListGenerateEntityMapping(), entityMap = _a.entityMap, nameToIDMap = _a.nameToIDMap;
        this.shotMapping = clonedeep(entityMap);
        this.shotNameToIDMapping = clonedeep(nameToIDMap);
        this.shotFiles = clonedeep(this.found);
    };
    preprocess.prototype.getAllHotspotFiles = function (dir) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        var _a = this.fromFoundListGenerateEntityMapping(), entityMap = _a.entityMap, nameToIDMap = _a.nameToIDMap;
        this.hotspotMapping = clonedeep(entityMap);
        this.hotspotNameToIDMapping = clonedeep(nameToIDMap);
        this.hotspotFiles = clonedeep(this.found);
    };
    preprocess.prototype.getAllPageFiles = function (dir) {
        this.found.length = 0;
        this.getFilesRecursive(dir);
        // From Found List generate Mapping
        var _a = this.fromFoundListGenerateEntityMapping(), entityMap = _a.entityMap, nameToIDMap = _a.nameToIDMap;
        this.pageMapping = clonedeep(entityMap);
        this.pageNameToIDMapping = clonedeep(nameToIDMap);
        this.pageFiles = clonedeep(this.found);
    };
    preprocess.prototype.processAllShots = function () {
        var _this = this;
        Object.keys(this.shotMapping).forEach(function (sID) {
            var f = _this.shotMapping[sID].paths.generic + '.json';
            console.info("\n______Processing Shot ", f);
            var shotDataRaw = _this.loadShotJSON(f);
            if (!shotDataRaw) {
                console.info(f, "has no data.");
                return;
            }
            var shotData = _this.mergeShotWithMaster(shotDataRaw, sID);
            _this.resolveTimelineItems(shotData);
            _this.ftConfig.locales.forEach(function (locale) {
                _this.localizeTimelineItemTitles(locale, shotData);
            });
            // Save shot for each locale if there are no hotspots
            if (!shotData.hotspots) {
                console.info(f, "has no hotspots.");
                _this.ftConfig.locales.forEach(function (locale) {
                    var pathLocalized = path.join(_this.outputRootDir, "s", locale, sID + '.json');
                    _this.saveShot(shotData, pathLocalized);
                });
                return;
            }
            // Only process hotspots if there is at least one .content field
            // const contentCount = this.hotspotsHaveContent(shotData.hotspots);
            // if (contentCount == 0) {
            //     console.info(f, " has hotspots but no content fields.")
            //     // Save unlocalized to public
            //     this.saveShot(shotData, f)
            //     return;
            // }
            // For each hotspot, process markdown for each locale
            // and store in tmp object
            var localeContent = _this.buildHotspotAllLocaleContent(shotData.hotspots);
            // Save localized versions.
            // Build shot for each locale
            _this.ftConfig.locales.forEach(function (locale) {
                // Make a copy of shotData
                var tmpShotData = clonedeep(shotData);
                // Each hotspot with .content, replace .content with localized html.
                Object.keys(tmpShotData.hotspots).forEach(function (hname) {
                    var hData = tmpShotData.hotspots[hname];
                    // Not every hotspot has content
                    if (!hData.content) {
                        return;
                    }
                    hData.content = localeContent[hname][locale];
                });
                var pathLocalized = path.join(_this.outputRootDir, "s", locale, sID + '.json');
                _this.saveShot(tmpShotData, pathLocalized);
            });
        });
        return;
    };
    preprocess.prototype.loadShotJSON = function (file) {
        // Only parse if valid
        var data = fs.readFileSync(file, 'utf8');
        try {
            return JSON.parse(data);
        }
        catch (e) {
            return null;
        }
    };
    /**
     *
     */
    preprocess.prototype.mergeShotWithMaster = function (shotData, shotID) {
        // Merge ancestors of shot down
        // Get ancestors list from id_path_mapping
        var _a, _b, _c, _d;
        //idPathMapping[shotid]
        //const idPathMapping = JSON.parse(fs.readFileSync('pre/menu/id_path_mapping.json', 'utf8'));
        var mp = path.join(this.sourceRootDir, "menu", "id_path_mapping.json");
        var idPathMapping = JSON.parse(fs.readFileSync(mp, "utf8"));
        console.info("Merging shot ", shotID);
        if (!idPathMapping || !idPathMapping[shotID]) {
            console.info("No id_path_mapping");
        }
        ;
        var shotHierarchyPath = idPathMapping[shotID];
        if (!shotHierarchyPath) {
            console.info("WARN: Shot not in id_path_mapping. Can not merge with ancestors.");
            return shotData;
        }
        // Load shot data for each shotID in id_path_mapping
        var ancestors = {};
        for (var i = 0; i < shotHierarchyPath.length; i++) {
            console.info("hierarchy ", i);
            console.info(shotHierarchyPath[i]);
            var idToLoad = shotHierarchyPath[i];
            var shotFilePath = this.shotMapping[idToLoad].paths.generic + '.json';
            var data = this.loadShotJSON(shotFilePath);
            ancestors[idToLoad] = data;
        }
        // After each parent->child Merge, the result is the new parent.
        // We start with the oldest ancestor:
        var oldestAncestorID = shotHierarchyPath[0];
        var mergedAncestorData = clonedeep(ancestors[oldestAncestorID]);
        // console.info("Starting with oldest ancestor:")
        // console.info(mergedAncestorData)
        // Loop over the ancestors and process parameters
        for (var i = 0; i < shotHierarchyPath.length; i++) {
            console.info("\n\nProcessing hierarchy path ", i);
            var parentID = shotHierarchyPath[i];
            var childID = shotHierarchyPath[i + 1];
            if (!childID) {
                // Reached the end of the path.
                // In this case, parentID needs to be the same as shotID (the shot we are processing)
                console.info("-> Reached end of path");
                if (!(parentID == shotID)) {
                    console.info("WARN: End of hierarchy is not current shot!");
                }
                break;
            }
            var childData = ancestors[childID];
            // Parameter situation
            // PARENT       CHILD
            // hasparam    hasparam  -> inherit (merge/replace)
            // noparam     noparam   -> empty object or false
            // hasparam     noparam  -> copy from ancestor
            // noparam      hasparam  -> copy from child
            //             disinherit -> clone from child, REMOVE disinherit
            // Processing .load
            console.info("\nAncestor merge: Load");
            var ancestorLoadData = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.load;
            var childLoadData = childData === null || childData === void 0 ? void 0 : childData.load;
            var loadData = this.inheritParam(ancestorLoadData, childLoadData);
            mergedAncestorData.load = loadData;
            // Processing .animation
            // .Camera
            console.info("\nAncestor merge: Camera");
            var ancestorCamData = (_a = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.animation) === null || _a === void 0 ? void 0 : _a.Camera;
            var childCamData = (_b = childData === null || childData === void 0 ? void 0 : childData.animation) === null || _b === void 0 ? void 0 : _b.Camera;
            var camData = this.inheritObject(ancestorCamData, childCamData);
            // .env
            console.info("\nAncestor merge: env");
            var ancestorEnvData = (_c = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.animation) === null || _c === void 0 ? void 0 : _c.env;
            var childEnvData = (_d = childData === null || childData === void 0 ? void 0 : childData.animation) === null || _d === void 0 ? void 0 : _d.env;
            var envData = this.inheritObject(ancestorEnvData, childEnvData);
            if (!mergedAncestorData.animation) {
                mergedAncestorData.animation = {};
            }
            mergedAncestorData.animation.env = envData;
            mergedAncestorData.animation.Camera = camData;
            // Build background
            console.info("\nAncestor merge: background");
            var ancestorBackground = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.background;
            var childBackground = childData === null || childData === void 0 ? void 0 : childData.background;
            var backgroundData = this.inheritParam(ancestorBackground, childBackground);
            mergedAncestorData.background = backgroundData;
            // Processing .animGraph
            // .animGraph is an object, but it should never be merged with the ancestor.
            console.info("\nAncestor merge: animGraph");
            var ancestorAnimGraph = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.animGraph;
            var childAnimGraph = childData === null || childData === void 0 ? void 0 : childData.animGraph;
            var AnimGraph = this.inheritObject(ancestorAnimGraph, childAnimGraph, false);
            mergedAncestorData.animGraph = AnimGraph;
            // Build materialTweaks - is Array, using inheritParam therefore will not merge
            // with ancestor
            console.info("\nAncestor inherit: materialTweaks");
            var ancestorMaterialTweaks = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.materialTweaks;
            var childMaterialTweaks = childData === null || childData === void 0 ? void 0 : childData.materialTweaks;
            var materialTweaksData = this.inheritParam(ancestorMaterialTweaks, childMaterialTweaks);
            mergedAncestorData.materialTweaks = materialTweaksData;
            // Build uvOffset - is Array, using inheritParam therefore will not merge
            // with ancestor
            console.info("\nAncestor inherit: uvOffset");
            var ancestorUvOffset = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.uvOffset;
            var childUvOffset = childData === null || childData === void 0 ? void 0 : childData.uvOffset;
            var UvOffsetData = this.inheritParam(ancestorUvOffset, childUvOffset);
            mergedAncestorData.uvOffset = UvOffsetData;
            // Build setEnabled
            console.info("\nAncestor merge: setEnabled");
            var ancestorSetEnabled = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.setEnabled;
            var childSetEnabled = childData === null || childData === void 0 ? void 0 : childData.setEnabled;
            var setEnabledData = this.inheritParam(ancestorSetEnabled, childSetEnabled);
            mergedAncestorData.setEnabled = setEnabledData;
            console.info("\nAncestor merge: enabledState");
            var ancestorEnabledState = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.enabledState;
            var childEnabledState = childData === null || childData === void 0 ? void 0 : childData.enabledState;
            var enabledStateData = this.inheritParam(ancestorEnabledState, childEnabledState);
            mergedAncestorData.enabledState = enabledStateData;
            // Build fadeCollections
            console.info("\nAncestor merge: fadeCollections");
            var ancestorfadeCollections = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.fadeCollections;
            var childfadeCollections = childData === null || childData === void 0 ? void 0 : childData.fadeCollections;
            var fadeCollectionsData = this.inheritParam(ancestorfadeCollections, childfadeCollections);
            mergedAncestorData.fadeCollections = fadeCollectionsData;
            console.info("\nAncestor merge: fadeAmount");
            var ancestorfadeAmount = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.fadeAmount;
            var childfadeAmount = childData === null || childData === void 0 ? void 0 : childData.fadeAmount;
            var fadeAmountData = this.inheritParam(ancestorfadeAmount, childfadeAmount);
            mergedAncestorData.fadeAmount = fadeAmountData;
            // Build hotspots
            // Hotspots can have an "Ancestor Modifiers". If present, it will be applied to the
            // ancestor object. 
            console.info("\nAncestor merge: hotspots");
            var hotspotMetadata = childData === null || childData === void 0 ? void 0 : childData.hotspotsMeta;
            var ancestorHotspots = mergedAncestorData === null || mergedAncestorData === void 0 ? void 0 : mergedAncestorData.hotspots;
            var childHotspots = childData === null || childData === void 0 ? void 0 : childData.hotspots;
            if (!childHotspots) {
                // Cant have undefined, since we need to copy .disinherit into hotpots object:
                childHotspots = {};
            }
            console.info(" - ancestorHotspots");
            console.info(ancestorHotspots);
            console.info(" - childHotspots");
            console.info(childHotspots);
            var hotspotModifier = {};
            // Hotspots can have metadata. In this case, .disinherit is in metadata and needs to 
            // be copied into data.
            if (hotspotMetadata) {
                childHotspots.disinherit = hotspotMetadata.disinherit;
                hotspotModifier = hotspotMetadata.modifier;
            }
            var mergedHotspotData = this.inheritObject(ancestorHotspots, childHotspots, true, hotspotModifier);
            delete mergedHotspotData.disinherit; // Just to be sure
            mergedAncestorData.hotspots = mergedHotspotData;
        }
        // console.info("\n\nReturning merged data:")
        // console.info(JSON.stringify(mergedAncestorData, null, 4));
        // console.info("\n\n\n\n")
        return mergedAncestorData;
        // OLD STUFF:
        /*
                if (!shotData.master) {
                    return shotData;
                }
                if (shotData.master == "isMaster") {
                    // Shot is a master shot and can not
                    // import data from another shot
                    return shotData;
                }
        
        
        
                if (shotData.master.camera) {
                    console.info("master camera")
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.camera);
                    // if shot has no camera, throw
                    if (!baseData?.animation["Camera"]) {
                        throw "Shot " + shotData.master.camera + " has no camera data to import";
                    }
        
                    console.info("Merging camera data from ", shotData.master.camera)
                    // We only want to merge the camera data
        
                    // shotData has no animation
                    if (!shotData.animation) {
                        shotData["animation"] = { "Camera": baseData.animation["Camera"] }
        
                    }
                    // shotData has no "Camera"
                    else if (!shotData.animation["Camera"]) {
                        shotData.animation["Camera"] = baseData.animation["Camera"];
        
                    }
                    else {
                        // shotData has animation and camera
                        const cameraBaseData = clonedeep(baseData.animation["Camera"]);
                        merge(cameraBaseData, shotData.animation["Camera"]);
                        shotData.animation["Camera"] = cameraBaseData;
                    }
        
                }
        
                if (shotData.master.EnvSphere) {
        
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.EnvSphere);
        
                    if (!baseData?.animation["EnvSphere"]) {
                        throw "Shot " + shotData.master.EnvSphere + " has no EnvSphere data to import";
                    }
        
                    console.info("Merging EnvSphere data from ", shotData.master.camera)
        
                    // shotData has no animation
                    if (!shotData.animation) {
                        shotData["animation"] = { "EnvSphere": baseData.animation["EnvSphere"] }
        
                    }
                    // shotData has no "EnvSphere"
                    else if (!shotData.animation["EnvSphere"]) {
                        shotData.animation["EnvSphere"] = baseData.animation["EnvSphere"];
        
                    }
                    else {
                        // shotData has animation and EnvSphere
                        const envSphereBaseData = clonedeep(baseData.animation["EnvSphere"]);
                        merge(envSphereBaseData, shotData.animation["EnvSphere"]);
                        shotData.animation["EnvSphere"] = envSphereBaseData;
                    }
        
                }
        
                if (shotData.master.env) {
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.env);
        
                    if (!baseData?.animation["env"]) {
                        throw "Shot " + shotData.master.env + " has no env data to import";
                    }
        
                    console.info("Merging env data from ", shotData.master.env)
        
                    // shotData has no animation
                    if (!shotData.animation) {
                        shotData["animation"] = { "env": baseData.animation["env"] }
        
                    }
                    // shotData has no "env"
                    else if (!shotData.animation["env"]) {
                        shotData.animation["env"] = baseData.animation["env"];
        
                    }
                    else {
                        // shotData has animation and env
                        const envBaseData = clonedeep(baseData.animation["env"]);
                        merge(envBaseData, shotData.animation["env"]);
                        shotData.animation["env"] = envBaseData;
                    }
                }
        
                if (shotData.master.animGraph) {
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.animGraph);
        
                    if (!baseData?.animGraph) {
                        throw "Shot " + shotData.master.animGraph + " has no animGraph data to import";
                    }
        
                    console.info("Merging animGraph data from ", shotData.master.animGraph)
        
                    // shotData has no animGraph
                    if (!shotData.animGraph) {
                        shotData["animGraph"] = baseData.animGraph;
        
                    } else {
                        // shotData has animGraph
                        const animGraphBaseData = clonedeep(baseData.animGraph);
                        merge(animGraphBaseData, shotData.animGraph);
                        shotData.animGraph = animGraphBaseData;
                    }
                }
        
                if (shotData.master.hotspots) {
                    const baseData = this.lookUpShotAndLoad(shotData.master.hotspots.shot);
        
        
        
                    if (!baseData?.hotspots) {
                        throw "Shot " + shotData.master.hotspots + " has no hotspots data to import";
                    }
        
                    console.info("Merging hotspots data from ", shotData.master.hotspots)
        
                    // shotData has no hotspots
                    if (!shotData.hotspots) {
                        shotData["hotspots"] = {};
        
                    }
                    // shotData has hotspots
                    const hotspotsBaseData = clonedeep(baseData.hotspots);
        
                    // Apply the modifiers to the baseData
                    if (shotData.master.hotspots.modifier) {
                        if (shotData.master.hotspots.modifier.expanded != null) {
                            const expandedVal = shotData.master.hotspots.modifier.expanded
                            Object.keys(hotspotsBaseData).forEach(k => {
                                hotspotsBaseData[k].expanded = expandedVal
                            })
                        }
                    }
        
                    merge(hotspotsBaseData, shotData.hotspots);
                    shotData.hotspots = hotspotsBaseData;
        
        
                    // delete keepHotspots
                    if (shotData.keepHotspots != null) {
                        delete shotData.keepHotspots;
                    }
        
                }
        
                if (shotData.master.setEnabled) {
                    // merge arrays and copy enabledState
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.setEnabled);
        
                    if (!baseData?.setEnabled) {
                        throw "Shot " + shotData.master.setEnabled + " has no setEnabled data to import";
                    }
        
                    console.info("Merging setEnabled data from ", shotData.master.setEnabled)
        
                    // shotData has no setEnabled
                    if (!shotData.setEnabled) {
                        shotData["setEnabled"] = baseData.setEnabled;
                        shotData["enabledState"] = baseData.enabledState;
        
                    } else {
                        // shotData has setEnabled
                        const setEnabledBaseData = clonedeep(baseData.setEnabled);
                        merge(setEnabledBaseData, shotData.setEnabled);
                        shotData.setEnabled = setEnabledBaseData;
                        shotData["enabledState"] = baseData.enabledState;
                    }
        
                }
        
        
                if (shotData.master.timeline) {
                    // Lookup shot ID from name
                    const baseData = this.lookUpShotAndLoad(shotData.master.timeline);
        
                    if (!baseData?.timeline) {
                        throw "Shot " + shotData.master.timeline + " has no timeline data to import";
                    }
        
                    console.info("Merging timeline data from ", shotData.master.timeline)
        
                    // shotData has no timeline
                    if (!shotData.timeline) {
                        shotData["timeline"] = baseData.timeline;
        
                    } else {
                        // shotData has timeline
                        const timelineBaseData = clonedeep(baseData.timeline);
                        merge(timelineBaseData, shotData.timeline);
                        shotData.timeline = timelineBaseData;
                    }
                }
        */
        return shotData;
    };
    /**
     *
     * @param ancestorData
     * @param childData
     * @param doInherit set false if data should NOT be merged with ancestor
     * @param modifier
     * @returns
     */
    preprocess.prototype.inheritObject = function (ancestorData, childData, doInherit, modifier) {
        var e_1, _a, e_2, _b;
        if (doInherit === void 0) { doInherit = true; }
        if (modifier === void 0) { modifier = false; }
        // parent noParam, child noParam
        if (!childData && !ancestorData) {
            // ancestor and child do not have param. Object will not exist on shot. 
            console.info("parent noParam, child noParam -> Object will not exist on shot.");
            return false;
        }
        // disinherit
        if (childData === null || childData === void 0 ? void 0 : childData.disinherit) {
            console.info("Child does not want to inherit object");
            // Use the data from the child
            // BUT REMOVE disinherit! Otherwise the next child will never inherit.
            var ob = clonedeep(childData);
            delete ob.disinherit;
            return ob;
        }
        // child hasParam, parent hasParam   (merge/replace)
        if (childData && ancestorData) {
            if (doInherit) {
                console.info("child hasParam, parent hasParam -> Merge Object");
                // Apply modifier
                if (modifier) {
                    console.info("Applying modifier");
                    try {
                        // Maybe this only works with hotspots for now?
                        // For each key,value pair of ancestorData, merge value with modifier
                        for (var _c = __values(Object.entries(ancestorData)), _d = _c.next(); !_d.done; _d = _c.next()) {
                            var _e = __read(_d.value, 2), key = _e[0], value = _e[1];
                            //console.log(`${key}: ${value}`);
                            var toModifiy = clonedeep(ancestorData[key]);
                            console.info("Before: ");
                            console.info(toModifiy);
                            merge(toModifiy, modifier);
                            console.info("After: ");
                            console.info(toModifiy);
                            ancestorData[key] = toModifiy;
                        }
                    }
                    catch (e_1_1) { e_1 = { error: e_1_1 }; }
                    finally {
                        try {
                            if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                        }
                        finally { if (e_1) throw e_1.error; }
                    }
                }
                return merge(ancestorData, childData);
            }
            else {
                console.info("child hasParam, parent hasParam, but doInherit is false -> use Child Object");
                return clonedeep(childData);
            }
        }
        // child hasParam, parent noParam   copy from child
        if (childData && !ancestorData) {
            console.info("child hasParam, parent noParam -> use Child Object");
            return clonedeep(childData);
        }
        // child noParam, parent hasParam    copy from ancestor
        if (!childData && ancestorData) {
            console.info("child noParam, parent hasParam -> use Ancestor Object");
            // Apply modifier
            if (modifier) {
                console.info("Applying modifier");
                try {
                    for (var _f = __values(Object.entries(ancestorData)), _g = _f.next(); !_g.done; _g = _f.next()) {
                        var _h = __read(_g.value, 2), key = _h[0], value = _h[1];
                        var toModifiy = clonedeep(ancestorData[key]);
                        //console.info("Before: ");console.info(toModifiy);
                        merge(toModifiy, modifier);
                        //console.info("After: ");console.info(toModifiy);
                        ancestorData[key] = toModifiy;
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (_g && !_g.done && (_b = _f.return)) _b.call(_f);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
            return clonedeep(ancestorData);
        }
        console.info("None of the cases match -> set object to false");
        return false;
    };
    preprocess.prototype.inheritParam = function (ancestorData, childData) {
        // parent noParam, child noParam
        if (childData == null && ancestorData == null) {
            // ancestor and child do not have param. Param will not exist on shot. 
            console.info("parent noParam, child noParam -> Param will not exist on shot.");
            return false;
        }
        // disinherit
        if (childData === null || childData === void 0 ? void 0 : childData.disinherit) {
            console.info("Child does not want to inherit parameter");
            // Param will not exist on shot
            return false;
        }
        // child hasParam, parent hasParam   use childs param
        if (childData && ancestorData) {
            console.info("child hasParam, parent hasParam -> use Child Param");
            return childData;
        }
        // child hasParam, parent noParam   use childs param
        if (childData && ancestorData == null) {
            console.info("child hasParam, parent noParam -> use childs param");
            return childData;
        }
        // child noParam, parent hasParam    use ancestors param
        if (childData == null && ancestorData) {
            console.info("child noParam, parent hasParam  ->  use ancestors param");
            return clonedeep(ancestorData);
        }
        console.info("None of the cases match, set param to false");
        return false;
    };
    /**
     * replace shot and page names with IDs in timeline items
     */
    preprocess.prototype.resolveTimelineItems = function (shotData) {
        var _this = this;
        if (shotData.timeline === "disinherit" || !shotData.timeline || !shotData.timeline.length) {
            return shotData;
        }
        shotData.timeline.forEach(function (item) {
            if (item.shot) {
                var shotID = _this.shotNameToIDMapping[item.shot];
                item.shot = shotID;
            }
            if (item.page) {
                if (item.page.includes("#")) {
                    var pageAndClassSplit = item.page.split("#");
                    var pageID = _this.pageNameToIDMapping[pageAndClassSplit[0]];
                    item.page = pageID + "#" + pageAndClassSplit[1];
                }
                else {
                    var pageID = _this.pageNameToIDMapping[item.page];
                    item.page = pageID;
                }
            }
        });
        return shotData;
    };
    preprocess.prototype.localizeTimelineItemTitles = function (locale, shotData) {
        if (shotData.timeline === "disinherit" || !shotData.timeline || !shotData.timeline.length) {
            return shotData;
        }
        shotData.timeline.forEach(function (item) {
            var localizedTitle = item.title[locale];
            item.title = localizedTitle;
        });
        return shotData;
    };
    /**
     * Count hotspots with .content field
     */
    preprocess.prototype.hotspotsHaveContent = function (hotspots) {
        var contentCount = 0;
        Object.keys(hotspots).forEach(function (hname) {
            var hData = hotspots[hname];
            if (!hData.content) {
                return;
            }
            else {
                contentCount++;
            }
        });
        return contentCount;
    };
    /**
     * Map localized html to each hotspot for each locale specified
     * in ft-config
     * @param hotspots
     */
    preprocess.prototype.buildHotspotAllLocaleContent = function (hotspots) {
        var _this = this;
        var localeContent = {};
        Object.keys(hotspots).forEach(function (hname) {
            var hData = hotspots[hname];
            if (!hData.content) {
                return;
            }
            localeContent[hname] = {};
            // process markdown for each locale, save in temp object
            // [hname]:{locale:html, locale:html}
            _this.ftConfig.locales.forEach(function (locale) {
                var hotspotID = _this.hotspotNameToIDMapping[hData.content];
                if (!hotspotID) {
                    return localeContent;
                }
                var hotspotPath = _this.hotspotMapping[hotspotID].paths[locale];
                if (!hotspotPath) {
                    return localeContent;
                }
                var mdObject = _this.processMarkdownToObject(hotspotPath, locale, EMarkdownType.hotspot, hotspotID);
                var hotspotHtml = _this.markdownObjectToHtml(mdObject);
                localeContent[hname][locale] = hotspotHtml;
            });
        });
        return localeContent;
    };
    preprocess.prototype.buildHotspotOneLocaleContent = function (hotspots, locale) {
        var _this = this;
        var localeContent = {};
        Object.keys(hotspots).forEach(function (hname) {
            var hData = hotspots[hname];
            if (!hData.content) {
                return;
            }
            localeContent[hname] = {};
            var hotspotID = _this.hotspotNameToIDMapping[hData.content];
            if (!hotspotID) {
                return localeContent;
            }
            var hotspotPath = _this.hotspotMapping[hotspotID].paths[locale];
            if (!hotspotPath) {
                return localeContent;
            }
            // const mdFilePath = path.join(this.sourceRootDir, "h", hData.content + "_" + locale + ".md");
            var mdObject = _this.processMarkdownToObject(hotspotPath, locale, EMarkdownType.hotspot, hotspotID);
            var hotspotHtml = _this.markdownObjectToHtml(mdObject);
            localeContent[hname][locale] = hotspotHtml;
        });
        return localeContent;
    };
    /**
     *
     * @param shotID id of shot
     */
    preprocess.prototype.shotContentFromShot = function (shotID, locale) {
        var shotPath = this.shotMapping[shotID].paths.generic + '.json';
        if (!shotPath) {
            return { error: "No file for shot " + this.shotMapping[shotID].clearname };
        }
        var filepath = shotPath; //path.join(this.sourceRootDir, "s", shotName + '.json');
        // Return error if file does not exist
        if (!fs.existsSync(filepath)) {
            return { error: "Could not find file " + filepath };
        }
        //if (shotName.includes('/_master')) {
        //    return { error: "Can not process _master shot" };
        //}
        console.info("\n______Processing Shot ", filepath);
        var shotDataRaw = this.loadShotJSON(filepath);
        if (!shotDataRaw) {
            return { error: "Has no data" };
            ;
        }
        var shotData = this.mergeShotWithMaster(shotDataRaw, shotID);
        shotData.rawData = shotDataRaw; // Inject raw data
        this.resolveTimelineItems(shotData);
        // Keep only localized title in timeline Items
        if (locale != null) {
            this.localizeTimelineItemTitles(locale, shotData);
        }
        shotData.shotGenericPath = this.shotMapping[shotID].paths.generic;
        // Return shot if there are no hotspots
        if (!shotData.hotspots) {
            return { data: shotData };
        }
        // Only process hotspots if there is at least one .content field
        var contentCount = this.hotspotsHaveContent(shotData.hotspots);
        if (contentCount == 0) {
            return { data: shotData };
        }
        // If there are hotspots (if locale is specified), for each hotspot, process markdown for each locale
        // and store in tmp object.
        var localeContent;
        if (locale != null) {
            localeContent = this.buildHotspotOneLocaleContent(shotData.hotspots, locale);
        }
        // Make a copy of shotData
        var tmpShotData = clonedeep(shotData);
        // If shot has hotspots:
        // Each hotspot with .content, replace .content with localized html.
        if (locale != null) {
            Object.keys(tmpShotData.hotspots).forEach(function (hname) {
                var hData = tmpShotData.hotspots[hname];
                // Not every hotspot has content
                if (!hData.content) {
                    return;
                }
                hData.content = localeContent[hname][locale];
            });
        }
        return { data: tmpShotData };
    };
    /**
     *
     * @param pageID pageID
     * @param locale
     * @returns
     */
    preprocess.prototype.pageContentFromPageAndLocale = function (pageID, locale) {
        if (!pageID) {
            console.info("No id for page ", pageID);
            return { outputPathLocalized: pageID, locale: locale, mdObject: null };
        }
        // handle status page
        if (pageID === "__status__") {
            var statusPath = path.join('pre', 'status', 'shotStatus_en-US.md');
            var _a = this.buildPageContent(statusPath, pageID), outputPathLocalized_1 = _a.outputPathLocalized, l_1 = _a.locale, mdObject_1 = _a.mdObject;
            mdObject_1.genericPath = "__status__";
            return { outputPathLocalized: outputPathLocalized_1, locale: locale, mdObject: mdObject_1 };
        }
        var pagePath = this.pageMapping[pageID].paths[locale];
        if (!pagePath) {
            console.info("No file for page ", pageID);
            return { outputPathLocalized: pageID, locale: locale, mdObject: null };
        }
        var filepath = pagePath;
        // Return error if file does not exist
        if (!fs.existsSync(filepath)) {
            return { pageMdFilepath: filepath, locale: locale, mdObject: null };
        }
        var _b = this.buildPageContent(filepath, pageID), outputPathLocalized = _b.outputPathLocalized, l = _b.locale, mdObject = _b.mdObject;
        mdObject.genericPath = this.pageMapping[pageID].paths.generic;
        return { outputPathLocalized: outputPathLocalized, locale: locale, mdObject: mdObject };
    };
    /**
     *
     * @param pageMdFilepath filepath with root e.g. pre/p/...
     * @returns
     */
    preprocess.prototype.buildPageContent = function (pageMdFilepath, pageID) {
        // extract locale from filepath
        // _([a-z]*)-([A-Z]*).md$
        var localRegex = /_([a-z]*-[A-Z]*).md$/;
        var layoutMatch = pageMdFilepath.match(localRegex);
        // ok if it crashes here -> user needs to fix source files
        var locale = layoutMatch[1].trim();
        var mdFilePath = path.join(pageMdFilepath);
        var mdObject = this.processMarkdownToObject(mdFilePath, locale, EMarkdownType.page);
        //const pageID = this.pageNameToIDMapping[pageMdFilepath]
        var idName = pageID + '.json';
        var outputPathLocalized = path.join(this.outputRootDir, "p", locale, idName); //f.replace('.json', '_' + locale + '.json');
        return { outputPathLocalized: outputPathLocalized, locale: locale, mdObject: mdObject };
    };
    /**
     * Turns ft markdown into Object
     * @param markdownFilePath
     */
    preprocess.prototype.processMarkdownToObject = function (markdownFilePath, locale, type, hotspotID) {
        if (hotspotID === void 0) { hotspotID = null; }
        var markdownContent = fs.readFileSync(markdownFilePath, 'utf8');
        // In markdown file, replace imports, then split everything into
        // an object before processing.
        if (this.dev && hotspotID) {
            // add block to visualize hotspot path
            var devHotspotPath = "<!--@dev-->\n<!--::dev-->\n".concat(this.hotspotMapping[hotspotID].paths.generic);
            markdownContent = markdownContent + devHotspotPath;
        }
        // Import and replace linked markdown
        var replacedImports = this.templateImports(markdownContent, locale, type);
        var buildingParts = { template: "", blocks: [] };
        buildingParts.template = this.templateLayout(replacedImports);
        buildingParts.blocks = this.templateBlocks(replacedImports);
        return buildingParts;
    };
    /**
     * For Hotspots: takes markdown-converted object and builds the html string
     * @param buildingParts
     */
    preprocess.prototype.markdownObjectToHtml = function (buildingParts) {
        var _this = this;
        var allBlocksHtml = [];
        // Process each block. Load the template and fill it with content from params.
        Object.keys(buildingParts.blocks).forEach(function (index) {
            var blockData = buildingParts.blocks[index];
            // Load html template
            var blockHtmlTemplate = fs.readFileSync(path.join(_this.hotspotTemplateDir, "blocks", blockData.template + '.html'), 'utf8');
            // Replace params in html template with content from object
            var blockHtml = blockHtmlTemplate.replace(/%(.*)%/g, function (str, param) { return blockData.params[param]; });
            allBlocksHtml.push(blockHtml);
        });
        // Load the Layout template and replace
        // If no template was specified, just return the blocks
        var pureHtml;
        if (!buildingParts.template) {
            pureHtml = DOMPurify.sanitize(allBlocksHtml.join(''));
        }
        else {
            var layoutHtmlTemplate = fs.readFileSync(path.join(this.hotspotTemplateDir, "layout", buildingParts.template + '.html'), 'utf8');
            var html = layoutHtmlTemplate.replace('%blocks%', allBlocksHtml.join(''));
            pureHtml = DOMPurify.sanitize(html);
        }
        var result = minify(pureHtml, {
            removeAttributeQuotes: true,
            collapseWhitespace: true,
            minifyCSS: true,
            quoteCharacter: "'"
        });
        return result;
    };
    /**
     * Save the rawData into pre/s
     */
    preprocess.prototype.saveRawShot = function (shotData, filepath) {
        var json = JSON.stringify(shotData, null, 4);
        // fs.mkdirSync(path.dirname(filepath), { recursive: true });
        fs.writeFileSync(filepath + ".json", json);
    };
    /**
     *
     * @param shotData
     * @param path
     */
    preprocess.prototype.saveShot = function (shotData, filepath) {
        // Replace first dir in path with outputRoot
        var outFilePath = filepath; //.replace(this.sourceRootDir, this.outputRootDir);
        var json;
        if (this.prettyPrintJson) {
            json = JSON.stringify(shotData, null, 4);
        }
        else {
            json = JSON.stringify(shotData);
        }
        // Make folders and write
        fs.mkdirSync(path.dirname(outFilePath), { recursive: true });
        fs.writeFileSync(outFilePath, json);
    };
    /**
     *
     * @param pageMdFilepath
     * @param locale
     * @param mdObject
     */
    preprocess.prototype.savePage = function (pageMdFilepath, locale, mdObject) {
        // Replace sourceRoot with outputRoot
        // Replace .md with .json
        var outFilePath = pageMdFilepath; //.replace(this.sourceRootDir, this.outputRootDir);
        var json;
        if (this.prettyPrintJson) {
            json = JSON.stringify(mdObject, null, 4);
        }
        else {
            json = JSON.stringify(mdObject);
        }
        // Make folders and write
        fs.mkdirSync(path.dirname(outFilePath), { recursive: true });
        fs.writeFileSync(outFilePath, json);
    };
    preprocess.prototype.templateLayout = function (md) {
        var layoutRegex = /<!--\$(.*)-->/; // Not global, will only match first occurence
        var layoutMatch = md.match(layoutRegex);
        if (layoutMatch && layoutMatch[1]) {
            return layoutMatch[1].trim();
        }
        else {
            return null;
        }
    };
    /**
     * Replace import annotations <!--@>importMe--> with markdown. It will not replace imports IN imported markdowns!
     * @param md
     * @param locale
     */
    preprocess.prototype.templateImports = function (md, locale, type) {
        var _this = this;
        var importRegex = /<!--@>(.*)-->/g;
        var matches = __spreadArray([], __read(md.matchAll(importRegex)), false);
        // Collect all paths to md files. Paths are just the cleanname of the file, e.g. toc
        var toImport = [];
        // Extract paths from matches 
        for (var i = 0; i < matches.length; i++) {
            toImport.push(matches[i][1]);
        }
        // Return if nothing to import
        if (!toImport.length) {
            return md;
        }
        // Remove doubles in Array of paths
        var uniqueToImport = __spreadArray([], __read(new Set(toImport)), false);
        // Initialise object to map content to its filename/import annotation
        var mapMdFileToContent = {};
        uniqueToImport.forEach(function (clearname) {
            var pageID = _this.pageNameToIDMapping[clearname];
            if (!pageID) {
                return;
            }
            var pagePath = _this.pageMapping[pageID].paths[locale];
            if (!pagePath) {
                return;
            }
            // Import and map to annotation
            if (pagePath) {
                mapMdFileToContent[clearname] = fs.readFileSync(pagePath, 'utf8');
            }
            else {
                mapMdFileToContent[clearname] = "";
            }
        });
        // Replace each import in the md file
        var replaced = md.replace(/<!--@>(.*)-->/g, function (str, nme) { return mapMdFileToContent[nme]; }); //`${mdContent[$1]}`)
        return replaced;
    };
    /**
     * Build array of blocks
     * @param md
     * @returns
     */
    preprocess.prototype.templateBlocks = function (md) {
        //const blocks:Array<IBlocks>=[];
        var blockRegex = /<!--@(.*)-->/g;
        var matches = __spreadArray([], __read(md.matchAll(blockRegex)), false);
        var blocks = {};
        // Collect start and end of each block annotation
        for (var i = 0; i < matches.length; i++) {
            var match = matches[i];
            blocks[i] = {
                template: match[1],
                start: match.index,
                end: match.index + match[0].length,
                params: {}
            };
        }
        // Extract content between block annotations
        for (var order in blocks) {
            var startOfContent = blocks[order].end + 1;
            // If there is no next template, use end of string
            var endOfContent = blocks[parseInt(order) + 1] ? blocks[parseInt(order) + 1].start - 1 : md.length - 1;
            var blockContent = md.substring(startOfContent, endOfContent);
            // Extract parameters of each blockContent
            blocks[order].params = this.extractParamsFromBlock(blockContent);
            delete blocks[order].start;
            delete blocks[order].end;
        }
        return blocks;
    };
    /**
     * From a markdown string with parameter decorators
     * <<!--::param-->>
     * extract param and its content
     * @param blockContent string
     */
    preprocess.prototype.extractParamsFromBlock = function (blockContent) {
        var paramRegex = /<!--::(.*)-->/g;
        var matches = __spreadArray([], __read(blockContent.matchAll(paramRegex)), false);
        var temp = {}; // storing start and end for each match
        var content = {};
        for (var i = 0; i < matches.length; i++) {
            var match = matches[i];
            // Collect start and end of each decorator.
            // Key need to be the order, so we can get the 
            // text between each decorator in the next step.
            temp[i] = {
                param: match[1].trim(),
                start: match.index,
                end: match.index + match[0].length,
            };
        }
        // Extract the content between each decorator
        // Content is between end of current and start of next decorator
        for (var order in temp) {
            var startOfContent = temp[order].end + 1;
            // If there is no next decorator, use end of string
            var endOfContent = temp[parseInt(order) + 1] ? temp[parseInt(order) + 1].start - 1 : blockContent.length - 1;
            // Translate markdown and replace links.
            content[temp[order].param] = this.ftAnchorToHTML(blockContent.substring(startOfContent, endOfContent));
        }
        return content;
    };
    preprocess.prototype.replaceIcons = function (content) {
        // match all __SVG__
        var iconRegex = /__SVG__(\S*)/g;
        var matches = __spreadArray([], __read(content.matchAll(iconRegex)), false);
        var mapSvgNameToMarkup = {};
        for (var i = 0; i < matches.length; i++) {
            var match = matches[i];
            var iconFilePath = path.join(this.iconsDir, match[1] + '.html');
            var markdownContent = fs.readFileSync(iconFilePath, 'utf8');
            // Map svg name to imported markup 
            mapSvgNameToMarkup[match[1]] = markdownContent;
        }
        var replaced = content.replace(/__SVG__(\S*)/g, function (str, nme) { return mapSvgNameToMarkup[nme]; });
        return replaced;
    };
    /**
     * Replace ft anchor {{#classnames[page,shot]text}} with html link.
     * page and shot are clearnames, need to be converted to ids.
     * Then generate html.
     * @param md
     */
    preprocess.prototype.ftAnchorToHTML = function (md) {
        var replaceWithClass = md.replace(/{{#(.*)\[(\S+)]/g, "<a data-sveltekit-preload-data=\"off\" class=\"$1\" REPLACEMEhref=\"$2\" >");
        // Space before closing tag is important for the match to work!
        var tailReplace = replaceWithClass.replace(/}}/g, " </a>");
        // Go through each href, split by ',' and find its id
        var hrefReplaced = tailReplace.replace(/REPLACEMEhref=(["'])(.*?)\1/g, function (str, foo, anchor) {
            var links = anchor.split(",");
            console.info(links);
            if (!links.length) {
                return "";
            }
            // get id from name
            var pageID = "";
            var shotID = "";
            if (links[0] && links[0] !== "") {
                if (links[0].startsWith("#")) {
                    pageID = links[0]; // keep highlight class
                }
                else if (links[0].includes("#")) {
                    var pageAndClassSplit = links[0].split("#");
                    pageID = this.pageNameToIDMapping[pageAndClassSplit[0]] + "#" + pageAndClassSplit[1];
                }
                else if (links[0] === "__back__" || links[0] === "__forward__") {
                    pageID = links[0];
                }
                else {
                    pageID = this.pageNameToIDMapping[links[0]];
                }
            }
            if (links[1] && links[1] !== "") {
                if (links[1].startsWith("$")) {
                    // Is inline shot override.
                    shotID = links[1];
                }
                else if (links[1].startsWith("!")) {
                    // Is scroll trigger.
                    // We need to determine, if what comes after, is shot override or shot name
                    if (links[1].startsWith("!$")) {
                        // Is scroll trigger and override.
                        // We simply keep the thing as is:
                        shotID = links[1];
                    }
                    else {
                        // Is scroll trigger, but we need to 
                        // get the id from name (removing the '!' in the lookup)
                        shotID = "!" + this.shotNameToIDMapping[links[1].replace("!", "", 1)];
                    }
                }
                else if (links[1] === "__back__" || links[1] === "__forward__") {
                    shotID = links[1];
                }
                else {
                    shotID = this.shotNameToIDMapping[links[1]];
                }
            }
            return ("href=" + pageID + ',' + shotID);
        }.bind(this));
        // Replace all __SVG__ with imported svg file.
        var iconReplace = this.replaceIcons(hrefReplaced);
        var html = marked.parse(iconReplace);
        // const pureHtml = DOMPurify.sanitize(html);
        var result = minify(html, {
            removeAttributeQuotes: true,
            collapseWhitespace: true,
            minifyCSS: true,
            quoteCharacter: "'"
        });
        return result;
    };
    // compileHotspotCss(): void {
    //     // Each in hotspots files ending with scss, process
    //     console.info("\n______Processing Hotspots SCSS")
    //     const cssOutputCollect = [];
    //     this.hotspotFiles.forEach(filename => {
    //         if (!filename.endsWith("scss")) {
    //             return;
    //         }
    //         //const hotspotsScssFilePath = path.join(this.hotspotTemplateDir, "hotspots.scss");
    //         const hotspotsScssFilePath = path.join(filename);
    //         const result = sass.compile(hotspotsScssFilePath, { style: "compressed" });
    //         cssOutputCollect.push(result.css);
    //     })
    //     const outPath = path.join(this.outputRootDir, 'd', "hotspots.css")
    //     fs.mkdirSync(path.dirname(outPath), { recursive: true });
    //     fs.writeFileSync(outPath, cssOutputCollect.join(''));
    // }
    preprocess.prototype.buildShotsStatus = function (locale) {
        // this.getAllShotFiles(path.join(this.sourceRootDir, "s"));
        var e_3, _a;
        var _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1;
        // open each paths.generic 
        // read data
        // 
        var statusData = clonedeep(this.shotMapping);
        try {
            for (var _2 = __values(Object.keys(statusData)), _3 = _2.next(); !_3.done; _3 = _2.next()) {
                var id = _3.value;
                var data = this.shotContentFromShot(id, locale).data;
                if ((_b = data === null || data === void 0 ? void 0 : data.animation) === null || _b === void 0 ? void 0 : _b.env) {
                    statusData[id].env = true;
                }
                if ((_c = data === null || data === void 0 ? void 0 : data.animation) === null || _c === void 0 ? void 0 : _c.EnvSphere) {
                    statusData[id].EnvSphere = true;
                }
                if ((_g = (_f = (_e = (_d = data === null || data === void 0 ? void 0 : data.animation) === null || _d === void 0 ? void 0 : _d.Camera) === null || _e === void 0 ? void 0 : _e.keys[0]) === null || _f === void 0 ? void 0 : _f.camera) === null || _g === void 0 ? void 0 : _g.pivotPoint) {
                    statusData[id].cameraPivotPoint = true;
                }
                if ((_l = (_k = (_j = (_h = data === null || data === void 0 ? void 0 : data.animation) === null || _h === void 0 ? void 0 : _h.Camera) === null || _j === void 0 ? void 0 : _j.keys[0]) === null || _k === void 0 ? void 0 : _k.camera) === null || _l === void 0 ? void 0 : _l.fov) {
                    statusData[id].cameraFov = true;
                }
                if ((_r = (_q = (_p = (_o = (_m = data === null || data === void 0 ? void 0 : data.animation) === null || _m === void 0 ? void 0 : _m.Camera) === null || _o === void 0 ? void 0 : _o.keys[0]) === null || _p === void 0 ? void 0 : _p.camera) === null || _q === void 0 ? void 0 : _q.float) === null || _r === void 0 ? void 0 : _r.distance) {
                    statusData[id].cameraDistance = true;
                }
                if (((_w = (_v = (_u = (_t = (_s = data === null || data === void 0 ? void 0 : data.animation) === null || _s === void 0 ? void 0 : _s.Camera) === null || _t === void 0 ? void 0 : _t.keys[0]) === null || _u === void 0 ? void 0 : _u.camera) === null || _v === void 0 ? void 0 : _v.float) === null || _w === void 0 ? void 0 : _w.yaw) != null) {
                    statusData[id].cameraYaw = true;
                }
                if (((_1 = (_0 = (_z = (_y = (_x = data === null || data === void 0 ? void 0 : data.animation) === null || _x === void 0 ? void 0 : _x.Camera) === null || _y === void 0 ? void 0 : _y.keys[0]) === null || _z === void 0 ? void 0 : _z.camera) === null || _0 === void 0 ? void 0 : _0.float) === null || _1 === void 0 ? void 0 : _1.pitch) != null) {
                    statusData[id].cameraPitch = true;
                }
                if (data === null || data === void 0 ? void 0 : data.animGraph) {
                    if (Object.keys(data.animGraph).length) {
                        statusData[id].animGraph = true;
                    }
                }
                if ((data === null || data === void 0 ? void 0 : data.setEnabled) && (data === null || data === void 0 ? void 0 : data.enabledState) != null) {
                    statusData[id].setEnabled = true;
                }
                if (data === null || data === void 0 ? void 0 : data.hotspots) {
                    statusData[id].hotspots = true;
                }
                if (data === null || data === void 0 ? void 0 : data.timeline) {
                    statusData[id].timeline = true;
                }
                if (data === null || data === void 0 ? void 0 : data.load) {
                    statusData[id].load = true;
                }
                if ((data === null || data === void 0 ? void 0 : data.master) && data.master == "isMaster") {
                    statusData[id].isMaster = true;
                }
                if (data.background) {
                    statusData[id].background = true;
                }
            }
        }
        catch (e_3_1) { e_3 = { error: e_3_1 }; }
        finally {
            try {
                if (_3 && !_3.done && (_a = _2.return)) _a.call(_2);
            }
            finally { if (e_3) throw e_3.error; }
        }
        return { data: statusData };
    };
    // In the contentPath, create a symlink to modelviewer
    preprocess.prototype.createSymlinks = function (contentPath) {
        if (contentPath === void 0) { contentPath = ""; }
        var contentMvPath = contentPath + "static/mv";
        var contentDracoPath = contentPath + "static/loader";
        var contentTypesPath = contentPath + "src/lib/types";
        // Delete existing symlink
        if (fs.existsSync(contentMvPath)) {
            fs.unlinkSync(contentMvPath);
        }
        if (fs.existsSync(contentDracoPath)) {
            fs.unlinkSync(contentDracoPath);
        }
        if (fs.existsSync(contentTypesPath)) {
            fs.unlinkSync(contentTypesPath);
        }
        var mvPath = path.join(path.resolve(), "static", "mv");
        var dracoPath = path.join(path.resolve(), "static", "loader");
        var typesPath = path.join(path.resolve(), "src", "lib", "types");
        fs.symlinkSync(mvPath, contentMvPath);
        fs.symlinkSync(dracoPath, contentDracoPath);
        fs.mkdirSync(path.dirname(contentTypesPath), { recursive: true });
        fs.symlinkSync(typesPath, contentTypesPath);
    };
    // Delete and create a tmp.scss file. This file is empty during build. 
    // In dev, this file contains imports from content path
    preprocess.prototype.createTmpScss = function (contentPath, dev) {
        if (contentPath === void 0) { contentPath = ""; }
        if (dev === void 0) { dev = false; }
        var scssPath = './src/tmp.scss';
        var scssDevString = "@use '".concat(contentPath, "src/globalStyles/_export.scss';");
        var scssProdString = "@use './globalStyles/_export.scss';";
        if (fs.existsSync(scssPath)) {
            // Delete existing file
            fs.unlinkSync(scssPath);
        }
        if (dev) {
            fs.writeFileSync(scssPath, scssDevString);
        }
        else {
            fs.writeFileSync(scssPath, scssProdString);
        }
    };
    preprocess.prototype.loadConfig = function () {
        console.info("Is dev: ", this.dev);
        if (this.dev) {
            var ftConfigPath = path.join(this.sourceRootDir, "ft-pc-config.json");
            if (!fs.existsSync(ftConfigPath)) {
                return { error: "Could not find file " + ftConfigPath };
            }
            this.ftConfig = this.loadShotJSON(ftConfigPath);
        }
        else {
            this.ftConfig = this.loadShotJSON("./pre/ft-pc-config.json");
        }
    };
    preprocess.prototype.getDataFromContentPath = function () {
        // const ftConfigPath = path.join(this.sourceRootDir, "ft-pc-config");
        var hierarchyPath = path.join(this.sourceRootDir, "menu/back_hierarchy.json");
        var menuPath = path.join(this.sourceRootDir, "menu/menu.json");
        var collectionsPath = path.join(this.sourceRootDir, "collections/collections.json");
        var ucxMeshMappingPath = path.join(this.sourceRootDir, "data/mv_ucx_mesh_mapping.json");
        var gltfLoadDataPath = path.join(this.sourceRootDir, "data/gltf_load.json");
        if (!fs.existsSync(hierarchyPath)) {
            return { error: "Could not find file " + hierarchyPath };
        }
        if (!fs.existsSync(collectionsPath)) {
            return { error: "Could not find file " + collectionsPath };
        }
        if (!fs.existsSync(ucxMeshMappingPath)) {
            return { error: "Could not find file " + ucxMeshMappingPath };
        }
        if (!fs.existsSync(gltfLoadDataPath)) {
            return { error: "Could not find file " + gltfLoadDataPath };
        }
        if (!fs.existsSync(menuPath)) {
            return { error: "Could not find file " + menuPath };
        }
        var hierarchyData = this.loadShotJSON(hierarchyPath);
        var collectionsData = this.loadShotJSON(collectionsPath);
        var ucxMeshMappingData = this.loadShotJSON(ucxMeshMappingPath);
        var gltfLoadDataData = this.loadShotJSON(gltfLoadDataPath);
        var menuData = this.loadShotJSON(menuPath);
        if (!hierarchyData || !collectionsData || !ucxMeshMappingData || !gltfLoadDataData || !menuData) {
            return { error: "Has no data" };
            ;
        }
        return {
            data: {
                ftConfig: this.ftConfig,
                backHierarchy: hierarchyData,
                collections: collectionsData,
                ucxMeshMapping: ucxMeshMappingData,
                gltfLoadData: gltfLoadDataData,
                menuData: menuData
            }
        };
    };
    return preprocess;
}());
export default preprocess;
