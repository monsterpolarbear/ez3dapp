import preprocess from "./preprocessing.js";
import express from 'express';
import cors from 'cors';
import * as path from 'path';
import utilsConfig from './utils-config.js';
// This uses the CONTENT env var, while vite uses VITE_CONTENT_PATH
var contentPath = process.env.CONTENT;
console.info("Content Path:", contentPath);
if (!contentPath) {
    console.warn("No content path! Exiting...");
    process.exit();
}
var pp = new preprocess(path.join(contentPath, "pre"), path.join(contentPath, "static"), true);
// Server
var PORT = utilsConfig.PORT;
var HOST = utilsConfig.HOST;
var app = express();
app.use(cors());
app.use(express.json());
// Delete and create temp.scss. This file is used during dev to import scss from content path.
// vite.config will need same folder in allow list.
pp.createTmpScss(contentPath, true);
// If not exist, create symlink to static/mv, static/loader, etc. 
// This is a workaround because, right now (May 2023), svelte config does not allow multiple static dirs.
// See: https://github.com/sveltejs/kit/issues/5115
pp.createSymlinks(contentPath);
// Serve static from contentpath static
app.use(express.static(contentPath + "static"));
app.get('/p', function (req, res) {
    // http://localhost:3060/p?name=_demo_page1&locale=en-US
    var pageName = req.query.name;
    var locale = req.query.locale;
    // const pp = new preprocess(path.join(contentPath, "pre"), path.join(contentPath,"static"),true);
    pp.getPathFilesDev();
    var _a = pp.pageContentFromPageAndLocale(pageName, locale), pageMdFilepath = _a.pageMdFilepath, l = _a.locale, mdObject = _a.mdObject;
    if (!mdObject) {
        res.status(400).send({
            message: "Could not process " + pageMdFilepath
        });
    }
    else {
        console.info(mdObject);
        res.json(mdObject);
    }
});
app.get('/s', function (req, res) {
    // http://localhost:3060/p?name=_demo_page1&locale=en-US
    // When we are in dev, shot lookup happens from clearname? or both?
    // right now, it looks up shots by clearname, but all anchors are with ids. 
    // should:
    //      in dev, all use clearnames?
    //      entrypoint is clearname, all others are ids?
    var shotName = req.query.name;
    var locale = req.query.locale;
    //const pp = new preprocess(path.join(contentPath, "pre"), path.join(contentPath,"static"),true);
    // Compile Scss for Hotspots
    pp.getHotspotFilesDev();
    pp.getPathFilesDev();
    // pp.getShotFilesDev(); // Already happens in getPathFilesDev
    // pp.compileHotspotCss();
    // Generate shot
    var _a = pp.shotContentFromShot(shotName, locale), data = _a.data, error = _a.error;
    if (error) {
        res.status(400).send({
            message: error
        });
    }
    else {
        res.json(data);
    }
});
app.get('/status', function (req, res) {
    // http://localhost:3060/status?locale=en-US
    var locale = req.query.locale;
    pp.getHotspotFilesDev();
    pp.getPathFilesDev();
    console.info("Building shot status");
    var data = pp.buildShotsStatus(locale).data;
    if (!data) {
        res.status(400).send({
            message: "Problem building status"
        });
    }
    else {
        res.json(data);
    }
});
app.post('/saveshot', function (req, res) {
    console.info("Save Shot to ", req.body.shotGenericPath);
    pp.saveRawShot(req.body, req.body.shotGenericPath);
    // remove .shotGenericPath
    res.send({ status: 'SUCCESS' });
});
// Gets the data from contenPath during dev
app.get('/data', function (req, res) {
    var _a = pp.getDataFromContentPath(), data = _a.data, error = _a.error;
    if (error) {
        res.status(400).send({
            message: error
        });
    }
    else {
        res.json(data);
    }
});
app.listen(PORT, HOST);
console.log("Running on http://".concat(HOST, ":").concat(PORT));
