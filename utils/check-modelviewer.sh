#!/bin/bash


# Do not allow build if app.html does not contain mv from cdn
{
if grep -iq "https://ez3d-mv.netlify.app/model-viewer.min.js" src/app.html;
then
    echo "mv from cdn"
    rm -f static/mv/*
    exit
else
    echo mv not from cdn. Aborting build!
    exit 1
fi
}

# If build is allowed, delete static/mv contents

# if file called 'dev' in static/mv/, then modelviewer was not build for production
# {
# if [ -f static/mv/dev ]; then
#     echo "Please build model-viewer"
#     exit 1
# fi
# }


# Only allow builds from master branch, if modelviewer was built from 'master' branch 
# Check current branch:
# branch_name=$(git symbolic-ref -q HEAD)
# branch_name=${branch_name##refs/heads/}
# branch_name=${branch_name:-HEAD}
# echo On branch $branch_name

# ismaster=false
# if [ "$branch_name" = master ];then
#  echo setting master true
#  ismaster=true
# fi

# check if 'mv/model-viewer.min.js' is in app.html
# {
# if grep -iq "mv/model-viewer.min.js" src/app.html;
# then
#     echo "Using modelviewer dev branch. App builds from master branch are allowed."
#     exit
# fi
# }


# echo "App is not using dev-branch of modelviewer. echtzeit-App builds from master branch are not allowed."
# if [ $ismaster = true ];
# then
#  echo Aborting build
#  exit 1
# else
#  echo Build from branch   $branch_name   is allowed
# fi


